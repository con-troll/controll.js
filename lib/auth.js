/**
 * http://usejsdoc.org/
 */

/**
 * Authentication API
 */
const ConTrollAuth = function(api){
	this.api = api;
	return this;
};

/**
 * Verify that the user is logged in to ConTroll
 * @return Promise an object with a boolean status field
 */
ConTrollAuth.prototype.verify = function() {
	return api.send('auth/verify')
		.then(res => res.status);
};

/**
 * Start authentication by calling ConTroll to provide an auth start URL for the specified provider
 * The returned promise will resolve to the authentication URL and the caller 
 * is expected to redirect the user to it somehow.
 */
ConTrollAuth.prototype.start = function(url, provider, callback) {
	if (arguments.length == 1) { // url is optional
		callback = arguments[0];
		provider = null;
		url = null;
	}
	if (arguments.length == 2) { // provider is optional
		callback = arguments[1];
		provider = null;
	}
	return this.api.send('auth/start', {
		'redirect-url': url,
		'provider': (provider || 'google')
	}).then(res => res['auth-url']);
};

/**
 * Change the current window to the contorll provider selection dialog, and if successful, come back to the
 * specified url.
 * This method only makes sense when running in the client, in which case
 * it will never return. When running in the server, this method will error out.
 */
ConTrollAuth.prototype.startSelect = function(url) {
	let parser = document.createElement('a');
	parser.href = url;
	window.location.href = this.api.endpoint + '/auth/select?redirect-url=' + 
			encodeURIComponent(parser.protocol + "//" + parser.host +  parser.pathname);
};

/**
 * Logout from ConTroll
 * @return a promise that resolves to a boolean specifying whether the logout
 * succeeded or not
 */
ConTrollAuth.prototype.logout = function(callback) {
	return this.api.send('auth/logout').then(() => true).catch(() => false);
}

/**
 * Get the current user's identification data, if logged in
 * @return a promise that resolves with the ID object
 */
ConTrollAuth.prototype.id = function() {
	return this.api.send('auth/id');
};

/**
 * Retrieve the current user convention role, if exists
 * @return a promise that resolves to the role of the logged in user, or
 * null if they have no role
 */
ConTrollAuth.prototype.role = function(callback) {
	this.id().then((res => {
		return this.api.send('entities/managers/' + res.id, { convention: true })
			.then(res => res.role);
	}).bind(this));
};

module.exports = ConTrollAuth;
