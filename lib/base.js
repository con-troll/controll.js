'use strict';

/**
 * Javascript SDK for the Con-Troll API
 */

const Auth = require('./auth');
const Users = require('./users');
const Conventions = require('./conventions');
const UserRecords = require('./convention/user-records');
const Tags = require('./convention/tags');
const Events = require('./convention/events');
const Timeslots = require('./convention/timeslots');
const Tickets = require('./convention/tickets');
const Passes = require('./convention/passes');
const UserPasses = require('./convention/user-passes');
const CouponTypes = require('./convention/coupon-types');
const Coupons = require('./convention/coupons');
const Locations = require('./convention/locations');
const Merchandise = require('./convention/merchandise');
const Purchases = require('./convention/purchases');
const Managers = require('./convention/managers');

/**
 * Helper method to parse incorrectly parsed JSON responses
 * @param reqObj XMLHTTPRequest object
 * @returns response object or parsed JSON
 */
function readResponse(reqObj) {
	if (reqObj.responseType == 'json')
		return reqObj.response;
	try {
		return JSON.parse(reqObj.responseText);
	} catch (e) {
		return false;
	}
}

/**
 * API Constructor
 * you shouldn't need to call this - the API is initialized by the main
 * or browser entry points
 */
const ConTroll = function(window, endpoint) {
	this.endpoint = endpoint || 'http://api.con-troll.org';
	// auto detect testing environment
	if (!endpoint && window && window.location && window.location.host.match(/localhost/))
		this.endpoint = 'http://localhost:8080';
	
	this.auth = new Auth(this);
	this.users = new Users(this);
	this.conventions = new Conventions(this);
	this.records = new UserRecords(this);
	this.tags = new Tags(this);
	this.events = new Events(this);
	this.timeslots = new Timeslots(this);
	this.tickets = new Tickets(this);
	this.passes = new Passes(this);
	this.userpasses = new UserPasses(this);
	this.coupontypes = new CouponTypes(this);
	this.coupons = new Coupons(this);
	this.locations = new Locations(this);
	this.merchandise = new Merchandise(this);
	this.purchases = new Purchases(this);
	this.managers = new Managers(this);

	if (window) {
		this.storage = window.sessionStorage;
		this.handleAuth(window);
	}
	
	return this;
};

/* -- Browser specific routines -- these will not work in the server
 * -- though these are mostly about user authentication, so they shouldn't
 * -- interest server developers
 */

/**
 * When running in the browser, detect if the API called our window with
 * an complete authentication token, in which case store it
 */
ConTroll.prototype.handleAuth = function(w) {
	this.startedIDLookup = true;
	let token = null;
	// check if we got here from a ConTroll redirect 
	w.location.search.split(/[\?\&]+/).forEach(function(f){
		let kv = f.split('='); 
		if (kv[0] == 'token')
			token = decodeURIComponent(kv[1]);
	});
	if (token) {
		// store token locally and reload without the query
		this.storage.setItem('controll-token', token);
		this.authtoken = token;
		if (w.history.pushState) {
			w.history.pushState({controll_token: token}, "authenticated", w.location.pathname);
		} else {
			w.location = w.location.pathname
		}
	}
	
	if (!this.authToken) {
		this.authToken =  w.sessionStorage.getItem('controll-token');
	}
	// load user data from Controll
	this.auth.id().then((res => {
		if (res.status == false)
			return; // no ID
		this.storage.setItem('controll-name', res['name']);
		this.storage.setItem('controll-email', res['email']);
	}).bind(this));
};

/**
 * Check if the user is authenticated with the ConTroll API.
 * If the user is not authenticated, this method will start the authentication
 * process and return to the current URL, instead of resolving the promise.
 * @return a promise that will resolve if the user is authenticated.
 */
ConTroll.prototype.ifAuth = function() {
	if (!this.getAuthToken())
		this.auth.startSelect(window.location.href);
	return this.auth.verify().then((loggedin => {
		if (loggedin)
			return;
		this.auth.startSelect(window.location.href);
	}).bind(this)).capture((() => this.auth.startSelect(window.location.href)).bind(this));
};

/**
 * Change the current window to another URL, authenticating first, if needed.
 * This method does not return
 */
ConTroll.prototype.authAndGo = function(url) {
	this.auth.verify().then((loggedin => {
		if (loggedin) {
			window.location.href = url;
			return;
		}
		
		this.auth.startSelect(url);
	}).bind(this));
};

/**
 * Perform a cashout for the current cart of the specified user
 * @param userid Number ID of user
 * @param amount Number payout received
 * @return a promise that will be resolved when cashout completes
 */
ConTroll.cashout = function(userid, amount) {
	return this.send('checkout/cashout', { user: userid, amount: amount }, 
			{ convention: true }, 'POST');
};

/* -- Non-browser specific methods */

/**
 * Retrieve the current API endpoint URL
 * @return endpoint URL
 */
ConTroll.getEndpoint = function() {
	return api.endpoint;
};

/**
 * Retrieve user's name and email from auth storage
 * @return a property list with the 'name' and 'email' properties
 */
ConTroll.prototype.userDetails = function() {
	return {
		name: this.storage ? this.storage.getItem('controll-name') : null,
		email: this.storage ? this.storage.getItem('controll-email') : null,
	};
};

/**
 * Retrieve the current user's email, from either local storage
 * or by querying the server
 * @return a promise that will resolve with the user's email, if the user
 *   is logged in, or false otherwise
 */
ConTroll.prototype.getUserEmail = function() {
	if (this.storage && this.storage.getItem('controll-email'))
		return new Promise((
				(accept,reject) => accept(this.storage.getItem('controll-email'))
				).bind(this));
	
	return this.auth.id().then(res => res.status ? res.email : false);
};

/**
 * Retrieve the current log in authentication token
 * @return the authentication token if it is known, false otherwise
 */
ConTroll.prototype.getAuthToken = function() {
	if (this.authToken)
		return this.authToken;
	if (this.storage && this.storage.getItem('controll-token'))
		return this.storage.getItem('controll-token');
	return false;
};

ConTroll.prototype.rollbar = function() {
	return this.Rollbar || window.Rollbar;
};

/**
 * Set the convention API key used for convention specific API calls
 * @param api_key String convention's public API key
 */
ConTroll.prototype.setConvention = function(api_key) {
	api.conventionApiKey = api_key;
};

/**
 * main call routine
 * Currently optimized for web browsers
 * TODO: convert to use require('http') and let browserify implement the logic
 */
ConTroll.prototype.send = function(action, data, auth, method) {
	if (typeof arguments[1] == 'object') { // data is optional
		method = arguments[2];
		auth = arguments[1];
		data = null;
	}
	var req = new XMLHttpRequest();
	return new Promise(((resolve, reject) => {
		req.onreadystatechange = function() {
			if (req.readyState == 4) {// DONE
				var res = readResponse(req);
				if (req.status == 200)
					return resolve(res);
				
				if (res.status / 100 == 5) // report server errors to Rollbar
					this.rollbar().error("ConTroll API error:", res || req.responseText || req);
				if (typeof res != 'object') // this not an error reported by the server
					// - its a server error or a network error, fake sth for the caller
					res = { status: false, error: req.responseText || "CORS error" };
				
				console.error(res.error);
				reject(res);
			}
		}.bind(this);
		
		req.withCredentials = true; // allow CORS cookies that are required for the PHP session
		if (data) {
			req.open(method || 'POST', this.endpoint + '/' + action);
			req.setRequestHeader("Content-type","application/json");
			// req.setRequestHeader("Access-Control-Request-Method", "POST");
		} else {
			req.open(method || 'GET', this.endpoint + '/' + action);
		}
		req.setRequestHeader("Cache-Control","no-cache");
		//req.setRequestHeader("Origin",window.location.host);
		if (this.authToken) {
			//req.setRequestHeader('Access-Control-Request-Headers', 'Authorization');
			req.setRequestHeader('Authorization', this.authToken);
		}
		if (auth) {
			if (auth.convention) {
				if (auth.convention === true) auth.convention = this.conventionApiKey;
				req.setRequestHeader('Convention', auth.convention);
			} 
		}
		if (data) {
			req.send(JSON.stringify(data));
		} else {
			req.send();
		}
	}).bind(this));
};

/**
 * Retreive a record or a catalog of collection from the server
 * @param collection Collection to retrieve
 * @param record_id records to lookup - one of:
 *  * a specific ID (Number or String) - get a single object
 *  * false - retrieve the entire collection catalog
 *  * a property list containing filter KV pairs - retrieve a filtered catalog
 * @return promise that will resolve with the response object or reject with an error object
 */
ConTroll.prototype.get = function(collection, record_id) {
	var auth = {};
	if (typeof collection == 'object') {
		for (var field in collection) {
			if (field == 'collection') continue;
			auth[field] = collection[field];
		}
		collection = collection.collection;
	}
	var uri = 'entities/' + collection + '/';
	if (typeof record_id == 'object') {
		var filters = [];
		for (var field in record_id) {
			filters.push(encodeURIComponent('by_' + field) + '=' + encodeURIComponent(record_id[field]));
		}
		uri += '?' + filters.join('&');
	} else if (record_id)
		uri += record_id
	return this.send(uri, auth);
};

/**
 * Create a new record in the specifeid collection
 * @param collection String|Object collection where the object is to be created
 * @param data Object list of proeprties for the new object
 * @return a promise that will resolve or reject with the server response
 */
ConTroll.prototype.create = function(collection, data) {
	var auth = {};
	if (typeof collection == 'object') {
		for (var field in collection) {
			if (field == 'collection') continue;
			auth[field] = collection[field];
		}
		collection = collection.collection;
	}
	return this.send('entities/' + collection, data, auth);
};

/**
 * Update an existing record in the specified collection
 * @param collection String|Object collection where the object is to be udpated
 * @param id Number|String id of the object to be updated
 * @param data Object list of fields to send in the update
 * @return a promise that will resolve or reject with the server response
 */
ConTroll.prototype.update = function(collection, id, data) {
	var auth = {};
	if (typeof collection == 'object') {
		for (var field in collection) {
			if (field == 'collection') continue;
			auth[field] = collection[field];
		}
		collection = collection.collection;
	}
	return this.send('entities/' + collection + '/' + id, data, auth, 'PUT');
};

/**
 * Delete an existing record in the specified collection
 * @param collection String|Object collection where the object is to be deleted
 * @param id Number|String id of the object to be deleted
 * @return a promise that will resolve or reject with the server response
 */
ConTroll.prototype.del= function(collection, id) {
	var auth = {};
	if (typeof collection == 'object') {
		for (var field in collection) {
			if (field == 'collection') continue;
			auth[field] = collection[field];
		}
		collection = collection.collection;
	}
	return this.send('entities/' + collection + '/' + id, null, auth, 'DELETE');
};

module.exports = ConTroll;
