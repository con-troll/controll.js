/**
 * http://usejsdoc.org/
 */

'use strict';

/**
 * ConTroll Merchandise API
 * @param ConTroll api
 */
var ConTrollMerchandise = function(api) {
	this.api = api;
	this.collection = {convention: true, collection: 'merchandiseskus'};
	return this;
};

/**
 * Retrieve the catalog of all currently available merchandise in the convention
 * @return a promise that will resolve with the list of merchandise objects
 */
ConTrollMerchandise.prototype.catalog = function() {
	return this.api.get(this.collection);
};

/**
 * Create a new merchandise item for the convention
 * @param title String the title for the new item
 * @param code String an SKU properties code - a colon delimited list of properties
 * @param price Number|String price of the new item
 * @param description String a long textual description of the new item
 * @return a promise that will resolve with the new merchandise item object
 */
ConTrollMerchandise.prototype.create = function(title, code, price, description) {
	var data = {
			title: title,
			code: code,
			price: price,
			description: description
		};
	return this.api.create(this.collection, data);
};

/**
 * Remove an existing merchandise item from the convention
 * @param id Number id of the merchandise item to remove
 * @return a promise that will resolve to a property list containing a boolean status field
 */
ConTrollMerchandise.prototype.remove = function(id) {
	return this.api.del(this.collection, id);
};

/**
 * Upadte an existing merchandise object in the convention
 * @param id Number id of the merchandise item to update
 * @param title String new title to set for the merchandise item
 * @param price Number|String new price to set for the merchandise item
 * @return a promise that will resolve to a property list containing a boolean status field
 */
ConTrollMerchandise.prototype.update = function(id, title, price) {
	return this.api.update(this.collection, id, {
		title: title,
		price: price
	});
};

module.exports = ConTrollMerchandise;
