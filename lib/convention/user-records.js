/**
 * http://usejsdoc.org/
 */

'use strict';

/**
 * User records API
 */
var ConTrollRecords = function(api) {
	this.api = api;
	return this;
};

/**
 * Retrieve the user data record for the specified descriptor
 * @param descriptor String text label associated with the data
 * @return promise that will resolev with the data object originally
 *   stored on that descriptor, or null if no such data was stored
 */
ConTrollRecords.prototype.get = function(descriptor) {
	return this.api.get({convention: true, collection: 'records'}, descriptor)
		.then(res => data ? JSON.parse(data.data) : null);
};

/**
 * Store a user data record for the convention on the specified descriptior
 * @param descriptior String text label to associate with the data
 * @param data A Javascript value to store
 * @param acl a string describing access permissions. Valid values are
 *   'private' (the default) and 'public'
 * @return a rpomise that will resolve to true if the data was successfully
 *   stored
 */
ConTrollRecords.prototype.save = function(descriptor, data, acl) {
	if (arguments.length == 3) { // acl is optional
		callback = acl;
		acl = null;
	}
	if (!acl) acl = 'private';
	return this.api.create({convention: true, collection: 'records'}, {
		descriptor: descriptor,
		content_type: 'application/json',
		acl: acl,
		data: JSON.stringify(data)
	}).then(() => true);
};

module.exports = ConTrollRecords;
