/**
 * http://usejsdoc.org/
 */

'use strict';

/**
 * ConTroll Events API
 * @param ConTroll api
 */
var ConTrollEvents = function(api) {
	this.api = api;
	this.collection = {convention: true, collection: 'events'};
	return this;
};

/**
 * List all events in the convention
 * @return a promise that will resolve with the list of event objects
 */
ConTrollEvents.prototype.catalog = function() {
	return this.api.get(this.collection);
};

/**
 * Retrieve a specific event by ID
 * @param id Number id of the event to retrieve
 * @return a promise that will resolve with the event object retreived
 */
ConTrollEvents.prototype.get = function(id) {
	return this.api.get(this.collection, id);
};

/**
 * Create a new event in the convention
 * @param title String title of the event to create
 * @param teaser String teaser text for the event
 * @param duration Number default duration for the event in minutes
 * @param ownerId Number the user ID who suggested/owns this event
 * @return a promise that will resolve with the new event object
 */
ConTrollEvents.prototype.create = function(title, teaser, duration, ownerId) {
	return this.api.create(this.collection, {
		title: title,
		teaser: teaser,
		duration: duration,
		user: { id: ownerId }
	});
};

/**
 * Update an existing event with new data
 * @param id Number id of the event to update
 * @param fields Object a list of fields to update on the event
 * @return a promise that will resolve to the updated event object
 */
ConTrollEvents.prototype.update = function(id, fields) {
	return this.api.update(this.collection, id, fields);
};

/**
 * Remove an existing event
 * @param id Number id of the event to remove
 * @return a promise that will resolve to a property list with a boolean status field
 */
ConTrollEvents.prototype.remove = function(id) {
	return this.api.del(this.collection, id);
};

module.exports = ConTrollEvents;
