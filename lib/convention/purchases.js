/**
 * http://usejsdoc.org/
 */

'use strict';

/**
 * ConTroll Purchases API
 * @param ConTroll api
 */
var ConTrollPurchases = function(api) {
	this.api = api;
	this.collection = {convention: true, collection: 'purchases'};
	return this;
};

/**
 * Retrieve the list of purchased items in the convention
 * @return a promise that will resolve to the list of purchase objects
 */
ConTrollPurchases.prototype.catalog = function() {
	return this.api.get(this.collection, '?all=1');
};

module.exports = ConTrollPurchases;
