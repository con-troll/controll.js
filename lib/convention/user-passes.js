/**
 * http://usejsdoc.org/
 */

'use strict';

/**
 * ConTroll User Pass API
 * @param ConTroll api
 */
var ConTrollUserPasses = function(api) {
	this.api = api;
	this.collection = {convention: true, collection: 'userpasses'};
	return this;
};

/**
 * Retrieve the list of all valid user passes for all users
 * @return a promise that will resolve to the list of user pass objects
 */
ConTrollUserPasses.prototype.catalog = function() {
	return this.api.get(this.collection, '?is_valid=1&all=1');
};

/**
 * Retrieve all passes for a user
 * @param userId ID to retrieve a report for
 * @param valid boolean specifying if only valid passes are to be retrieved
 * @return a promise that will resolve tot he list of user pass objects
 */
ConTrollUserPasses.prototype.userReport = function(userId, valid){
	var params = '?user=' + userId;
	if (valid)
		params += '&is_valid=1';
	return this.api.get(this.collection, params);
};

/**
 * Retrieve all passes for a user, including availability report for the specified timeslot
 * @param userId ID to retrieve a report for
 * @param timeslot ID of timeslot to get availabitlity for
 * @return a promise that will resolve to the list of all user pass objects
 */
ConTrollUserPasses.prototype.userTimeslotReport = function(userId, timeslot) {
	return this.api.get(this.collection, '?user=' + userId + '&for_timeslot=' + timeslot);
};

/**
 * Cancel or refund an existing pass
 * @param passId ID of the user pass to cancel
 * @return a promise that will resolve to a property list containing a boolean status field
 */
ConTrollUserPasses.prototype.remove = function(passId) {
	return this.api.del(this.collection, passId);
};

/**
 * Reserve a user pass in the shopping cart
 * @param userId ID of the user to reserve a pass for
 * @param name pass holder name to list on the pass
 * @param passId Id of pass type to reserve
 * @return a promise that will resolve to the newly created user pass object
 */
ConTrollUserPasses.prototype.create = function(userId, name, passId) {
	var data = {
			pass: passId,
			user: userId,
			name: name
	};
	return this.api.create(this.collection, data);
};

module.exports = ConTrollUserPasses;
