/**
 * http://usejsdoc.org/
 */

'use strict';

/**
 * ConTroll Coupon Types API
 * @param ConTroll api
 */
var ConTrollCouponTypes = function(api) {
	this.api = api;
	this.collection = {convention: true, collection: 'coupontypes'};
	return this;
};

/**
 * Retrieve the list of all coupon types in the convention
 * @return a promise that will resolve to the list of coupon type objects
 */
ConTrollCouponTypes.prototype.catalog = function() {
	return this.api.get(this.collection);
};

/**
 * Add a new coupon type to the convention
 * @param title String title of the new coupon type
 * @param value Number|String monetary value of the new coupon type
 * @param category String name of the category this type belongs to
 * @param code String activation code to allow users to create coupons
 * @return a promise that will resolve to the new coupon type object created
 */
ConTrollCouponTypes.prototype.add = function(title, value, category, code) {
	var data = {
			title: title,
			type: 'fixed',
			value: value,
			category: category,
			code: code,
			multiuse: false
	};
	return this.api.create(this.collection, data);
};

/**
 * Remove a coupon type from the convention
 * @parm id Number id of the coupon type to remove
 * @return a promise that will resolve to a property list containing a boolean status field
 */
ConTrollCouponTypes.prototype.remove = function(id) {
	return this.api.del(this.collection, id);
}

module.exports = ConTrollCouponTypes;
