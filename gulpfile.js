'use strict';

const gulp			= require('gulp');
const browserify	= require('browserify');
const source		= require('vinyl-source-stream');
const buffer		= require('vinyl-buffer');
const fs			= require('fs');
const del			= require('del');
const seq			= require('gulp-sequence');
const jshint		= require('gulp-jshint');

const config = {
	base: './lib/browser.js',
	export: 'ConTroll',
	output: 'controll.js',
	outputMin: 'controll.min.js',
	outputDir: 'dist',
};

//todo bundle source maps externally with gulp-browserify / exorcist

function getBundle(debug, args) {
	return browserify({
		standalone: config.export,
		debug: debug||false,
	}, args)
	.add(config.base);
}

gulp.task('clean', function() {
	return del(config.outputDir);
});

gulp.task('lintjs', function(){
	return gulp.src([
		'gulpfile.js',
		'./lib/**/*.js'
	]).pipe(jshint());
	//.pipe(jshint.reporter(stylish))
});

gulp.task('build', function(){
	return getBundle(true)
		.bundle()
		.pipe(source(config.output))
		.pipe(buffer())
		.pipe(gulp.dest(config.outputDir));
});

gulp.task('build-min', function(){
	return getBundle(true)
		.transform('babelify', {presets: ['babili']})
		.bundle()
		.pipe(source(config.outputMin))
		.pipe(buffer())
		.pipe(gulp.dest(config.outputDir));
});

gulp.task('default', seq('clean','lintjs', [ 'build', 'build-min' ]));
