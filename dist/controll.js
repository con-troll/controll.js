(function(f){if(typeof exports==="object"&&typeof module!=="undefined"){module.exports=f()}else if(typeof define==="function"&&define.amd){define([],f)}else{var g;if(typeof window!=="undefined"){g=window}else if(typeof global!=="undefined"){g=global}else if(typeof self!=="undefined"){g=self}else{g=this}g.ConTroll = f()}})(function(){var define,module,exports;return (function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
/**
 * http://usejsdoc.org/
 */

/**
 * Authentication API
 */
const ConTrollAuth = function(api){
	this.api = api;
	return this;
};

/**
 * Verify that the user is logged in to ConTroll
 * @return Promise an object with a boolean status field
 */
ConTrollAuth.prototype.verify = function() {
	return api.send('auth/verify')
		.then(res => res.status);
};

/**
 * Start authentication by calling ConTroll to provide an auth start URL for the specified provider
 * The returned promise will resolve to the authentication URL and the caller 
 * is expected to redirect the user to it somehow.
 */
ConTrollAuth.prototype.start = function(url, provider, callback) {
	if (arguments.length == 1) { // url is optional
		callback = arguments[0];
		provider = null;
		url = null;
	}
	if (arguments.length == 2) { // provider is optional
		callback = arguments[1];
		provider = null;
	}
	return this.api.send('auth/start', {
		'redirect-url': url,
		'provider': (provider || 'google')
	}).then(res => res['auth-url']);
};

/**
 * Change the current window to the contorll provider selection dialog, and if successful, come back to the
 * specified url.
 * This method only makes sense when running in the client, in which case
 * it will never return. When running in the server, this method will error out.
 */
ConTrollAuth.prototype.startSelect = function(url) {
	let parser = document.createElement('a');
	parser.href = url;
	window.location.href = this.api.endpoint + '/auth/select?redirect-url=' + 
			encodeURIComponent(parser.protocol + "//" + parser.host +  parser.pathname);
};

/**
 * Logout from ConTroll
 * @return a promise that resolves to a boolean specifying whether the logout
 * succeeded or not
 */
ConTrollAuth.prototype.logout = function(callback) {
	return this.api.send('auth/logout').then(() => true).catch(() => false);
}

/**
 * Get the current user's identification data, if logged in
 * @return a promise that resolves with the ID object
 */
ConTrollAuth.prototype.id = function() {
	return this.api.send('auth/id');
};

/**
 * Retrieve the current user convention role, if exists
 * @return a promise that resolves to the role of the logged in user, or
 * null if they have no role
 */
ConTrollAuth.prototype.role = function(callback) {
	this.id().then((res => {
		return this.api.send('entities/managers/' + res.id, { convention: true })
			.then(res => res.role);
	}).bind(this));
};

module.exports = ConTrollAuth;

},{}],2:[function(require,module,exports){
'use strict';

/**
 * Javascript SDK for the Con-Troll API
 */

const Auth = require('./auth');
const Users = require('./users');
const Conventions = require('./conventions');
const UserRecords = require('./convention/user-records');
const Tags = require('./convention/tags');
const Events = require('./convention/events');
const Timeslots = require('./convention/timeslots');
const Tickets = require('./convention/tickets');
const Passes = require('./convention/passes');
const UserPasses = require('./convention/user-passes');
const CouponTypes = require('./convention/coupon-types');
const Coupons = require('./convention/coupons');
const Locations = require('./convention/locations');
const Merchandise = require('./convention/merchandise');
const Purchases = require('./convention/purchases');
const Managers = require('./convention/managers');

/**
 * Helper method to parse incorrectly parsed JSON responses
 * @param reqObj XMLHTTPRequest object
 * @returns response object or parsed JSON
 */
function readResponse(reqObj) {
	if (reqObj.responseType == 'json')
		return reqObj.response;
	try {
		return JSON.parse(reqObj.responseText);
	} catch (e) {
		return false;
	}
}

/**
 * API Constructor
 * you shouldn't need to call this - the API is initialized by the main
 * or browser entry points
 */
const ConTroll = function(window, endpoint) {
	this.endpoint = endpoint || 'http://api.con-troll.org';
	// auto detect testing environment
	if (!endpoint && window && window.location && window.location.host.match(/localhost/))
		this.endpoint = 'http://localhost:8080';
	
	this.auth = new Auth(this);
	this.users = new Users(this);
	this.conventions = new Conventions(this);
	this.records = new UserRecords(this);
	this.tags = new Tags(this);
	this.events = new Events(this);
	this.timeslots = new Timeslots(this);
	this.tickets = new Tickets(this);
	this.passes = new Passes(this);
	this.userpasses = new UserPasses(this);
	this.coupontypes = new CouponTypes(this);
	this.coupons = new Coupons(this);
	this.locations = new Locations(this);
	this.merchandise = new Merchandise(this);
	this.purchases = new Purchases(this);
	this.managers = new Managers(this);

	if (window) {
		this.storage = window.sessionStorage;
		this.handleAuth(window);
	}
	
	return this;
};

/* -- Browser specific routines -- these will not work in the server
 * -- though these are mostly about user authentication, so they shouldn't
 * -- interest server developers
 */

/**
 * When running in the browser, detect if the API called our window with
 * an complete authentication token, in which case store it
 */
ConTroll.prototype.handleAuth = function(w) {
	this.startedIDLookup = true;
	let token = null;
	// check if we got here from a ConTroll redirect 
	w.location.search.split(/[\?\&]+/).forEach(function(f){
		let kv = f.split('='); 
		if (kv[0] == 'token')
			token = decodeURIComponent(kv[1]);
	});
	if (token) {
		// store token locally and reload without the query
		this.storage.setItem('controll-token', token);
		this.authtoken = token;
		if (w.history.pushState) {
			w.history.pushState({controll_token: token}, "authenticated", w.location.pathname);
		} else {
			w.location = w.location.pathname
		}
	}
	
	if (!this.authToken) {
		this.authToken =  w.sessionStorage.getItem('controll-token');
	}
	// load user data from Controll
	this.auth.id().then((res => {
		if (res.status == false)
			return; // no ID
		this.storage.setItem('controll-name', res['name']);
		this.storage.setItem('controll-email', res['email']);
	}).bind(this));
};

/**
 * Check if the user is authenticated with the ConTroll API.
 * If the user is not authenticated, this method will start the authentication
 * process and return to the current URL, instead of resolving the promise.
 * @return a promise that will resolve if the user is authenticated.
 */
ConTroll.prototype.ifAuth = function() {
	if (!this.getAuthToken())
		this.auth.startSelect(window.location.href);
	return this.auth.verify().then((loggedin => {
		if (loggedin)
			return;
		this.auth.startSelect(window.location.href);
	}).bind(this)).capture((() => this.auth.startSelect(window.location.href)).bind(this));
};

/**
 * Change the current window to another URL, authenticating first, if needed.
 * This method does not return
 */
ConTroll.prototype.authAndGo = function(url) {
	this.auth.verify().then((loggedin => {
		if (loggedin) {
			window.location.href = url;
			return;
		}
		
		this.auth.startSelect(url);
	}).bind(this));
};

/**
 * Perform a cashout for the current cart of the specified user
 * @param userid Number ID of user
 * @param amount Number payout received
 * @return a promise that will be resolved when cashout completes
 */
ConTroll.cashout = function(userid, amount) {
	return this.send('checkout/cashout', { user: userid, amount: amount }, 
			{ convention: true }, 'POST');
};

/* -- Non-browser specific methods */

/**
 * Retrieve the current API endpoint URL
 * @return endpoint URL
 */
ConTroll.getEndpoint = function() {
	return api.endpoint;
};

/**
 * Retrieve user's name and email from auth storage
 * @return a property list with the 'name' and 'email' properties
 */
ConTroll.prototype.userDetails = function() {
	return {
		name: this.storage ? this.storage.getItem('controll-name') : null,
		email: this.storage ? this.storage.getItem('controll-email') : null,
	};
};

/**
 * Retrieve the current user's email, from either local storage
 * or by querying the server
 * @return a promise that will resolve with the user's email, if the user
 *   is logged in, or false otherwise
 */
ConTroll.prototype.getUserEmail = function() {
	if (this.storage && this.storage.getItem('controll-email'))
		return new Promise((
				(accept,reject) => accept(this.storage.getItem('controll-email'))
				).bind(this));
	
	return this.auth.id().then(res => res.status ? res.email : false);
};

/**
 * Retrieve the current log in authentication token
 * @return the authentication token if it is known, false otherwise
 */
ConTroll.prototype.getAuthToken = function() {
	if (this.authToken)
		return this.authToken;
	if (this.storage && this.storage.getItem('controll-token'))
		return this.storage.getItem('controll-token');
	return false;
};

ConTroll.prototype.rollbar = function() {
	return this.Rollbar || window.Rollbar;
};

/**
 * Set the convention API key used for convention specific API calls
 * @param api_key String convention's public API key
 */
ConTroll.prototype.setConvention = function(api_key) {
	api.conventionApiKey = api_key;
};

/**
 * main call routine
 * Currently optimized for web browsers
 * TODO: convert to use require('http') and let browserify implement the logic
 */
ConTroll.prototype.send = function(action, data, auth, method) {
	if (typeof arguments[1] == 'object') { // data is optional
		method = arguments[2];
		auth = arguments[1];
		data = null;
	}
	var req = new XMLHttpRequest();
	return new Promise(((resolve, reject) => {
		req.onreadystatechange = function() {
			if (req.readyState == 4) {// DONE
				var res = readResponse(req);
				if (req.status == 200)
					return resolve(res);
				
				if (res.status / 100 == 5) // report server errors to Rollbar
					this.rollbar().error("ConTroll API error:", res || req.responseText || req);
				if (typeof res != 'object') // this not an error reported by the server
					// - its a server error or a network error, fake sth for the caller
					res = { status: false, error: req.responseText || "CORS error" };
				
				console.error(res.error);
				reject(res);
			}
		}.bind(this);
		
		req.withCredentials = true; // allow CORS cookies that are required for the PHP session
		if (data) {
			req.open(method || 'POST', this.endpoint + '/' + action);
			req.setRequestHeader("Content-type","application/json");
			// req.setRequestHeader("Access-Control-Request-Method", "POST");
		} else {
			req.open(method || 'GET', this.endpoint + '/' + action);
		}
		req.setRequestHeader("Cache-Control","no-cache");
		//req.setRequestHeader("Origin",window.location.host);
		if (this.authToken) {
			//req.setRequestHeader('Access-Control-Request-Headers', 'Authorization');
			req.setRequestHeader('Authorization', this.authToken);
		}
		if (auth) {
			if (auth.convention) {
				if (auth.convention === true) auth.convention = this.conventionApiKey;
				req.setRequestHeader('Convention', auth.convention);
			} 
		}
		if (data) {
			req.send(JSON.stringify(data));
		} else {
			req.send();
		}
	}).bind(this));
};

/**
 * Retreive a record or a catalog of collection from the server
 * @param collection Collection to retrieve
 * @param record_id records to lookup - one of:
 *  * a specific ID (Number or String) - get a single object
 *  * false - retrieve the entire collection catalog
 *  * a property list containing filter KV pairs - retrieve a filtered catalog
 * @return promise that will resolve with the response object or reject with an error object
 */
ConTroll.prototype.get = function(collection, record_id) {
	var auth = {};
	if (typeof collection == 'object') {
		for (var field in collection) {
			if (field == 'collection') continue;
			auth[field] = collection[field];
		}
		collection = collection.collection;
	}
	var uri = 'entities/' + collection + '/';
	if (typeof record_id == 'object') {
		var filters = [];
		for (var field in record_id) {
			filters.push(encodeURIComponent('by_' + field) + '=' + encodeURIComponent(record_id[field]));
		}
		uri += '?' + filters.join('&');
	} else if (record_id)
		uri += record_id
	return this.send(uri, auth);
};

/**
 * Create a new record in the specifeid collection
 * @param collection String|Object collection where the object is to be created
 * @param data Object list of proeprties for the new object
 * @return a promise that will resolve or reject with the server response
 */
ConTroll.prototype.create = function(collection, data) {
	var auth = {};
	if (typeof collection == 'object') {
		for (var field in collection) {
			if (field == 'collection') continue;
			auth[field] = collection[field];
		}
		collection = collection.collection;
	}
	return this.send('entities/' + collection, data, auth);
};

/**
 * Update an existing record in the specified collection
 * @param collection String|Object collection where the object is to be udpated
 * @param id Number|String id of the object to be updated
 * @param data Object list of fields to send in the update
 * @return a promise that will resolve or reject with the server response
 */
ConTroll.prototype.update = function(collection, id, data) {
	var auth = {};
	if (typeof collection == 'object') {
		for (var field in collection) {
			if (field == 'collection') continue;
			auth[field] = collection[field];
		}
		collection = collection.collection;
	}
	return this.send('entities/' + collection + '/' + id, data, auth, 'PUT');
};

/**
 * Delete an existing record in the specified collection
 * @param collection String|Object collection where the object is to be deleted
 * @param id Number|String id of the object to be deleted
 * @return a promise that will resolve or reject with the server response
 */
ConTroll.prototype.del= function(collection, id) {
	var auth = {};
	if (typeof collection == 'object') {
		for (var field in collection) {
			if (field == 'collection') continue;
			auth[field] = collection[field];
		}
		collection = collection.collection;
	}
	return this.send('entities/' + collection + '/' + id, null, auth, 'DELETE');
};

module.exports = ConTroll;

},{"./auth":1,"./convention/coupon-types":4,"./convention/coupons":5,"./convention/events":6,"./convention/locations":7,"./convention/managers":8,"./convention/merchandise":9,"./convention/passes":10,"./convention/purchases":11,"./convention/tags":12,"./convention/tickets":13,"./convention/timeslots":14,"./convention/user-passes":15,"./convention/user-records":16,"./conventions":17,"./users":18}],3:[function(require,module,exports){
'use strict';

const rollbar = require('rollbar-browser');

const rollbarConfig = {
	accessToken: "35db6413e9984266a607b2c31e376db3",
	captureUncaught: true,
	captureUnhandledRejections: false,
	payload: {
		environment: ((window||{}).location||{}).host||'local',
	}
};

const Rollbar = rollbar.init(rollbarConfig);
const ctr = new (require('./base'))(window);
ctr.Rollbar = Rollbar;

module.exports = ctr;

},{"./base":2,"rollbar-browser":19}],4:[function(require,module,exports){
/**
 * http://usejsdoc.org/
 */

'use strict';

/**
 * ConTroll Coupon Types API
 * @param ConTroll api
 */
var ConTrollCouponTypes = function(api) {
	this.api = api;
	this.collection = {convention: true, collection: 'coupontypes'};
	return this;
};

/**
 * Retrieve the list of all coupon types in the convention
 * @return a promise that will resolve to the list of coupon type objects
 */
ConTrollCouponTypes.prototype.catalog = function() {
	return this.api.get(this.collection);
};

/**
 * Add a new coupon type to the convention
 * @param title String title of the new coupon type
 * @param value Number|String monetary value of the new coupon type
 * @param category String name of the category this type belongs to
 * @param code String activation code to allow users to create coupons
 * @return a promise that will resolve to the new coupon type object created
 */
ConTrollCouponTypes.prototype.add = function(title, value, category, code) {
	var data = {
			title: title,
			type: 'fixed',
			value: value,
			category: category,
			code: code,
			multiuse: false
	};
	return this.api.create(this.collection, data);
};

/**
 * Remove a coupon type from the convention
 * @parm id Number id of the coupon type to remove
 * @return a promise that will resolve to a property list containing a boolean status field
 */
ConTrollCouponTypes.prototype.remove = function(id) {
	return this.api.del(this.collection, id);
}

module.exports = ConTrollCouponTypes;

},{}],5:[function(require,module,exports){
/**
 * http://usejsdoc.org/
 */

'use strict';

/**
 * ConTroll Coupons API
 * @param ConTroll api
 */
var ConTrollCoupons = function(api) {
	this.api = api;
	this.collection = {convention: true, collection: 'coupons'};
	return this;
};

/**
 * Retrieve a list of all coupons in the convention
 * @return a promise that will resolve to the list of coupon objects
 */
ConTrollCoupons.prototype.catalog = function() {
	return this.api.get(this.collection);
};

/**
 * Retrieve a list of all coupons for a specified coupon type
 * @param typeId Number id of the coupon type
 * @return a promise that will resolve to the list of coupon objects for the specifeid
 *   type
 */
ConTrollCoupons.prototype.forType = function(typeId) {
	return this.api.get(this.collection, { type: typeId });
};

/**
 * Add a new coupon for a user
 * @param typeId Number id of the coupon type to add
 * @param user String|Number id or email of the user to add a coupon for
 * @return a promise that will resolve to the new coupon object created
 */
ConTrollCoupons.prototype.add = function(typeId, user) {
	var data = {
			type: typeId,
			user: user
		};
	return this.api.create(this.collection, data);
};

/**
 * Remove an existing coupon from the convention
 * @param id Number id of the coupon to remove
 * @return a promise that will resolve to a property list containing a boolean status field
 */
ConTrollCoupons.prototype.remove = function(id) {
	return this.api.del(this.collection, id);
};

module.exports = ConTrollCoupons;

},{}],6:[function(require,module,exports){
/**
 * http://usejsdoc.org/
 */

'use strict';

/**
 * ConTroll Events API
 * @param ConTroll api
 */
var ConTrollEvents = function(api) {
	this.api = api;
	this.collection = {convention: true, collection: 'events'};
	return this;
};

/**
 * List all events in the convention
 * @return a promise that will resolve with the list of event objects
 */
ConTrollEvents.prototype.catalog = function() {
	return this.api.get(this.collection);
};

/**
 * Retrieve a specific event by ID
 * @param id Number id of the event to retrieve
 * @return a promise that will resolve with the event object retreived
 */
ConTrollEvents.prototype.get = function(id) {
	return this.api.get(this.collection, id);
};

/**
 * Create a new event in the convention
 * @param title String title of the event to create
 * @param teaser String teaser text for the event
 * @param duration Number default duration for the event in minutes
 * @param ownerId Number the user ID who suggested/owns this event
 * @return a promise that will resolve with the new event object
 */
ConTrollEvents.prototype.create = function(title, teaser, duration, ownerId) {
	return this.api.create(this.collection, {
		title: title,
		teaser: teaser,
		duration: duration,
		user: { id: ownerId }
	});
};

/**
 * Update an existing event with new data
 * @param id Number id of the event to update
 * @param fields Object a list of fields to update on the event
 * @return a promise that will resolve to the updated event object
 */
ConTrollEvents.prototype.update = function(id, fields) {
	return this.api.update(this.collection, id, fields);
};

/**
 * Remove an existing event
 * @param id Number id of the event to remove
 * @return a promise that will resolve to a property list with a boolean status field
 */
ConTrollEvents.prototype.remove = function(id) {
	return this.api.del(this.collection, id);
};

module.exports = ConTrollEvents;

},{}],7:[function(require,module,exports){
/**
 * http://usejsdoc.org/
 */

'use strict';

/**
 * ConTroll Locations API
 * @param ConTroll api
 */
var ConTrollLocations = function(api) {
	this.api = api;
	this.collection = {convention: true, collection: 'locations'};
	return this;
};

/**
 * Retrieve the list of all locations in the current convention
 * @return a promise that will resolve to the list of location objects
 */
ConTrollLocations.prototype.catalog = function() {
	return this.api.get(this.collection);
};

/**
 * Add a new location to the convention
 * @param title String title for the new location
 * @param area String area description for the new location
 * @param max_attendees Number maximum number of attendees the new location supports
 * @return a promise that will resolve to the new location object created
 */
ConTrollLocations.prototype.add = function(title, area, max_attendees) {
	return this.api.create(this.collection, {
		title: title,
		area: area,
		"max-attendees": max_attendees
	});
}

/**
 * Update an existing location in the convention
 * @param id Number id of the location to update
 * @pram title String new title to set for this location
 * @param area String new area description to set for this location
 * @param max_attendees Number new maximum number of attendees allowed in this location
 * @return a promise that will resolve to a property list containing a boolean status field
 */
ConTrollLocations.prototype.update = function(id, title, area, max_attendees) {
	return this.api.update(this.collection, id, {
		title: title,
		area: area,
		"max-attendees": max_attendees
	});
}

/**
 * Remove an existing location from the convention
 * @param id Number id of the location to remove
 * @return a promise that will resolve to a property list contianing a boolean status field
 */
ConTrollLocations.prototype.remove = function(id) {
	return this.api.del(this.collection, id);
}

module.exports = ConTrollLocations;

},{}],8:[function(require,module,exports){
/**
 * http://usejsdoc.org/
 */

'use strict';

/**
 * ConTroll Managers API
 * @param ConTroll api
 */
var ConTrollManagers = function(api) {
	this.api = api;
	this.collection = {convention: true, collection: 'managers'};
	return this;
};

/**
 * Return a list of all convention management users (for all roles)
 * @return a promise that will resolve with the list of all management objects
 */
ConTrollManagers.prototype.catalog = function() {
	return this.api.get(this.collection);
};

/**
 * Add a new management role to the convention
 * @param userObj Object a management object to create
 * @return a promise that will resolve with the new management object
 */
ConTrollManagers.prototype.add = function(userObj) {
	return this.api.create(this.collection, userObj);
};

/**
 * Remove a management role from the convention
 * @param id Number id of management object to remove
 * @return a promise that will resolve with a property list containing a boolean status field
 */
ConTrollManagers.prototype.remove = function(id) {
	this.api.del(this.collection, id);
};

module.exports = ConTrollManagers;

},{}],9:[function(require,module,exports){
/**
 * http://usejsdoc.org/
 */

'use strict';

/**
 * ConTroll Merchandise API
 * @param ConTroll api
 */
var ConTrollMerchandise = function(api) {
	this.api = api;
	this.collection = {convention: true, collection: 'merchandiseskus'};
	return this;
};

/**
 * Retrieve the catalog of all currently available merchandise in the convention
 * @return a promise that will resolve with the list of merchandise objects
 */
ConTrollMerchandise.prototype.catalog = function() {
	return this.api.get(this.collection);
};

/**
 * Create a new merchandise item for the convention
 * @param title String the title for the new item
 * @param code String an SKU properties code - a colon delimited list of properties
 * @param price Number|String price of the new item
 * @param description String a long textual description of the new item
 * @return a promise that will resolve with the new merchandise item object
 */
ConTrollMerchandise.prototype.create = function(title, code, price, description) {
	var data = {
			title: title,
			code: code,
			price: price,
			description: description
		};
	return this.api.create(this.collection, data);
};

/**
 * Remove an existing merchandise item from the convention
 * @param id Number id of the merchandise item to remove
 * @return a promise that will resolve to a property list containing a boolean status field
 */
ConTrollMerchandise.prototype.remove = function(id) {
	return this.api.del(this.collection, id);
};

/**
 * Upadte an existing merchandise object in the convention
 * @param id Number id of the merchandise item to update
 * @param title String new title to set for the merchandise item
 * @param price Number|String new price to set for the merchandise item
 * @return a promise that will resolve to a property list containing a boolean status field
 */
ConTrollMerchandise.prototype.update = function(id, title, price) {
	return this.api.update(this.collection, id, {
		title: title,
		price: price
	});
};

module.exports = ConTrollMerchandise;

},{}],10:[function(require,module,exports){
/**
 * http://usejsdoc.org/
 */

'use strict';

/**
 * ConTroll Pass API
 * @param ConTroll api
 */
const ConTrollPasses = function(api) {
	this.api = api;
	this.collection = { convention: true, collection: 'passes' };
	return this;
};

/**
 * Retrieve the list of all available passes (including non-public ones)
 * @return a promise that will resolve to the list of pass objects
 */
ConTrollPasses.prototype.catalog = function() {
	return this.api.get(this.collection, '?all=1');
};

module.exports = ConTrollPasses;

},{}],11:[function(require,module,exports){
/**
 * http://usejsdoc.org/
 */

'use strict';

/**
 * ConTroll Purchases API
 * @param ConTroll api
 */
var ConTrollPurchases = function(api) {
	this.api = api;
	this.collection = {convention: true, collection: 'purchases'};
	return this;
};

/**
 * Retrieve the list of purchased items in the convention
 * @return a promise that will resolve to the list of purchase objects
 */
ConTrollPurchases.prototype.catalog = function() {
	return this.api.get(this.collection, '?all=1');
};

module.exports = ConTrollPurchases;

},{}],12:[function(require,module,exports){
/**
 * http://usejsdoc.org/
 */

'use strict';

/**
 * ConTroll System Tags API
 * @param ConTroll api
 */
var ConTrollTags = function(api) {
	this.api = api;
	this.collection = {convention: true, collection: 'tagtypes'};
	return this;
};

/**
 * Retrieve the list of system tags
 * @return a promise that will resolve to the list of system tag objects
 */
ConTrollTags.prototype.catalog = function() {
	return this.api.get(this.collection);
};

/**
 * Return a specific system tag
 * @param title String title or slug of the system tag to retrieve
 * @return a promise that will resolve to the system tag object
 */
ConTrollTags.prototype.getType = function(title) {
	return this.api.get(this.collection, title);
};

/**
 * Add a system tag to the convention
 * @param title String title of the system tag to add
 * @param requirement String the type of requirement for this tag. One of:
 *  * Optional
 *  * One
 *  * Multiple
 * @param ispublic Boolean whether the new tag is to be exposed to non-managers
 * @return a promise that will resolve to a property list with a boolean status field
 */
ConTrollTags.prototype.addType = function(title, requirement, ispublic) {
	return this.api.create(this.collection, {
		title: title,
		requirement: requirement,
		"public": ispublic
	});
};

/**
 * Update an existing system tag
 * @param oldtitle String title or slug of the system tag to update
 * @param newtitle String update title to change to
 * @param requirement String updated requirement to change to (see #addType for details)
 * @param ispublic Boolean new public/private state to set
 * @return a promise that will resolve to a property list with a boolean status field
 */
ConTrollTags.prototype.updateType = function(oldtitle, newtitle, requirement, ispublic) {
	return this.api.update(this.collection, oldtitle, {
		title: newtitle,
		requirement: requirement,
		"public": ispublic
	});
};

/**
 * Remove and existing system tag
 * @param title String title or slug of the system tag to remove
 * @return a promise that will resolve to a property list with a boolean status field
 */
ConTrollTags.prototype.deleteType = function(title) {
	return this.api.del(this.collection, title);
};

/**
 * Replace an option value in an existing system tag
 * @param title String title or slug of the system tag where a value is to be updated
 * @param oldvalue String old value to replace
 * @param newvalue String new value to use instead
 * @return a promise that will resolve to a property list with a boolean status field
 */
ConTrollTags.prototype.replaceValue = function(title, oldvalue, newvalue) {
	var replace_values = {};
	replace_values[oldvalue] = newvalue;
	return this.api.update(this.collection, title, {
		"replace-values": replace_values
	});
};

/**
 * Add a new option value to an existing system tag
 * @param title String title or slug of the system tag where a value is to be updated
 * @param newvalue String new value to add as an option to the system tag
 * @return a promise that will resolve to a property list with a boolean status field
 */
ConTrollTags.prototype.addValue = function(title, newvalue) {
	return this.api.update(this.collection, title, {
		values: [ newvalue ]
	});
};

/**
 * Delete an existing option on an existing system tag
 * @param title String title or slug of the system tag where a value is to be updated
 * @param value String the existing option value to remove
 * @return a promise that will resolve to a property list with a boolean status field
 */
ConTrollTags.prototype.deleteValue = function(title, value) {
	return this.api.update(this.collection, title, {
		"remove-values": [ value ]
	});
};

module.exports = ConTrollTags;

},{}],13:[function(require,module,exports){
/**
 * http://usejsdoc.org/
 */

'use strict';

/**
 * ConTroll Tickets API
 * @param ConTroll api
 */
var ConTrollTickets = function(api) {
	this.api = api;
	this.collection = {convention: true, collection: 'tickets'};
	return this;
};

/**
 * Retrieve all tickets authorized for the specified event
 * @param eventId Number id of the event to list tickets for
 * @return a promise that will resolve to the list of ticket objects
 */
ConTrollTickets.prototype.forEvent = function(eventId) {
	return this.api.get(this.collection, '?all=1&is_valid=1&by_event=' + eventId);
};

/**
 * Retrieve all tickets authorized for the specified time slot
 * @param timeslotId Number id of the timeslot to list tickets for
 * @return a promise that will resolve to the list of ticket objects
 */
ConTrollTickets.prototype.forTimeslot = function(timeslotId) {
	return this.api.get(this.collection, '?all=1&is_valid=1&by_timeslot=' + timeslotId);
};

/**
 * Retrieve all tickets authorized for the specified user
 * @param userId Number id of the user to list tickets for
 * @return a promise that will resolve to the list of ticket objects
 */
ConTrollTickets.prototype.forUser = function(userId) {
	return this.api.get(this.collection, '?all=1&is_valid=1&by_user=' + userId);
};

/**
 * Retrieve all tickets authorized in the convention
 * @return a promise that will resolve to the list of ticket objects
 */
ConTrollTickets.prototype.catalog = function() {
	return this.api.get(this.collection, '?all=1&is_valid=1');
};

/**
 * Register a ticket for the specified user, on the specified timeslot with 
 * the specified pass.
 * @param user Number|String id or email of the user to register a ticket for
 * @param timeslot Number id of the timeslot to register a ticket for
 * @param passId Number|Array[Number] id of the pass or passes to register tickets for
 * @return a promise that will resolve to the newly created ticket or a list of ticktes
 */
ConTrollTickets.prototype.create = function(user, timeslot, passId) {
	var data = {
			timeslot: timeslot,
			user_passes: passId,
			user: user
	};
	return this.api.create(this.collection, data);
};

/**
 * Remove a ticket from registration
 * @param id Number id of the ticket to cancel
 * @para, refundTypeId Number id of the coupon type to create as a refund, if needed
 * @return a promise that will resolve to a property list containing a boolean status field
 */
ConTrollTickets.prototype.remove = function(id, refundTypeId) {
	var query = id;
	if (refundTypeId) 
		query += '?refund-coupon-type='+ refundTypeId;
	return this.api.del(this.collection, query);
};

module.exports = ConTrollTickets;

},{}],14:[function(require,module,exports){
/**
 * http://usejsdoc.org/
 */

'use strict';

/**
 * ConTroll Time Slots API
 * @param ConTroll api
 */
var ConTrollTimeslots = function(api) {
	this.api = api;
	this.collection = {convention: true, collection: 'timeslots'};
	return this;
};

/**
 * Retrieve all timeslots for the convention
 * @return a promise that will resolve to a list of timeslot objects
 */
ConTrollTimeslots.prototype.catalog = function() {
	return this.api.get(this.collection);
};

/**
 * Retrieve all timeslots for a specific event
 * @param event_id Number id of the event to retrieve timeslots for
 * @return a promise that will resovle to a list of timeslot objects
 */
ConTrollTimeslots.prototype.forEvent = function(event_id) {
	return this.api.get(this.collection, { event: event_id });
};

/**
 * Create a new timeslot for an event
 * @param event_id Number id of the event to create a timeslot for
 * @return a promise that will resolve to the created timeslot object
 */
ConTrollTimeslots.prototype.create = function(event_id, start_time, duration, locations, hosts) {
	var data = {
			event: event_id,
			start: parseInt(start_time.getTime()/1000),
			duration: duration,
			locations: locations
		};
	if (hosts && hosts.length > 0)
		// submit hosts directly - assumes a host is an object with either email or id fields and optionally a name - like api wants 
		data.hosts = hosts; 
	return this.api.create(this.collection, data);
};

/**
 * Remove an existing timeslot
 * @param id Number id of the timeslot to remove
 * @return a promise that will resolve to a property list with a boolean status field
 */
ConTrollTimeslots.prototype.remove = function(id) {
	return this.api.del(this.collection, id);
};

/**
 * Update the timeslot location list
 * @param id Number id of the timeslot to update
 * @param toAdd Array[Object|String] list of location objects or location slugs to add
 *   to the timeslot
 * @param toRemove Array[Object|String] list of location objects or location slugs to
 *   remove from the timeslot
 * @return a promise that will resolve to a property list with a boolean status field
 */
ConTrollTimeslots.prototype.updateLocations = function(id, toAdd, toRemove) {
	return this.api.update(this.collection, id, {
		'locations': toAdd.map(function(l){ return l.slug ? l.slug : l; }),
		'remove-locations': toRemove.map(function(l){ return l.slug ? l.slug : l; })
	});
};

/**
 * Update the host list for the timeslot
 * @param id Number id of the timeslot to update
 * @param toAdd Array[Object] list of timeslot host objects to add to the timeslot
 * @param toRemove Arrau[Object] list of timeslot host objects to remove from the
 *   timeslot
 * @return a promise that will resolve to a property list with a boolean status field 
 */
ConTrollTimeslots.prototype.updateHosts = function(id, toAdd, toRemove) {
	return this.api.update(this.collection, id, {
		'hosts': toAdd,
		'remove-hosts': toRemove
	});
};

module.exports = ConTrollTimeslots;

},{}],15:[function(require,module,exports){
/**
 * http://usejsdoc.org/
 */

'use strict';

/**
 * ConTroll User Pass API
 * @param ConTroll api
 */
var ConTrollUserPasses = function(api) {
	this.api = api;
	this.collection = {convention: true, collection: 'userpasses'};
	return this;
};

/**
 * Retrieve the list of all valid user passes for all users
 * @return a promise that will resolve to the list of user pass objects
 */
ConTrollUserPasses.prototype.catalog = function() {
	return this.api.get(this.collection, '?is_valid=1&all=1');
};

/**
 * Retrieve all passes for a user
 * @param userId ID to retrieve a report for
 * @param valid boolean specifying if only valid passes are to be retrieved
 * @return a promise that will resolve tot he list of user pass objects
 */
ConTrollUserPasses.prototype.userReport = function(userId, valid){
	var params = '?user=' + userId;
	if (valid)
		params += '&is_valid=1';
	return this.api.get(this.collection, params);
};

/**
 * Retrieve all passes for a user, including availability report for the specified timeslot
 * @param userId ID to retrieve a report for
 * @param timeslot ID of timeslot to get availabitlity for
 * @return a promise that will resolve to the list of all user pass objects
 */
ConTrollUserPasses.prototype.userTimeslotReport = function(userId, timeslot) {
	return this.api.get(this.collection, '?user=' + userId + '&for_timeslot=' + timeslot);
};

/**
 * Cancel or refund an existing pass
 * @param passId ID of the user pass to cancel
 * @return a promise that will resolve to a property list containing a boolean status field
 */
ConTrollUserPasses.prototype.remove = function(passId) {
	return this.api.del(this.collection, passId);
};

/**
 * Reserve a user pass in the shopping cart
 * @param userId ID of the user to reserve a pass for
 * @param name pass holder name to list on the pass
 * @param passId Id of pass type to reserve
 * @return a promise that will resolve to the newly created user pass object
 */
ConTrollUserPasses.prototype.create = function(userId, name, passId) {
	var data = {
			pass: passId,
			user: userId,
			name: name
	};
	return this.api.create(this.collection, data);
};

module.exports = ConTrollUserPasses;

},{}],16:[function(require,module,exports){
/**
 * http://usejsdoc.org/
 */

'use strict';

/**
 * User records API
 */
var ConTrollRecords = function(api) {
	this.api = api;
	return this;
};

/**
 * Retrieve the user data record for the specified descriptor
 * @param descriptor String text label associated with the data
 * @return promise that will resolev with the data object originally
 *   stored on that descriptor, or null if no such data was stored
 */
ConTrollRecords.prototype.get = function(descriptor) {
	return this.api.get({convention: true, collection: 'records'}, descriptor)
		.then(res => data ? JSON.parse(data.data) : null);
};

/**
 * Store a user data record for the convention on the specified descriptior
 * @param descriptior String text label to associate with the data
 * @param data A Javascript value to store
 * @param acl a string describing access permissions. Valid values are
 *   'private' (the default) and 'public'
 * @return a rpomise that will resolve to true if the data was successfully
 *   stored
 */
ConTrollRecords.prototype.save = function(descriptor, data, acl) {
	if (arguments.length == 3) { // acl is optional
		callback = acl;
		acl = null;
	}
	if (!acl) acl = 'private';
	return this.api.create({convention: true, collection: 'records'}, {
		descriptor: descriptor,
		content_type: 'application/json',
		acl: acl,
		data: JSON.stringify(data)
	}).then(() => true);
};

module.exports = ConTrollRecords;

},{}],17:[function(require,module,exports){
/**
 * http://usejsdoc.org/
 */

'use strict';

/**
 * ConTroll Conventions API
 * @param ConTroll api
 */
var ConTrollConventions = function(api) {
	this.api = api;
	this.collection = {convention: true, collection: 'conventions'};
	return this;
};

/**
 * List all conventions
 * @return a promise that will resolve to the list of all convention objects managed
 *   by the current user, including their keys
 */
ConTrollConventions.prototype.catalog = function() {
	return this.api.get(this.collection, '?keys=1&manager=1');
};

/**
 * Create a new convention
 * @param title Title of the convention
 * @param series Name of a convention series if this instance is a yearly convention
 * @param location Physical location (such as an address) where the convention will be held
 * @param website URL of the convention website
 * @return a promise that will resolve to the newly created convention object
 */
ConTrollConventions.prototype.create = function(title, series, location, website) {
	var data = {
			title: title,
			series: series,
			location: location,
			website: website
	};
	return this.api.create(this.collection, data);
};

/**
 * Retrieve the current convention's details
 * @return a promise that will resolve to the currently selected convnetion object
 */
ConTrollConventions.prototype.getCurrent = function() {
	return this.api.get(this.collection, 'self');
};

/**
 * Update convention settings
 * @param id convention ID to update
 * @param data property list with data to update
 * @return a promise that will resolve to the updated convention object
 */
ConTrollConventions.prototype.update = function(id, data) {
	return this.api.update(this.collection, id, data);
};

module.exports = ConTrollConventions;

},{}],18:[function(require,module,exports){
/**
 * http://usejsdoc.org/
 */

'use strict';

/**
 * ConTroll Users API
 * @param ConTroll api
 */
var ConTrollUsers = function(api) {
	this.api = api;
	this.collection = {convention: true, collection: 'users'};
	return this;
};

/**
 * Retrieve all user profiles in the system
 * @return a promise that will resolve with the list of user profile objects
 */
ConTrollUsers.prototype.catalog = function() {
	return this.api.get(this.collection);
};

/**
 * Retrieve a user report for the specified user
 * @param id User ID to retrieve
 * @return a promise that will resolve with the requested user profile object
 */
ConTrollUsers.prototype.get = function(id) {
	return this.api.get(this.collection, id);
};

/**
 * Create a new user profile
 * @param name String full name of the user
 * @param email String email address of the user
 * @return a promise that will resolve with the new user profile object
 */
ConTrollUsers.prototype.create = function(name, email) {
	return this.api.create(this.collection, {
		name: name,
		email: email,
	});
}

module.exports = ConTrollUsers;

},{}],19:[function(require,module,exports){
!function(e,r){if("object"==typeof exports&&"object"==typeof module)module.exports=r();else if("function"==typeof define&&define.amd)define([],r);else{var t=r();for(var n in t)("object"==typeof exports?exports:e)[n]=t[n]}}(this,function(){return function(e){function r(n){if(t[n])return t[n].exports;var o=t[n]={exports:{},id:n,loaded:!1};return e[n].call(o.exports,o,o.exports,r),o.loaded=!0,o.exports}var t={};return r.m=e,r.c=t,r.p="",r(0)}([function(e,r,t){e.exports=t(1)},function(e,r,t){"use strict";function n(){var e="undefined"==typeof JSON?{}:JSON;o.setupJSON(e)}var o=t(2),i=t(3);n();var a=window._rollbarConfig,s=a&&a.globalAlias||"Rollbar",u=window[s]&&"undefined"!=typeof window[s].shimId;!u&&a?o.wrapper.init(a):(window.Rollbar=o.wrapper,window.RollbarNotifier=i.Notifier),e.exports=o.wrapper},function(e,r,t){"use strict";function n(e,r,t){!t[4]&&window._rollbarWrappedError&&(t[4]=window._rollbarWrappedError,window._rollbarWrappedError=null),e.uncaughtError.apply(e,t),r&&r.apply(window,t)}function o(e,r){if(r.hasOwnProperty&&r.hasOwnProperty("addEventListener")){var t=r.addEventListener;r.addEventListener=function(r,n,o){t.call(this,r,e.wrap(n),o)};var n=r.removeEventListener;r.removeEventListener=function(e,r,t){n.call(this,e,r&&r._wrapped||r,t)}}}var i=t(3),a=t(8),s=i.Notifier;window._rollbarWrappedError=null;var u={};u.init=function(e,r){var t=new s(r);if(t.configure(e),e.captureUncaught){var i;r&&a.isType(r._rollbarOldOnError,"function")?i=r._rollbarOldOnError:window.onerror&&!window.onerror.belongsToShim&&(i=window.onerror),window.onerror=function(){var e=Array.prototype.slice.call(arguments,0);n(t,i,e)};var u,c,l=["EventTarget","Window","Node","ApplicationCache","AudioTrackList","ChannelMergerNode","CryptoOperation","EventSource","FileReader","HTMLUnknownElement","IDBDatabase","IDBRequest","IDBTransaction","KeyOperation","MediaController","MessagePort","ModalWindow","Notification","SVGElementInstance","Screen","TextTrack","TextTrackCue","TextTrackList","WebSocket","WebSocketWorker","Worker","XMLHttpRequest","XMLHttpRequestEventTarget","XMLHttpRequestUpload"];for(u=0;u<l.length;++u)c=l[u],window[c]&&window[c].prototype&&o(t,window[c].prototype)}return e.captureUnhandledRejections&&(r&&a.isType(r._unhandledRejectionHandler,"function")&&window.removeEventListener("unhandledrejection",r._unhandledRejectionHandler),t._unhandledRejectionHandler=function(e){var r=e.reason,n=e.promise,o=e.detail;!r&&o&&(r=o.reason,n=o.promise),t.unhandledRejection(r,n)},window.addEventListener("unhandledrejection",t._unhandledRejectionHandler)),window.Rollbar=t,s.processPayloads(),t},e.exports={wrapper:u,setupJSON:i.setupJSON}},function(e,r,t){"use strict";function n(e){E=e,w.setupJSON(e),v.setupJSON(e)}function o(e,r){return function(){var t=r||this;try{return e.apply(t,arguments)}catch(e){v.consoleError("[Rollbar]:",e)}}}function i(){h||(h=setTimeout(f,1e3))}function a(){return _}function s(e){_=_||this;var r="https://"+s.DEFAULT_ENDPOINT;this.options={enabled:!0,endpoint:r,environment:"production",scrubFields:g([],s.DEFAULT_SCRUB_FIELDS),checkIgnore:null,logLevel:s.DEFAULT_LOG_LEVEL,reportLevel:s.DEFAULT_REPORT_LEVEL,uncaughtErrorLevel:s.DEFAULT_UNCAUGHT_ERROR_LEVEL,payload:{}},this.lastError=null,this.plugins={},this.parentNotifier=e,e&&(e.hasOwnProperty("shimId")?e.notifier=this:this.configure(e.options))}function u(e){window._rollbarPayloadQueue.push(e),i()}function c(e){return o(function(){var r=this._getLogArgs(arguments);return this._log(e||r.level||this.options.logLevel||s.DEFAULT_LOG_LEVEL,r.message,r.err,r.custom,r.callback)})}function l(e,r){e||(e=r?E.stringify(r):"");var t={body:e};return r&&(t.extra=g(!0,{},r)),{message:t}}function p(e,r,t){var n=m.guessErrorClass(r.message),o=r.name||n[0],i=n[1],a={exception:{class:o,message:i}};if(e&&(a.exception.description=e||"uncaught exception"),r.stack){var s,u,c,p,f,d,h,w;for(a.frames=[],h=0;h<r.stack.length;++h)s=r.stack[h],u={filename:s.url?v.sanitizeUrl(s.url):"(unknown)",lineno:s.line||null,method:s.func&&"?"!==s.func?s.func:"[anonymous]",colno:s.column},c=p=f=null,d=s.context?s.context.length:0,d&&(w=Math.floor(d/2),p=s.context.slice(0,w),c=s.context[w],f=s.context.slice(w)),c&&(u.code=c),(p||f)&&(u.context={},p&&p.length&&(u.context.pre=p),f&&f.length&&(u.context.post=f)),s.args&&(u.args=s.args),a.frames.push(u);return a.frames.reverse(),t&&(a.extra=g(!0,{},t)),{trace:a}}return l(o+": "+i,t)}function f(){var e;try{for(;e=window._rollbarPayloadQueue.shift();)d(e)}finally{h=void 0}}function d(e){var r=e.endpointUrl,t=e.accessToken,n=e.payload,o=e.callback||function(){},i=(new Date).getTime();i-L>=6e4&&(L=i,R=0);var a=window._globalRollbarOptions.maxItems,c=window._globalRollbarOptions.itemsPerMinute,l=function(){return!n.ignoreRateLimit&&a>=1&&T>=a},p=function(){return!n.ignoreRateLimit&&c>=1&&R>=c};return l()?void o(new Error(a+" max items reached")):p()?void o(new Error(c+" items per minute reached")):(T++,R++,l()&&_._log(_.options.uncaughtErrorLevel,"maxItems has been hit. Ignoring errors for the remainder of the current page load.",null,{maxItems:a},null,!1,!0),n.ignoreRateLimit&&delete n.ignoreRateLimit,void y.post(r,t,n,function(r,t){return r?(r instanceof b&&(e.callback=function(){},setTimeout(function(){u(e)},s.RETRY_DELAY)),o(r)):o(null,t)}))}var h,g=t(4),m=t(5),v=t(8),w=t(11),y=w.XHR,b=w.ConnectionError,E=null;s.NOTIFIER_VERSION="1.9.4",s.DEFAULT_ENDPOINT="api.rollbar.com/api/1/",s.DEFAULT_SCRUB_FIELDS=["pw","pass","passwd","password","secret","confirm_password","confirmPassword","password_confirmation","passwordConfirmation","access_token","accessToken","secret_key","secretKey","secretToken"],s.DEFAULT_LOG_LEVEL="debug",s.DEFAULT_REPORT_LEVEL="debug",s.DEFAULT_UNCAUGHT_ERROR_LEVEL="error",s.DEFAULT_ITEMS_PER_MIN=60,s.DEFAULT_MAX_ITEMS=0,s.LEVELS={debug:0,info:1,warning:2,error:3,critical:4},s.RETRY_DELAY=1e4,window._rollbarPayloadQueue=window._rollbarPayloadQueue||[],window._globalRollbarOptions={startTime:(new Date).getTime(),maxItems:s.DEFAULT_MAX_ITEMS,itemsPerMinute:s.DEFAULT_ITEMS_PER_MIN};var _,x=s.prototype;x._getLogArgs=function(e){for(var r,t,n,i,a,u,c=this.options.logLevel||s.DEFAULT_LOG_LEVEL,l=[],p=0;p<e.length;++p)u=e[p],a=v.typeName(u),"string"===a?r?l.push(u):r=u:"function"===a?i=o(u,this):"date"===a?l.push(u):"error"===a||u instanceof Error||"undefined"!=typeof DOMException&&u instanceof DOMException?t?l.push(u):t=u:"object"!==a&&"array"!==a||(n?l.push(u):n=u);return l.length&&(n=n||{},n.extraArgs=l),{level:c,message:r,err:t,custom:n,callback:i}},x._route=function(e){var r=this.options.endpoint,t=/\/$/.test(r),n=/^\//.test(e);return t&&n?e=e.substring(1):t||n||(e="/"+e),r+e},x._processShimQueue=function(e){for(var r,t,n,o,i,a,u,c={};t=e.shift();)r=t.shim,n=t.method,o=t.args,i=r.parentShim,u=c[r.shimId],u||(i?(a=c[i.shimId],u=new s(a)):u=this,c[r.shimId]=u),u[n]&&v.isType(u[n],"function")&&u[n].apply(u,o)},x._buildPayload=function(e,r,t,n,o){var i=this.options.accessToken,a=this.options.environment,u=g(!0,{},this.options.payload),c=v.uuid4();if(void 0===s.LEVELS[r])throw new Error("Invalid level");if(!t&&!n&&!o)throw new Error("No message, stack info or custom data");var l={environment:a,endpoint:this.options.endpoint,uuid:c,level:r,platform:"browser",framework:"browser-js",language:"javascript",body:this._buildBody(t,n,o),request:{url:window.location.href,query_string:window.location.search,user_ip:"$remote_ip"},client:{runtime_ms:e.getTime()-window._globalRollbarOptions.startTime,timestamp:Math.round(e.getTime()/1e3),javascript:{browser:window.navigator.userAgent,language:window.navigator.language,cookie_enabled:window.navigator.cookieEnabled,screen:{width:window.screen.width,height:window.screen.height},plugins:this._getBrowserPlugins()}},server:{},notifier:{name:"rollbar-browser-js",version:s.NOTIFIER_VERSION}};u.body&&delete u.body;var p={access_token:i,data:g(!0,l,u)};return this._scrub(p.data),p},x._buildBody=function(e,r,t){var n;return n=r?p(e,r,t):l(e,t)},x._getBrowserPlugins=function(){if(!this._browserPlugins){var e,r,t=window.navigator.plugins||[],n=t.length,o=[];for(r=0;r<n;++r)e=t[r],o.push({name:e.name,description:e.description});this._browserPlugins=o}return this._browserPlugins},x._scrub=function(e){function r(e,r,t,n,o,i){return r+v.redact(i)}function t(e){var t;if(v.isType(e,"string"))for(t=0;t<s.length;++t)e=e.replace(s[t],r);return e}function n(e,r){var t;for(t=0;t<a.length;++t)if(a[t].test(e)){r=v.redact(r);break}return r}function o(e,r){var o=n(e,r);return o===r?t(o):o}var i=this.options.scrubFields,a=this._getScrubFieldRegexs(i),s=this._getScrubQueryParamRegexs(i);return v.traverse(e,o),e},x._getScrubFieldRegexs=function(e){for(var r,t=[],n=0;n<e.length;++n)r="\\[?(%5[bB])?"+e[n]+"\\[?(%5[bB])?\\]?(%5[dD])?",t.push(new RegExp(r,"i"));return t},x._getScrubQueryParamRegexs=function(e){for(var r,t=[],n=0;n<e.length;++n)r="\\[?(%5[bB])?"+e[n]+"\\[?(%5[bB])?\\]?(%5[dD])?",t.push(new RegExp("("+r+"=)([^&\\n]+)","igm"));return t},x._urlIsWhitelisted=function(e){var r,t,n,o,i,a,s,u,c,l;try{if(r=this.options.hostWhiteList,t=e&&e.data&&e.data.body&&e.data.body.trace,!r||0===r.length)return!0;if(!t)return!0;for(s=r.length,i=t.frames.length,c=0;c<i;c++){if(n=t.frames[c],o=n.filename,!v.isType(o,"string"))return!0;for(l=0;l<s;l++)if(a=r[l],u=new RegExp(a),u.test(o))return!0}}catch(e){return this.configure({hostWhiteList:null}),v.consoleError("[Rollbar]: Error while reading your configuration's hostWhiteList option. Removing custom hostWhiteList.",e),!0}return!1},x._messageIsIgnored=function(e){var r,t,n,o,i,a,s,u,c;try{if(i=!1,n=this.options.ignoredMessages,!n||0===n.length)return!1;if(s=e&&e.data&&e.data.body,u=s&&s.trace&&s.trace.exception&&s.trace.exception.message,c=s&&s.message&&s.message.body,r=u||c,!r)return!1;for(o=n.length,t=0;t<o&&(a=new RegExp(n[t],"gi"),!(i=a.test(r)));t++);}catch(e){this.configure({ignoredMessages:null}),v.consoleError("[Rollbar]: Error while reading your configuration's ignoredMessages option. Removing custom ignoredMessages.")}return i},x._enqueuePayload=function(e,r,t,n){var o={callback:n,accessToken:this.options.accessToken,endpointUrl:this._route("item/"),payload:e},i=function(){if(n){var e="This item was not sent to Rollbar because it was ignored. This can happen if a custom checkIgnore() function was used or if the item's level was less than the notifier' reportLevel. See https://rollbar.com/docs/notifier/rollbar.js/configuration for more details.";n(null,{err:0,result:{id:null,uuid:null,message:e}})}};if(this._internalCheckIgnore(r,t,e))return void i();try{if(v.isType(this.options.checkIgnore,"function")&&this.options.checkIgnore(r,t,e))return void i()}catch(e){this.configure({checkIgnore:null}),v.consoleError("[Rollbar]: Error while calling custom checkIgnore() function. Removing custom checkIgnore().",e)}if(this._urlIsWhitelisted(e)&&!this._messageIsIgnored(e)){if(this.options.verbose){if(e.data&&e.data.body&&e.data.body.trace){var a=e.data.body.trace,s=a.exception.message;v.consoleError("[Rollbar]: ",s)}v.consoleInfo("[Rollbar]: ",o)}v.isType(this.options.logFunction,"function")&&this.options.logFunction(o);try{v.isType(this.options.transform,"function")&&this.options.transform(e)}catch(e){this.configure({transform:null}),v.consoleError("[Rollbar]: Error while calling custom transform() function. Removing custom transform().",e)}this.options.enabled&&u(o)}},x._internalCheckIgnore=function(e,r,t){var n=r[0],o=s.LEVELS[n]||0,i=s.LEVELS[this.options.reportLevel]||0;if(o<i)return!0;var a=this.options?this.options.plugins:{};if(a&&a.jquery&&a.jquery.ignoreAjaxErrors)try{return!!t.data.body.message.extra.isAjax}catch(e){return!1}return!1},x._log=function(e,r,t,n,o,i,a){var s=null;if(t)try{if(s=t._savedStackTrace?t._savedStackTrace:m.parse(t),t===this.lastError)return;this.lastError=t}catch(e){v.consoleError("[Rollbar]: Error while parsing the error object.",e),r=t.message||t.description||r||String(t),t=null}var u=this._buildPayload(new Date,e,r,s,n);return a&&(u.ignoreRateLimit=!0),this._enqueuePayload(u,!!i,[e,r,t,n],o),{uuid:u.data.uuid}},x.log=c(),x.debug=c("debug"),x.info=c("info"),x.warn=c("warning"),x.warning=c("warning"),x.error=c("error"),x.critical=c("critical"),x.uncaughtError=o(function(e,r,t,n,o,i){if(i=i||null,o&&v.isType(o,"error"))return void this._log(this.options.uncaughtErrorLevel,e,o,i,null,!0);if(r&&v.isType(r,"error"))return void this._log(this.options.uncaughtErrorLevel,e,r,i,null,!0);var a={url:r||"",line:t};a.func=m.guessFunctionName(a.url,a.line),a.context=m.gatherContext(a.url,a.line);var s={mode:"onerror",message:o?String(o):e||"uncaught exception",url:document.location.href,stack:[a],useragent:navigator.userAgent},u=this._buildPayload(new Date,this.options.uncaughtErrorLevel,e,s,i);this._enqueuePayload(u,!0,[this.options.uncaughtErrorLevel,e,r,t,n,o])}),x.unhandledRejection=o(function(e,r){var t,n;if(e?(t=e.message||String(e),n=e._rollbarContext):t="unhandled rejection was null or undefined!",n=n||r._rollbarContext||null,e&&v.isType(e,"error"))return void this._log(this.options.uncaughtErrorLevel,t,e,n,null,!0);var o={url:"",line:0};o.func=m.guessFunctionName(o.url,o.line),o.context=m.gatherContext(o.url,o.line);var i={mode:"unhandledrejection",message:t,url:document.location.href,stack:[o],useragent:navigator.userAgent},a=this._buildPayload(new Date,this.options.uncaughtErrorLevel,t,i,n);this._enqueuePayload(a,!0,[this.options.uncaughtErrorLevel,t,o.url,o.line,0,e,r])}),x.global=o(function(e){e=e||{};var r={startTime:e.startTime,maxItems:e.maxItems,itemsPerMinute:e.itemsPerMinute};g(!0,window._globalRollbarOptions,r),void 0!==e.maxItems&&(T=0),void 0!==e.itemsPerMinute&&(R=0)}),x.configure=o(function(e,r){var t=g(!0,{},e);g(!r,this.options,t),this.global(t)}),x.scope=o(function(e){var r=new s(this);return g(!0,r.options.payload,e),r}),x.wrap=function(e,r){try{var t;if(t=v.isType(r,"function")?r:function(){return r||{}},!v.isType(e,"function"))return e;if(e._isWrap)return e;if(!e._wrapped){e._wrapped=function(){try{return e.apply(this,arguments)}catch(r){throw"string"==typeof r&&(r=new String(r)),r.stack||(r._savedStackTrace=m.parse(r)),r._rollbarContext=t()||{},r._rollbarContext._wrappedSource=e.toString(),window._rollbarWrappedError=r,r}},e._wrapped._isWrap=!0;for(var n in e)e.hasOwnProperty(n)&&(e._wrapped[n]=e[n])}return e._wrapped}catch(r){return e}},x.loadFull=function(){v.consoleError("[Rollbar]: Unexpected Rollbar.loadFull() called on a Notifier instance")},s.processPayloads=function(e){return e?void f():void i()};var L=(new Date).getTime(),T=0,R=0;e.exports={Notifier:s,setupJSON:n,topLevelNotifier:a}},function(e,r){"use strict";var t=Object.prototype.hasOwnProperty,n=Object.prototype.toString,o=function(e){return"function"==typeof Array.isArray?Array.isArray(e):"[object Array]"===n.call(e)},i=function(e){if(!e||"[object Object]"!==n.call(e))return!1;var r=t.call(e,"constructor"),o=e.constructor&&e.constructor.prototype&&t.call(e.constructor.prototype,"isPrototypeOf");if(e.constructor&&!r&&!o)return!1;var i;for(i in e);return"undefined"==typeof i||t.call(e,i)};e.exports=function e(){var r,t,n,a,s,u,c=arguments[0],l=1,p=arguments.length,f=!1;for("boolean"==typeof c?(f=c,c=arguments[1]||{},l=2):("object"!=typeof c&&"function"!=typeof c||null==c)&&(c={});l<p;++l)if(r=arguments[l],null!=r)for(t in r)n=c[t],a=r[t],c!==a&&(f&&a&&(i(a)||(s=o(a)))?(s?(s=!1,u=n&&o(n)?n:[]):u=n&&i(n)?n:{},c[t]=e(f,u,a)):"undefined"!=typeof a&&(c[t]=a));return c}},function(e,r,t){"use strict";function n(){return l}function o(){return null}function i(e){var r={};return r._stackFrame=e,r.url=e.fileName,r.line=e.lineNumber,r.func=e.functionName,r.column=e.columnNumber,r.args=e.args,r.context=o(r.url,r.line),r}function a(e){function r(){var r=[];try{r=c.parse(e)}catch(e){r=[]}for(var t=[],n=0;n<r.length;n++)t.push(new i(r[n]));return t}return{stack:r(),message:e.message,name:e.name}}function s(e){return new a(e)}function u(e){if(!e)return["Unknown error. There was no error message to display.",""];var r=e.match(p),t="(unknown)";return r&&(t=r[r.length-1],e=e.replace((r[r.length-2]||"")+t+":",""),e=e.replace(/(^[\s]+|[\s]+$)/g,"")),[t,e]}var c=t(6),l="?",p=new RegExp("^(([a-zA-Z0-9-_$ ]*): *)?(Uncaught )?([a-zA-Z0-9-_$ ]*): ");e.exports={guessFunctionName:n,guessErrorClass:u,gatherContext:o,parse:s,Stack:a,Frame:i}},function(e,r,t){var n,o,i;!function(a,s){"use strict";o=[t(7)],n=s,i="function"==typeof n?n.apply(r,o):n,!(void 0!==i&&(e.exports=i))}(this,function(e){"use strict";function r(e,r,t){if("function"==typeof Array.prototype.map)return e.map(r,t);for(var n=new Array(e.length),o=0;o<e.length;o++)n[o]=r.call(t,e[o]);return n}function t(e,r,t){if("function"==typeof Array.prototype.filter)return e.filter(r,t);for(var n=[],o=0;o<e.length;o++)r.call(t,e[o])&&n.push(e[o]);return n}var n=/(^|@)\S+\:\d+/,o=/^\s*at .*(\S+\:\d+|\(native\))/m,i=/^(eval@)?(\[native code\])?$/;return{parse:function(e){if("undefined"!=typeof e.stacktrace||"undefined"!=typeof e["opera#sourceloc"])return this.parseOpera(e);if(e.stack&&e.stack.match(o))return this.parseV8OrIE(e);if(e.stack)return this.parseFFOrSafari(e);throw new Error("Cannot parse given Error object")},extractLocation:function(e){if(e.indexOf(":")===-1)return[e];var r=e.replace(/[\(\)\s]/g,"").split(":"),t=r.pop(),n=r[r.length-1];if(!isNaN(parseFloat(n))&&isFinite(n)){var o=r.pop();return[r.join(":"),o,t]}return[r.join(":"),t,void 0]},parseV8OrIE:function(n){var i=t(n.stack.split("\n"),function(e){return!!e.match(o)},this);return r(i,function(r){r.indexOf("(eval ")>-1&&(r=r.replace(/eval code/g,"eval").replace(/(\(eval at [^\()]*)|(\)\,.*$)/g,""));var t=r.replace(/^\s+/,"").replace(/\(eval code/g,"(").split(/\s+/).slice(1),n=this.extractLocation(t.pop()),o=t.join(" ")||void 0,i="eval"===n[0]?void 0:n[0];return new e(o,void 0,i,n[1],n[2],r)},this)},parseFFOrSafari:function(n){var o=t(n.stack.split("\n"),function(e){return!e.match(i)},this);return r(o,function(r){if(r.indexOf(" > eval")>-1&&(r=r.replace(/ line (\d+)(?: > eval line \d+)* > eval\:\d+\:\d+/g,":$1")),r.indexOf("@")===-1&&r.indexOf(":")===-1)return new e(r);var t=r.split("@"),n=this.extractLocation(t.pop()),o=t.shift()||void 0;return new e(o,void 0,n[0],n[1],n[2],r)},this)},parseOpera:function(e){return!e.stacktrace||e.message.indexOf("\n")>-1&&e.message.split("\n").length>e.stacktrace.split("\n").length?this.parseOpera9(e):e.stack?this.parseOpera11(e):this.parseOpera10(e)},parseOpera9:function(r){for(var t=/Line (\d+).*script (?:in )?(\S+)/i,n=r.message.split("\n"),o=[],i=2,a=n.length;i<a;i+=2){var s=t.exec(n[i]);s&&o.push(new e(void 0,void 0,s[2],s[1],void 0,n[i]))}return o},parseOpera10:function(r){for(var t=/Line (\d+).*script (?:in )?(\S+)(?:: In function (\S+))?$/i,n=r.stacktrace.split("\n"),o=[],i=0,a=n.length;i<a;i+=2){var s=t.exec(n[i]);s&&o.push(new e(s[3]||void 0,void 0,s[2],s[1],void 0,n[i]))}return o},parseOpera11:function(o){var i=t(o.stack.split("\n"),function(e){return!!e.match(n)&&!e.match(/^Error created at/)},this);return r(i,function(r){var t,n=r.split("@"),o=this.extractLocation(n.pop()),i=n.shift()||"",a=i.replace(/<anonymous function(: (\w+))?>/,"$2").replace(/\([^\)]*\)/g,"")||void 0;i.match(/\(([^\)]*)\)/)&&(t=i.replace(/^[^\(]+\(([^\)]*)\)$/,"$1"));var s=void 0===t||"[arguments not available]"===t?void 0:t.split(",");return new e(a,s,o[0],o[1],o[2],r)},this)}}})},function(e,r,t){var n,o,i;!function(t,a){"use strict";o=[],n=a,i="function"==typeof n?n.apply(r,o):n,!(void 0!==i&&(e.exports=i))}(this,function(){"use strict";function e(e){return!isNaN(parseFloat(e))&&isFinite(e)}function r(e,r,t,n,o,i){void 0!==e&&this.setFunctionName(e),void 0!==r&&this.setArgs(r),void 0!==t&&this.setFileName(t),void 0!==n&&this.setLineNumber(n),void 0!==o&&this.setColumnNumber(o),void 0!==i&&this.setSource(i)}return r.prototype={getFunctionName:function(){return this.functionName},setFunctionName:function(e){this.functionName=String(e)},getArgs:function(){return this.args},setArgs:function(e){if("[object Array]"!==Object.prototype.toString.call(e))throw new TypeError("Args must be an Array");this.args=e},getFileName:function(){return this.fileName},setFileName:function(e){this.fileName=String(e)},getLineNumber:function(){return this.lineNumber},setLineNumber:function(r){if(!e(r))throw new TypeError("Line Number must be a Number");this.lineNumber=Number(r)},getColumnNumber:function(){return this.columnNumber},setColumnNumber:function(r){if(!e(r))throw new TypeError("Column Number must be a Number");this.columnNumber=Number(r)},getSource:function(){return this.source},setSource:function(e){this.source=String(e)},toString:function(){var r=this.getFunctionName()||"{anonymous}",t="("+(this.getArgs()||[]).join(",")+")",n=this.getFileName()?"@"+this.getFileName():"",o=e(this.getLineNumber())?":"+this.getLineNumber():"",i=e(this.getColumnNumber())?":"+this.getColumnNumber():"";return r+t+n+o+i}},r})},function(e,r,t){"use strict";function n(e){v=e}function o(e){return{}.toString.call(e).match(/\s([a-zA-Z]+)/)[1].toLowerCase()}function i(e,r){return o(e)===r}function a(e){if(!i(e,"string"))throw new Error("received invalid input");for(var r=w,t=r.parser[r.strictMode?"strict":"loose"].exec(e),n={},o=14;o--;)n[r.key[o]]=t[o]||"";return n[r.q.name]={},n[r.key[12]].replace(r.q.parser,function(e,t,o){t&&(n[r.q.name][t]=o)}),n}function s(e){var r=a(e);return""===r.anchor&&(r.source=r.source.replace("#","")),e=r.source.replace("?"+r.query,"")}function u(e,r){var t,n,o,a=i(e,"object"),s=i(e,"array"),c=[];if(a)for(t in e)e.hasOwnProperty(t)&&c.push(t);else if(s)for(o=0;o<e.length;++o)c.push(o);for(o=0;o<c.length;++o)t=c[o],n=e[t],a=i(n,"object"),s=i(n,"array"),a||s?e[t]=u(n,r):e[t]=r(t,n);return e}function c(e){return e=String(e),new Array(e.length+1).join("*")}function l(){var e=(new Date).getTime(),r="xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g,function(r){var t=(e+16*Math.random())%16|0;return e=Math.floor(e/16),("x"===r?t:7&t|8).toString(16)});return r}function p(e){return"function"!=typeof Object.create?function(e){var r=function(){};return function(e){if(null!==e&&e!==Object(e))throw TypeError("Argument must be an object, or null");r.prototype=e||{};var t=new r;return r.prototype=null,null===e&&(t.__proto__=null),t}}()(e):Object.create(e)}function f(){for(var e=[],r=0;r<arguments.length;r++){var t=arguments[r];"object"==typeof t?(t=v.stringify(t),t.length>500&&(t=t.substr(0,500)+"...")):"undefined"==typeof t&&(t="undefined"),e.push(t)}return e.join(" ")}function d(){m.ieVersion()<=8?console.error(f.apply(null,arguments)):console.error.apply(null,arguments)}function h(){m.ieVersion()<=8?console.info(f.apply(null,arguments)):console.info.apply(null,arguments)}function g(){m.ieVersion()<=8?console.log(f.apply(null,arguments)):console.log.apply(null,arguments)}t(9);var m=t(10),v=null,w={strictMode:!1,key:["source","protocol","authority","userInfo","user","password","host","port","relative","path","directory","file","query","anchor"],q:{name:"queryKey",parser:/(?:^|&)([^&=]*)=?([^&]*)/g},parser:{strict:/^(?:([^:\/?#]+):)?(?:\/\/((?:(([^:@]*)(?::([^:@]*))?)?@)?([^:\/?#]*)(?::(\d*))?))?((((?:[^?#\/]*\/)*)([^?#]*))(?:\?([^#]*))?(?:#(.*))?)/,loose:/^(?:(?![^:@]+:[^:@\/]*@)([^:\/?#.]+):)?(?:\/\/)?((?:(([^:@]*)(?::([^:@]*))?)?@)?([^:\/?#]*)(?::(\d*))?)(((\/(?:[^?#](?![^?#\/]*\.[^?#\/.]+(?:[?#]|$)))*\/?)?([^?#\/]*))(?:\?([^#]*))?(?:#(.*))?)/}},y={setupJSON:n,isType:i,parseUri:a,parseUriOptions:w,redact:c,sanitizeUrl:s,traverse:u,typeName:o,uuid4:l,objectCreate:p,consoleError:d,consoleInfo:h,consoleLog:g};e.exports=y},function(e,r){!function(e){"use strict";e.console||(e.console={});for(var r,t,n=e.console,o=function(){},i=["memory"],a="assert,clear,count,debug,dir,dirxml,error,exception,group,groupCollapsed,groupEnd,info,log,markTimeline,profile,profiles,profileEnd,show,table,time,timeEnd,timeline,timelineEnd,timeStamp,trace,warn".split(",");r=i.pop();)n[r]||(n[r]={});for(;t=a.pop();)n[t]||(n[t]=o)}("undefined"==typeof window?this:window)},function(e,r){"use strict";function t(){for(var e,r=3,t=document.createElement("div"),n=t.getElementsByTagName("i");t.innerHTML="<!--[if gt IE "+ ++r+"]><i></i><![endif]-->",n[0];);return r>4?r:e}var n={ieVersion:t};e.exports=n},function(e,r,t){"use strict";function n(e){a=e}function o(e){this.name="Connection Error",this.message=e,this.stack=(new Error).stack}var i=t(8),a=null;o.prototype=i.objectCreate(Error.prototype),o.prototype.constructor=o;var s={XMLHttpFactories:[function(){return new XMLHttpRequest},function(){return new ActiveXObject("Msxml2.XMLHTTP")},function(){return new ActiveXObject("Msxml3.XMLHTTP")},function(){return new ActiveXObject("Microsoft.XMLHTTP")}],createXMLHTTPObject:function(){var e,r=!1,t=s.XMLHttpFactories,n=t.length;for(e=0;e<n;e++)try{r=t[e]();break}catch(e){}return r},post:function(e,r,t,n){if(!i.isType(t,"object"))throw new Error("Expected an object to POST");t=a.stringify(t),n=n||function(){};var u=s.createXMLHTTPObject();if(u)try{try{var c=function(){try{if(c&&4===u.readyState){c=void 0;var e=a.parse(u.responseText);200===u.status?n(null,e):i.isType(u.status,"number")&&u.status>=400&&u.status<600?(403==u.status&&i.consoleError("[Rollbar]:"+e.message),n(new Error(String(u.status)))):n(new o("XHR response had no status code (likely connection failure)"))}}catch(e){var r;r=e&&e.stack?e:new Error(e),n(r)}};u.open("POST",e,!0),u.setRequestHeader&&(u.setRequestHeader("Content-Type","application/json"),u.setRequestHeader("X-Rollbar-Access-Token",r)),u.onreadystatechange=c,u.send(t)}catch(r){if("undefined"!=typeof XDomainRequest){"http:"===window.location.href.substring(0,5)&&"https"===e.substring(0,5)&&(e="http"+e.substring(5));var l=function(){n(new o("Request timed out"))},p=function(){n(new Error("Error during request"))},f=function(){n(null,a.parse(u.responseText))};u=new XDomainRequest,u.onprogress=function(){},u.ontimeout=l,u.onerror=p,u.onload=f,u.open("POST",e,!0),u.send(t)}}}catch(e){n(e)}}};e.exports={XHR:s,setupJSON:n,ConnectionError:o}}])});
},{}]},{},[3])(3)
});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm5vZGVfbW9kdWxlcy9icm93c2VyLXBhY2svX3ByZWx1ZGUuanMiLCJsaWIvYXV0aC5qcyIsImxpYi9iYXNlLmpzIiwibGliL2Jyb3dzZXIuanMiLCJsaWIvY29udmVudGlvbi9jb3Vwb24tdHlwZXMuanMiLCJsaWIvY29udmVudGlvbi9jb3Vwb25zLmpzIiwibGliL2NvbnZlbnRpb24vZXZlbnRzLmpzIiwibGliL2NvbnZlbnRpb24vbG9jYXRpb25zLmpzIiwibGliL2NvbnZlbnRpb24vbWFuYWdlcnMuanMiLCJsaWIvY29udmVudGlvbi9tZXJjaGFuZGlzZS5qcyIsImxpYi9jb252ZW50aW9uL3Bhc3Nlcy5qcyIsImxpYi9jb252ZW50aW9uL3B1cmNoYXNlcy5qcyIsImxpYi9jb252ZW50aW9uL3RhZ3MuanMiLCJsaWIvY29udmVudGlvbi90aWNrZXRzLmpzIiwibGliL2NvbnZlbnRpb24vdGltZXNsb3RzLmpzIiwibGliL2NvbnZlbnRpb24vdXNlci1wYXNzZXMuanMiLCJsaWIvY29udmVudGlvbi91c2VyLXJlY29yZHMuanMiLCJsaWIvY29udmVudGlvbnMuanMiLCJsaWIvdXNlcnMuanMiLCJub2RlX21vZHVsZXMvcm9sbGJhci1icm93c2VyL2Rpc3Qvcm9sbGJhci51bWQubm9qc29uLm1pbi5qcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtBQ0FBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDckZBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDeldBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQ2xCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUN0REE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUMxREE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUN0RUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQ2pFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQzNDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUNsRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUN6QkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUN6QkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQ3BIQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQ2xGQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDNUZBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDekVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDakRBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUM5REE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQy9DQSIsImZpbGUiOiJnZW5lcmF0ZWQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlc0NvbnRlbnQiOlsiKGZ1bmN0aW9uIGUodCxuLHIpe2Z1bmN0aW9uIHMobyx1KXtpZighbltvXSl7aWYoIXRbb10pe3ZhciBhPXR5cGVvZiByZXF1aXJlPT1cImZ1bmN0aW9uXCImJnJlcXVpcmU7aWYoIXUmJmEpcmV0dXJuIGEobywhMCk7aWYoaSlyZXR1cm4gaShvLCEwKTt2YXIgZj1uZXcgRXJyb3IoXCJDYW5ub3QgZmluZCBtb2R1bGUgJ1wiK28rXCInXCIpO3Rocm93IGYuY29kZT1cIk1PRFVMRV9OT1RfRk9VTkRcIixmfXZhciBsPW5bb109e2V4cG9ydHM6e319O3Rbb11bMF0uY2FsbChsLmV4cG9ydHMsZnVuY3Rpb24oZSl7dmFyIG49dFtvXVsxXVtlXTtyZXR1cm4gcyhuP246ZSl9LGwsbC5leHBvcnRzLGUsdCxuLHIpfXJldHVybiBuW29dLmV4cG9ydHN9dmFyIGk9dHlwZW9mIHJlcXVpcmU9PVwiZnVuY3Rpb25cIiYmcmVxdWlyZTtmb3IodmFyIG89MDtvPHIubGVuZ3RoO28rKylzKHJbb10pO3JldHVybiBzfSkiLCIvKipcbiAqIGh0dHA6Ly91c2Vqc2RvYy5vcmcvXG4gKi9cblxuLyoqXG4gKiBBdXRoZW50aWNhdGlvbiBBUElcbiAqL1xuY29uc3QgQ29uVHJvbGxBdXRoID0gZnVuY3Rpb24oYXBpKXtcblx0dGhpcy5hcGkgPSBhcGk7XG5cdHJldHVybiB0aGlzO1xufTtcblxuLyoqXG4gKiBWZXJpZnkgdGhhdCB0aGUgdXNlciBpcyBsb2dnZWQgaW4gdG8gQ29uVHJvbGxcbiAqIEByZXR1cm4gUHJvbWlzZSBhbiBvYmplY3Qgd2l0aCBhIGJvb2xlYW4gc3RhdHVzIGZpZWxkXG4gKi9cbkNvblRyb2xsQXV0aC5wcm90b3R5cGUudmVyaWZ5ID0gZnVuY3Rpb24oKSB7XG5cdHJldHVybiBhcGkuc2VuZCgnYXV0aC92ZXJpZnknKVxuXHRcdC50aGVuKHJlcyA9PiByZXMuc3RhdHVzKTtcbn07XG5cbi8qKlxuICogU3RhcnQgYXV0aGVudGljYXRpb24gYnkgY2FsbGluZyBDb25Ucm9sbCB0byBwcm92aWRlIGFuIGF1dGggc3RhcnQgVVJMIGZvciB0aGUgc3BlY2lmaWVkIHByb3ZpZGVyXG4gKiBUaGUgcmV0dXJuZWQgcHJvbWlzZSB3aWxsIHJlc29sdmUgdG8gdGhlIGF1dGhlbnRpY2F0aW9uIFVSTCBhbmQgdGhlIGNhbGxlciBcbiAqIGlzIGV4cGVjdGVkIHRvIHJlZGlyZWN0IHRoZSB1c2VyIHRvIGl0IHNvbWVob3cuXG4gKi9cbkNvblRyb2xsQXV0aC5wcm90b3R5cGUuc3RhcnQgPSBmdW5jdGlvbih1cmwsIHByb3ZpZGVyLCBjYWxsYmFjaykge1xuXHRpZiAoYXJndW1lbnRzLmxlbmd0aCA9PSAxKSB7IC8vIHVybCBpcyBvcHRpb25hbFxuXHRcdGNhbGxiYWNrID0gYXJndW1lbnRzWzBdO1xuXHRcdHByb3ZpZGVyID0gbnVsbDtcblx0XHR1cmwgPSBudWxsO1xuXHR9XG5cdGlmIChhcmd1bWVudHMubGVuZ3RoID09IDIpIHsgLy8gcHJvdmlkZXIgaXMgb3B0aW9uYWxcblx0XHRjYWxsYmFjayA9IGFyZ3VtZW50c1sxXTtcblx0XHRwcm92aWRlciA9IG51bGw7XG5cdH1cblx0cmV0dXJuIHRoaXMuYXBpLnNlbmQoJ2F1dGgvc3RhcnQnLCB7XG5cdFx0J3JlZGlyZWN0LXVybCc6IHVybCxcblx0XHQncHJvdmlkZXInOiAocHJvdmlkZXIgfHwgJ2dvb2dsZScpXG5cdH0pLnRoZW4ocmVzID0+IHJlc1snYXV0aC11cmwnXSk7XG59O1xuXG4vKipcbiAqIENoYW5nZSB0aGUgY3VycmVudCB3aW5kb3cgdG8gdGhlIGNvbnRvcmxsIHByb3ZpZGVyIHNlbGVjdGlvbiBkaWFsb2csIGFuZCBpZiBzdWNjZXNzZnVsLCBjb21lIGJhY2sgdG8gdGhlXG4gKiBzcGVjaWZpZWQgdXJsLlxuICogVGhpcyBtZXRob2Qgb25seSBtYWtlcyBzZW5zZSB3aGVuIHJ1bm5pbmcgaW4gdGhlIGNsaWVudCwgaW4gd2hpY2ggY2FzZVxuICogaXQgd2lsbCBuZXZlciByZXR1cm4uIFdoZW4gcnVubmluZyBpbiB0aGUgc2VydmVyLCB0aGlzIG1ldGhvZCB3aWxsIGVycm9yIG91dC5cbiAqL1xuQ29uVHJvbGxBdXRoLnByb3RvdHlwZS5zdGFydFNlbGVjdCA9IGZ1bmN0aW9uKHVybCkge1xuXHRsZXQgcGFyc2VyID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnYScpO1xuXHRwYXJzZXIuaHJlZiA9IHVybDtcblx0d2luZG93LmxvY2F0aW9uLmhyZWYgPSB0aGlzLmFwaS5lbmRwb2ludCArICcvYXV0aC9zZWxlY3Q/cmVkaXJlY3QtdXJsPScgKyBcblx0XHRcdGVuY29kZVVSSUNvbXBvbmVudChwYXJzZXIucHJvdG9jb2wgKyBcIi8vXCIgKyBwYXJzZXIuaG9zdCArICBwYXJzZXIucGF0aG5hbWUpO1xufTtcblxuLyoqXG4gKiBMb2dvdXQgZnJvbSBDb25Ucm9sbFxuICogQHJldHVybiBhIHByb21pc2UgdGhhdCByZXNvbHZlcyB0byBhIGJvb2xlYW4gc3BlY2lmeWluZyB3aGV0aGVyIHRoZSBsb2dvdXRcbiAqIHN1Y2NlZWRlZCBvciBub3RcbiAqL1xuQ29uVHJvbGxBdXRoLnByb3RvdHlwZS5sb2dvdXQgPSBmdW5jdGlvbihjYWxsYmFjaykge1xuXHRyZXR1cm4gdGhpcy5hcGkuc2VuZCgnYXV0aC9sb2dvdXQnKS50aGVuKCgpID0+IHRydWUpLmNhdGNoKCgpID0+IGZhbHNlKTtcbn1cblxuLyoqXG4gKiBHZXQgdGhlIGN1cnJlbnQgdXNlcidzIGlkZW50aWZpY2F0aW9uIGRhdGEsIGlmIGxvZ2dlZCBpblxuICogQHJldHVybiBhIHByb21pc2UgdGhhdCByZXNvbHZlcyB3aXRoIHRoZSBJRCBvYmplY3RcbiAqL1xuQ29uVHJvbGxBdXRoLnByb3RvdHlwZS5pZCA9IGZ1bmN0aW9uKCkge1xuXHRyZXR1cm4gdGhpcy5hcGkuc2VuZCgnYXV0aC9pZCcpO1xufTtcblxuLyoqXG4gKiBSZXRyaWV2ZSB0aGUgY3VycmVudCB1c2VyIGNvbnZlbnRpb24gcm9sZSwgaWYgZXhpc3RzXG4gKiBAcmV0dXJuIGEgcHJvbWlzZSB0aGF0IHJlc29sdmVzIHRvIHRoZSByb2xlIG9mIHRoZSBsb2dnZWQgaW4gdXNlciwgb3JcbiAqIG51bGwgaWYgdGhleSBoYXZlIG5vIHJvbGVcbiAqL1xuQ29uVHJvbGxBdXRoLnByb3RvdHlwZS5yb2xlID0gZnVuY3Rpb24oY2FsbGJhY2spIHtcblx0dGhpcy5pZCgpLnRoZW4oKHJlcyA9PiB7XG5cdFx0cmV0dXJuIHRoaXMuYXBpLnNlbmQoJ2VudGl0aWVzL21hbmFnZXJzLycgKyByZXMuaWQsIHsgY29udmVudGlvbjogdHJ1ZSB9KVxuXHRcdFx0LnRoZW4ocmVzID0+IHJlcy5yb2xlKTtcblx0fSkuYmluZCh0aGlzKSk7XG59O1xuXG5tb2R1bGUuZXhwb3J0cyA9IENvblRyb2xsQXV0aDtcbiIsIid1c2Ugc3RyaWN0JztcblxuLyoqXG4gKiBKYXZhc2NyaXB0IFNESyBmb3IgdGhlIENvbi1Ucm9sbCBBUElcbiAqL1xuXG5jb25zdCBBdXRoID0gcmVxdWlyZSgnLi9hdXRoJyk7XG5jb25zdCBVc2VycyA9IHJlcXVpcmUoJy4vdXNlcnMnKTtcbmNvbnN0IENvbnZlbnRpb25zID0gcmVxdWlyZSgnLi9jb252ZW50aW9ucycpO1xuY29uc3QgVXNlclJlY29yZHMgPSByZXF1aXJlKCcuL2NvbnZlbnRpb24vdXNlci1yZWNvcmRzJyk7XG5jb25zdCBUYWdzID0gcmVxdWlyZSgnLi9jb252ZW50aW9uL3RhZ3MnKTtcbmNvbnN0IEV2ZW50cyA9IHJlcXVpcmUoJy4vY29udmVudGlvbi9ldmVudHMnKTtcbmNvbnN0IFRpbWVzbG90cyA9IHJlcXVpcmUoJy4vY29udmVudGlvbi90aW1lc2xvdHMnKTtcbmNvbnN0IFRpY2tldHMgPSByZXF1aXJlKCcuL2NvbnZlbnRpb24vdGlja2V0cycpO1xuY29uc3QgUGFzc2VzID0gcmVxdWlyZSgnLi9jb252ZW50aW9uL3Bhc3NlcycpO1xuY29uc3QgVXNlclBhc3NlcyA9IHJlcXVpcmUoJy4vY29udmVudGlvbi91c2VyLXBhc3NlcycpO1xuY29uc3QgQ291cG9uVHlwZXMgPSByZXF1aXJlKCcuL2NvbnZlbnRpb24vY291cG9uLXR5cGVzJyk7XG5jb25zdCBDb3Vwb25zID0gcmVxdWlyZSgnLi9jb252ZW50aW9uL2NvdXBvbnMnKTtcbmNvbnN0IExvY2F0aW9ucyA9IHJlcXVpcmUoJy4vY29udmVudGlvbi9sb2NhdGlvbnMnKTtcbmNvbnN0IE1lcmNoYW5kaXNlID0gcmVxdWlyZSgnLi9jb252ZW50aW9uL21lcmNoYW5kaXNlJyk7XG5jb25zdCBQdXJjaGFzZXMgPSByZXF1aXJlKCcuL2NvbnZlbnRpb24vcHVyY2hhc2VzJyk7XG5jb25zdCBNYW5hZ2VycyA9IHJlcXVpcmUoJy4vY29udmVudGlvbi9tYW5hZ2VycycpO1xuXG4vKipcbiAqIEhlbHBlciBtZXRob2QgdG8gcGFyc2UgaW5jb3JyZWN0bHkgcGFyc2VkIEpTT04gcmVzcG9uc2VzXG4gKiBAcGFyYW0gcmVxT2JqIFhNTEhUVFBSZXF1ZXN0IG9iamVjdFxuICogQHJldHVybnMgcmVzcG9uc2Ugb2JqZWN0IG9yIHBhcnNlZCBKU09OXG4gKi9cbmZ1bmN0aW9uIHJlYWRSZXNwb25zZShyZXFPYmopIHtcblx0aWYgKHJlcU9iai5yZXNwb25zZVR5cGUgPT0gJ2pzb24nKVxuXHRcdHJldHVybiByZXFPYmoucmVzcG9uc2U7XG5cdHRyeSB7XG5cdFx0cmV0dXJuIEpTT04ucGFyc2UocmVxT2JqLnJlc3BvbnNlVGV4dCk7XG5cdH0gY2F0Y2ggKGUpIHtcblx0XHRyZXR1cm4gZmFsc2U7XG5cdH1cbn1cblxuLyoqXG4gKiBBUEkgQ29uc3RydWN0b3JcbiAqIHlvdSBzaG91bGRuJ3QgbmVlZCB0byBjYWxsIHRoaXMgLSB0aGUgQVBJIGlzIGluaXRpYWxpemVkIGJ5IHRoZSBtYWluXG4gKiBvciBicm93c2VyIGVudHJ5IHBvaW50c1xuICovXG5jb25zdCBDb25Ucm9sbCA9IGZ1bmN0aW9uKHdpbmRvdywgZW5kcG9pbnQpIHtcblx0dGhpcy5lbmRwb2ludCA9IGVuZHBvaW50IHx8ICdodHRwOi8vYXBpLmNvbi10cm9sbC5vcmcnO1xuXHQvLyBhdXRvIGRldGVjdCB0ZXN0aW5nIGVudmlyb25tZW50XG5cdGlmICghZW5kcG9pbnQgJiYgd2luZG93ICYmIHdpbmRvdy5sb2NhdGlvbiAmJiB3aW5kb3cubG9jYXRpb24uaG9zdC5tYXRjaCgvbG9jYWxob3N0LykpXG5cdFx0dGhpcy5lbmRwb2ludCA9ICdodHRwOi8vbG9jYWxob3N0OjgwODAnO1xuXHRcblx0dGhpcy5hdXRoID0gbmV3IEF1dGgodGhpcyk7XG5cdHRoaXMudXNlcnMgPSBuZXcgVXNlcnModGhpcyk7XG5cdHRoaXMuY29udmVudGlvbnMgPSBuZXcgQ29udmVudGlvbnModGhpcyk7XG5cdHRoaXMucmVjb3JkcyA9IG5ldyBVc2VyUmVjb3Jkcyh0aGlzKTtcblx0dGhpcy50YWdzID0gbmV3IFRhZ3ModGhpcyk7XG5cdHRoaXMuZXZlbnRzID0gbmV3IEV2ZW50cyh0aGlzKTtcblx0dGhpcy50aW1lc2xvdHMgPSBuZXcgVGltZXNsb3RzKHRoaXMpO1xuXHR0aGlzLnRpY2tldHMgPSBuZXcgVGlja2V0cyh0aGlzKTtcblx0dGhpcy5wYXNzZXMgPSBuZXcgUGFzc2VzKHRoaXMpO1xuXHR0aGlzLnVzZXJwYXNzZXMgPSBuZXcgVXNlclBhc3Nlcyh0aGlzKTtcblx0dGhpcy5jb3Vwb250eXBlcyA9IG5ldyBDb3Vwb25UeXBlcyh0aGlzKTtcblx0dGhpcy5jb3Vwb25zID0gbmV3IENvdXBvbnModGhpcyk7XG5cdHRoaXMubG9jYXRpb25zID0gbmV3IExvY2F0aW9ucyh0aGlzKTtcblx0dGhpcy5tZXJjaGFuZGlzZSA9IG5ldyBNZXJjaGFuZGlzZSh0aGlzKTtcblx0dGhpcy5wdXJjaGFzZXMgPSBuZXcgUHVyY2hhc2VzKHRoaXMpO1xuXHR0aGlzLm1hbmFnZXJzID0gbmV3IE1hbmFnZXJzKHRoaXMpO1xuXG5cdGlmICh3aW5kb3cpIHtcblx0XHR0aGlzLnN0b3JhZ2UgPSB3aW5kb3cuc2Vzc2lvblN0b3JhZ2U7XG5cdFx0dGhpcy5oYW5kbGVBdXRoKHdpbmRvdyk7XG5cdH1cblx0XG5cdHJldHVybiB0aGlzO1xufTtcblxuLyogLS0gQnJvd3NlciBzcGVjaWZpYyByb3V0aW5lcyAtLSB0aGVzZSB3aWxsIG5vdCB3b3JrIGluIHRoZSBzZXJ2ZXJcbiAqIC0tIHRob3VnaCB0aGVzZSBhcmUgbW9zdGx5IGFib3V0IHVzZXIgYXV0aGVudGljYXRpb24sIHNvIHRoZXkgc2hvdWxkbid0XG4gKiAtLSBpbnRlcmVzdCBzZXJ2ZXIgZGV2ZWxvcGVyc1xuICovXG5cbi8qKlxuICogV2hlbiBydW5uaW5nIGluIHRoZSBicm93c2VyLCBkZXRlY3QgaWYgdGhlIEFQSSBjYWxsZWQgb3VyIHdpbmRvdyB3aXRoXG4gKiBhbiBjb21wbGV0ZSBhdXRoZW50aWNhdGlvbiB0b2tlbiwgaW4gd2hpY2ggY2FzZSBzdG9yZSBpdFxuICovXG5Db25Ucm9sbC5wcm90b3R5cGUuaGFuZGxlQXV0aCA9IGZ1bmN0aW9uKHcpIHtcblx0dGhpcy5zdGFydGVkSURMb29rdXAgPSB0cnVlO1xuXHRsZXQgdG9rZW4gPSBudWxsO1xuXHQvLyBjaGVjayBpZiB3ZSBnb3QgaGVyZSBmcm9tIGEgQ29uVHJvbGwgcmVkaXJlY3QgXG5cdHcubG9jYXRpb24uc2VhcmNoLnNwbGl0KC9bXFw/XFwmXSsvKS5mb3JFYWNoKGZ1bmN0aW9uKGYpe1xuXHRcdGxldCBrdiA9IGYuc3BsaXQoJz0nKTsgXG5cdFx0aWYgKGt2WzBdID09ICd0b2tlbicpXG5cdFx0XHR0b2tlbiA9IGRlY29kZVVSSUNvbXBvbmVudChrdlsxXSk7XG5cdH0pO1xuXHRpZiAodG9rZW4pIHtcblx0XHQvLyBzdG9yZSB0b2tlbiBsb2NhbGx5IGFuZCByZWxvYWQgd2l0aG91dCB0aGUgcXVlcnlcblx0XHR0aGlzLnN0b3JhZ2Uuc2V0SXRlbSgnY29udHJvbGwtdG9rZW4nLCB0b2tlbik7XG5cdFx0dGhpcy5hdXRodG9rZW4gPSB0b2tlbjtcblx0XHRpZiAody5oaXN0b3J5LnB1c2hTdGF0ZSkge1xuXHRcdFx0dy5oaXN0b3J5LnB1c2hTdGF0ZSh7Y29udHJvbGxfdG9rZW46IHRva2VufSwgXCJhdXRoZW50aWNhdGVkXCIsIHcubG9jYXRpb24ucGF0aG5hbWUpO1xuXHRcdH0gZWxzZSB7XG5cdFx0XHR3LmxvY2F0aW9uID0gdy5sb2NhdGlvbi5wYXRobmFtZVxuXHRcdH1cblx0fVxuXHRcblx0aWYgKCF0aGlzLmF1dGhUb2tlbikge1xuXHRcdHRoaXMuYXV0aFRva2VuID0gIHcuc2Vzc2lvblN0b3JhZ2UuZ2V0SXRlbSgnY29udHJvbGwtdG9rZW4nKTtcblx0fVxuXHQvLyBsb2FkIHVzZXIgZGF0YSBmcm9tIENvbnRyb2xsXG5cdHRoaXMuYXV0aC5pZCgpLnRoZW4oKHJlcyA9PiB7XG5cdFx0aWYgKHJlcy5zdGF0dXMgPT0gZmFsc2UpXG5cdFx0XHRyZXR1cm47IC8vIG5vIElEXG5cdFx0dGhpcy5zdG9yYWdlLnNldEl0ZW0oJ2NvbnRyb2xsLW5hbWUnLCByZXNbJ25hbWUnXSk7XG5cdFx0dGhpcy5zdG9yYWdlLnNldEl0ZW0oJ2NvbnRyb2xsLWVtYWlsJywgcmVzWydlbWFpbCddKTtcblx0fSkuYmluZCh0aGlzKSk7XG59O1xuXG4vKipcbiAqIENoZWNrIGlmIHRoZSB1c2VyIGlzIGF1dGhlbnRpY2F0ZWQgd2l0aCB0aGUgQ29uVHJvbGwgQVBJLlxuICogSWYgdGhlIHVzZXIgaXMgbm90IGF1dGhlbnRpY2F0ZWQsIHRoaXMgbWV0aG9kIHdpbGwgc3RhcnQgdGhlIGF1dGhlbnRpY2F0aW9uXG4gKiBwcm9jZXNzIGFuZCByZXR1cm4gdG8gdGhlIGN1cnJlbnQgVVJMLCBpbnN0ZWFkIG9mIHJlc29sdmluZyB0aGUgcHJvbWlzZS5cbiAqIEByZXR1cm4gYSBwcm9taXNlIHRoYXQgd2lsbCByZXNvbHZlIGlmIHRoZSB1c2VyIGlzIGF1dGhlbnRpY2F0ZWQuXG4gKi9cbkNvblRyb2xsLnByb3RvdHlwZS5pZkF1dGggPSBmdW5jdGlvbigpIHtcblx0aWYgKCF0aGlzLmdldEF1dGhUb2tlbigpKVxuXHRcdHRoaXMuYXV0aC5zdGFydFNlbGVjdCh3aW5kb3cubG9jYXRpb24uaHJlZik7XG5cdHJldHVybiB0aGlzLmF1dGgudmVyaWZ5KCkudGhlbigobG9nZ2VkaW4gPT4ge1xuXHRcdGlmIChsb2dnZWRpbilcblx0XHRcdHJldHVybjtcblx0XHR0aGlzLmF1dGguc3RhcnRTZWxlY3Qod2luZG93LmxvY2F0aW9uLmhyZWYpO1xuXHR9KS5iaW5kKHRoaXMpKS5jYXB0dXJlKCgoKSA9PiB0aGlzLmF1dGguc3RhcnRTZWxlY3Qod2luZG93LmxvY2F0aW9uLmhyZWYpKS5iaW5kKHRoaXMpKTtcbn07XG5cbi8qKlxuICogQ2hhbmdlIHRoZSBjdXJyZW50IHdpbmRvdyB0byBhbm90aGVyIFVSTCwgYXV0aGVudGljYXRpbmcgZmlyc3QsIGlmIG5lZWRlZC5cbiAqIFRoaXMgbWV0aG9kIGRvZXMgbm90IHJldHVyblxuICovXG5Db25Ucm9sbC5wcm90b3R5cGUuYXV0aEFuZEdvID0gZnVuY3Rpb24odXJsKSB7XG5cdHRoaXMuYXV0aC52ZXJpZnkoKS50aGVuKChsb2dnZWRpbiA9PiB7XG5cdFx0aWYgKGxvZ2dlZGluKSB7XG5cdFx0XHR3aW5kb3cubG9jYXRpb24uaHJlZiA9IHVybDtcblx0XHRcdHJldHVybjtcblx0XHR9XG5cdFx0XG5cdFx0dGhpcy5hdXRoLnN0YXJ0U2VsZWN0KHVybCk7XG5cdH0pLmJpbmQodGhpcykpO1xufTtcblxuLyoqXG4gKiBQZXJmb3JtIGEgY2FzaG91dCBmb3IgdGhlIGN1cnJlbnQgY2FydCBvZiB0aGUgc3BlY2lmaWVkIHVzZXJcbiAqIEBwYXJhbSB1c2VyaWQgTnVtYmVyIElEIG9mIHVzZXJcbiAqIEBwYXJhbSBhbW91bnQgTnVtYmVyIHBheW91dCByZWNlaXZlZFxuICogQHJldHVybiBhIHByb21pc2UgdGhhdCB3aWxsIGJlIHJlc29sdmVkIHdoZW4gY2FzaG91dCBjb21wbGV0ZXNcbiAqL1xuQ29uVHJvbGwuY2FzaG91dCA9IGZ1bmN0aW9uKHVzZXJpZCwgYW1vdW50KSB7XG5cdHJldHVybiB0aGlzLnNlbmQoJ2NoZWNrb3V0L2Nhc2hvdXQnLCB7IHVzZXI6IHVzZXJpZCwgYW1vdW50OiBhbW91bnQgfSwgXG5cdFx0XHR7IGNvbnZlbnRpb246IHRydWUgfSwgJ1BPU1QnKTtcbn07XG5cbi8qIC0tIE5vbi1icm93c2VyIHNwZWNpZmljIG1ldGhvZHMgKi9cblxuLyoqXG4gKiBSZXRyaWV2ZSB0aGUgY3VycmVudCBBUEkgZW5kcG9pbnQgVVJMXG4gKiBAcmV0dXJuIGVuZHBvaW50IFVSTFxuICovXG5Db25Ucm9sbC5nZXRFbmRwb2ludCA9IGZ1bmN0aW9uKCkge1xuXHRyZXR1cm4gYXBpLmVuZHBvaW50O1xufTtcblxuLyoqXG4gKiBSZXRyaWV2ZSB1c2VyJ3MgbmFtZSBhbmQgZW1haWwgZnJvbSBhdXRoIHN0b3JhZ2VcbiAqIEByZXR1cm4gYSBwcm9wZXJ0eSBsaXN0IHdpdGggdGhlICduYW1lJyBhbmQgJ2VtYWlsJyBwcm9wZXJ0aWVzXG4gKi9cbkNvblRyb2xsLnByb3RvdHlwZS51c2VyRGV0YWlscyA9IGZ1bmN0aW9uKCkge1xuXHRyZXR1cm4ge1xuXHRcdG5hbWU6IHRoaXMuc3RvcmFnZSA/IHRoaXMuc3RvcmFnZS5nZXRJdGVtKCdjb250cm9sbC1uYW1lJykgOiBudWxsLFxuXHRcdGVtYWlsOiB0aGlzLnN0b3JhZ2UgPyB0aGlzLnN0b3JhZ2UuZ2V0SXRlbSgnY29udHJvbGwtZW1haWwnKSA6IG51bGwsXG5cdH07XG59O1xuXG4vKipcbiAqIFJldHJpZXZlIHRoZSBjdXJyZW50IHVzZXIncyBlbWFpbCwgZnJvbSBlaXRoZXIgbG9jYWwgc3RvcmFnZVxuICogb3IgYnkgcXVlcnlpbmcgdGhlIHNlcnZlclxuICogQHJldHVybiBhIHByb21pc2UgdGhhdCB3aWxsIHJlc29sdmUgd2l0aCB0aGUgdXNlcidzIGVtYWlsLCBpZiB0aGUgdXNlclxuICogICBpcyBsb2dnZWQgaW4sIG9yIGZhbHNlIG90aGVyd2lzZVxuICovXG5Db25Ucm9sbC5wcm90b3R5cGUuZ2V0VXNlckVtYWlsID0gZnVuY3Rpb24oKSB7XG5cdGlmICh0aGlzLnN0b3JhZ2UgJiYgdGhpcy5zdG9yYWdlLmdldEl0ZW0oJ2NvbnRyb2xsLWVtYWlsJykpXG5cdFx0cmV0dXJuIG5ldyBQcm9taXNlKChcblx0XHRcdFx0KGFjY2VwdCxyZWplY3QpID0+IGFjY2VwdCh0aGlzLnN0b3JhZ2UuZ2V0SXRlbSgnY29udHJvbGwtZW1haWwnKSlcblx0XHRcdFx0KS5iaW5kKHRoaXMpKTtcblx0XG5cdHJldHVybiB0aGlzLmF1dGguaWQoKS50aGVuKHJlcyA9PiByZXMuc3RhdHVzID8gcmVzLmVtYWlsIDogZmFsc2UpO1xufTtcblxuLyoqXG4gKiBSZXRyaWV2ZSB0aGUgY3VycmVudCBsb2cgaW4gYXV0aGVudGljYXRpb24gdG9rZW5cbiAqIEByZXR1cm4gdGhlIGF1dGhlbnRpY2F0aW9uIHRva2VuIGlmIGl0IGlzIGtub3duLCBmYWxzZSBvdGhlcndpc2VcbiAqL1xuQ29uVHJvbGwucHJvdG90eXBlLmdldEF1dGhUb2tlbiA9IGZ1bmN0aW9uKCkge1xuXHRpZiAodGhpcy5hdXRoVG9rZW4pXG5cdFx0cmV0dXJuIHRoaXMuYXV0aFRva2VuO1xuXHRpZiAodGhpcy5zdG9yYWdlICYmIHRoaXMuc3RvcmFnZS5nZXRJdGVtKCdjb250cm9sbC10b2tlbicpKVxuXHRcdHJldHVybiB0aGlzLnN0b3JhZ2UuZ2V0SXRlbSgnY29udHJvbGwtdG9rZW4nKTtcblx0cmV0dXJuIGZhbHNlO1xufTtcblxuQ29uVHJvbGwucHJvdG90eXBlLnJvbGxiYXIgPSBmdW5jdGlvbigpIHtcblx0cmV0dXJuIHRoaXMuUm9sbGJhciB8fCB3aW5kb3cuUm9sbGJhcjtcbn07XG5cbi8qKlxuICogU2V0IHRoZSBjb252ZW50aW9uIEFQSSBrZXkgdXNlZCBmb3IgY29udmVudGlvbiBzcGVjaWZpYyBBUEkgY2FsbHNcbiAqIEBwYXJhbSBhcGlfa2V5IFN0cmluZyBjb252ZW50aW9uJ3MgcHVibGljIEFQSSBrZXlcbiAqL1xuQ29uVHJvbGwucHJvdG90eXBlLnNldENvbnZlbnRpb24gPSBmdW5jdGlvbihhcGlfa2V5KSB7XG5cdGFwaS5jb252ZW50aW9uQXBpS2V5ID0gYXBpX2tleTtcbn07XG5cbi8qKlxuICogbWFpbiBjYWxsIHJvdXRpbmVcbiAqIEN1cnJlbnRseSBvcHRpbWl6ZWQgZm9yIHdlYiBicm93c2Vyc1xuICogVE9ETzogY29udmVydCB0byB1c2UgcmVxdWlyZSgnaHR0cCcpIGFuZCBsZXQgYnJvd3NlcmlmeSBpbXBsZW1lbnQgdGhlIGxvZ2ljXG4gKi9cbkNvblRyb2xsLnByb3RvdHlwZS5zZW5kID0gZnVuY3Rpb24oYWN0aW9uLCBkYXRhLCBhdXRoLCBtZXRob2QpIHtcblx0aWYgKHR5cGVvZiBhcmd1bWVudHNbMV0gPT0gJ29iamVjdCcpIHsgLy8gZGF0YSBpcyBvcHRpb25hbFxuXHRcdG1ldGhvZCA9IGFyZ3VtZW50c1syXTtcblx0XHRhdXRoID0gYXJndW1lbnRzWzFdO1xuXHRcdGRhdGEgPSBudWxsO1xuXHR9XG5cdHZhciByZXEgPSBuZXcgWE1MSHR0cFJlcXVlc3QoKTtcblx0cmV0dXJuIG5ldyBQcm9taXNlKCgocmVzb2x2ZSwgcmVqZWN0KSA9PiB7XG5cdFx0cmVxLm9ucmVhZHlzdGF0ZWNoYW5nZSA9IGZ1bmN0aW9uKCkge1xuXHRcdFx0aWYgKHJlcS5yZWFkeVN0YXRlID09IDQpIHsvLyBET05FXG5cdFx0XHRcdHZhciByZXMgPSByZWFkUmVzcG9uc2UocmVxKTtcblx0XHRcdFx0aWYgKHJlcS5zdGF0dXMgPT0gMjAwKVxuXHRcdFx0XHRcdHJldHVybiByZXNvbHZlKHJlcyk7XG5cdFx0XHRcdFxuXHRcdFx0XHRpZiAocmVzLnN0YXR1cyAvIDEwMCA9PSA1KSAvLyByZXBvcnQgc2VydmVyIGVycm9ycyB0byBSb2xsYmFyXG5cdFx0XHRcdFx0dGhpcy5yb2xsYmFyKCkuZXJyb3IoXCJDb25Ucm9sbCBBUEkgZXJyb3I6XCIsIHJlcyB8fCByZXEucmVzcG9uc2VUZXh0IHx8IHJlcSk7XG5cdFx0XHRcdGlmICh0eXBlb2YgcmVzICE9ICdvYmplY3QnKSAvLyB0aGlzIG5vdCBhbiBlcnJvciByZXBvcnRlZCBieSB0aGUgc2VydmVyXG5cdFx0XHRcdFx0Ly8gLSBpdHMgYSBzZXJ2ZXIgZXJyb3Igb3IgYSBuZXR3b3JrIGVycm9yLCBmYWtlIHN0aCBmb3IgdGhlIGNhbGxlclxuXHRcdFx0XHRcdHJlcyA9IHsgc3RhdHVzOiBmYWxzZSwgZXJyb3I6IHJlcS5yZXNwb25zZVRleHQgfHwgXCJDT1JTIGVycm9yXCIgfTtcblx0XHRcdFx0XG5cdFx0XHRcdGNvbnNvbGUuZXJyb3IocmVzLmVycm9yKTtcblx0XHRcdFx0cmVqZWN0KHJlcyk7XG5cdFx0XHR9XG5cdFx0fS5iaW5kKHRoaXMpO1xuXHRcdFxuXHRcdHJlcS53aXRoQ3JlZGVudGlhbHMgPSB0cnVlOyAvLyBhbGxvdyBDT1JTIGNvb2tpZXMgdGhhdCBhcmUgcmVxdWlyZWQgZm9yIHRoZSBQSFAgc2Vzc2lvblxuXHRcdGlmIChkYXRhKSB7XG5cdFx0XHRyZXEub3BlbihtZXRob2QgfHwgJ1BPU1QnLCB0aGlzLmVuZHBvaW50ICsgJy8nICsgYWN0aW9uKTtcblx0XHRcdHJlcS5zZXRSZXF1ZXN0SGVhZGVyKFwiQ29udGVudC10eXBlXCIsXCJhcHBsaWNhdGlvbi9qc29uXCIpO1xuXHRcdFx0Ly8gcmVxLnNldFJlcXVlc3RIZWFkZXIoXCJBY2Nlc3MtQ29udHJvbC1SZXF1ZXN0LU1ldGhvZFwiLCBcIlBPU1RcIik7XG5cdFx0fSBlbHNlIHtcblx0XHRcdHJlcS5vcGVuKG1ldGhvZCB8fCAnR0VUJywgdGhpcy5lbmRwb2ludCArICcvJyArIGFjdGlvbik7XG5cdFx0fVxuXHRcdHJlcS5zZXRSZXF1ZXN0SGVhZGVyKFwiQ2FjaGUtQ29udHJvbFwiLFwibm8tY2FjaGVcIik7XG5cdFx0Ly9yZXEuc2V0UmVxdWVzdEhlYWRlcihcIk9yaWdpblwiLHdpbmRvdy5sb2NhdGlvbi5ob3N0KTtcblx0XHRpZiAodGhpcy5hdXRoVG9rZW4pIHtcblx0XHRcdC8vcmVxLnNldFJlcXVlc3RIZWFkZXIoJ0FjY2Vzcy1Db250cm9sLVJlcXVlc3QtSGVhZGVycycsICdBdXRob3JpemF0aW9uJyk7XG5cdFx0XHRyZXEuc2V0UmVxdWVzdEhlYWRlcignQXV0aG9yaXphdGlvbicsIHRoaXMuYXV0aFRva2VuKTtcblx0XHR9XG5cdFx0aWYgKGF1dGgpIHtcblx0XHRcdGlmIChhdXRoLmNvbnZlbnRpb24pIHtcblx0XHRcdFx0aWYgKGF1dGguY29udmVudGlvbiA9PT0gdHJ1ZSkgYXV0aC5jb252ZW50aW9uID0gdGhpcy5jb252ZW50aW9uQXBpS2V5O1xuXHRcdFx0XHRyZXEuc2V0UmVxdWVzdEhlYWRlcignQ29udmVudGlvbicsIGF1dGguY29udmVudGlvbik7XG5cdFx0XHR9IFxuXHRcdH1cblx0XHRpZiAoZGF0YSkge1xuXHRcdFx0cmVxLnNlbmQoSlNPTi5zdHJpbmdpZnkoZGF0YSkpO1xuXHRcdH0gZWxzZSB7XG5cdFx0XHRyZXEuc2VuZCgpO1xuXHRcdH1cblx0fSkuYmluZCh0aGlzKSk7XG59O1xuXG4vKipcbiAqIFJldHJlaXZlIGEgcmVjb3JkIG9yIGEgY2F0YWxvZyBvZiBjb2xsZWN0aW9uIGZyb20gdGhlIHNlcnZlclxuICogQHBhcmFtIGNvbGxlY3Rpb24gQ29sbGVjdGlvbiB0byByZXRyaWV2ZVxuICogQHBhcmFtIHJlY29yZF9pZCByZWNvcmRzIHRvIGxvb2t1cCAtIG9uZSBvZjpcbiAqICAqIGEgc3BlY2lmaWMgSUQgKE51bWJlciBvciBTdHJpbmcpIC0gZ2V0IGEgc2luZ2xlIG9iamVjdFxuICogICogZmFsc2UgLSByZXRyaWV2ZSB0aGUgZW50aXJlIGNvbGxlY3Rpb24gY2F0YWxvZ1xuICogICogYSBwcm9wZXJ0eSBsaXN0IGNvbnRhaW5pbmcgZmlsdGVyIEtWIHBhaXJzIC0gcmV0cmlldmUgYSBmaWx0ZXJlZCBjYXRhbG9nXG4gKiBAcmV0dXJuIHByb21pc2UgdGhhdCB3aWxsIHJlc29sdmUgd2l0aCB0aGUgcmVzcG9uc2Ugb2JqZWN0IG9yIHJlamVjdCB3aXRoIGFuIGVycm9yIG9iamVjdFxuICovXG5Db25Ucm9sbC5wcm90b3R5cGUuZ2V0ID0gZnVuY3Rpb24oY29sbGVjdGlvbiwgcmVjb3JkX2lkKSB7XG5cdHZhciBhdXRoID0ge307XG5cdGlmICh0eXBlb2YgY29sbGVjdGlvbiA9PSAnb2JqZWN0Jykge1xuXHRcdGZvciAodmFyIGZpZWxkIGluIGNvbGxlY3Rpb24pIHtcblx0XHRcdGlmIChmaWVsZCA9PSAnY29sbGVjdGlvbicpIGNvbnRpbnVlO1xuXHRcdFx0YXV0aFtmaWVsZF0gPSBjb2xsZWN0aW9uW2ZpZWxkXTtcblx0XHR9XG5cdFx0Y29sbGVjdGlvbiA9IGNvbGxlY3Rpb24uY29sbGVjdGlvbjtcblx0fVxuXHR2YXIgdXJpID0gJ2VudGl0aWVzLycgKyBjb2xsZWN0aW9uICsgJy8nO1xuXHRpZiAodHlwZW9mIHJlY29yZF9pZCA9PSAnb2JqZWN0Jykge1xuXHRcdHZhciBmaWx0ZXJzID0gW107XG5cdFx0Zm9yICh2YXIgZmllbGQgaW4gcmVjb3JkX2lkKSB7XG5cdFx0XHRmaWx0ZXJzLnB1c2goZW5jb2RlVVJJQ29tcG9uZW50KCdieV8nICsgZmllbGQpICsgJz0nICsgZW5jb2RlVVJJQ29tcG9uZW50KHJlY29yZF9pZFtmaWVsZF0pKTtcblx0XHR9XG5cdFx0dXJpICs9ICc/JyArIGZpbHRlcnMuam9pbignJicpO1xuXHR9IGVsc2UgaWYgKHJlY29yZF9pZClcblx0XHR1cmkgKz0gcmVjb3JkX2lkXG5cdHJldHVybiB0aGlzLnNlbmQodXJpLCBhdXRoKTtcbn07XG5cbi8qKlxuICogQ3JlYXRlIGEgbmV3IHJlY29yZCBpbiB0aGUgc3BlY2lmZWlkIGNvbGxlY3Rpb25cbiAqIEBwYXJhbSBjb2xsZWN0aW9uIFN0cmluZ3xPYmplY3QgY29sbGVjdGlvbiB3aGVyZSB0aGUgb2JqZWN0IGlzIHRvIGJlIGNyZWF0ZWRcbiAqIEBwYXJhbSBkYXRhIE9iamVjdCBsaXN0IG9mIHByb2VwcnRpZXMgZm9yIHRoZSBuZXcgb2JqZWN0XG4gKiBAcmV0dXJuIGEgcHJvbWlzZSB0aGF0IHdpbGwgcmVzb2x2ZSBvciByZWplY3Qgd2l0aCB0aGUgc2VydmVyIHJlc3BvbnNlXG4gKi9cbkNvblRyb2xsLnByb3RvdHlwZS5jcmVhdGUgPSBmdW5jdGlvbihjb2xsZWN0aW9uLCBkYXRhKSB7XG5cdHZhciBhdXRoID0ge307XG5cdGlmICh0eXBlb2YgY29sbGVjdGlvbiA9PSAnb2JqZWN0Jykge1xuXHRcdGZvciAodmFyIGZpZWxkIGluIGNvbGxlY3Rpb24pIHtcblx0XHRcdGlmIChmaWVsZCA9PSAnY29sbGVjdGlvbicpIGNvbnRpbnVlO1xuXHRcdFx0YXV0aFtmaWVsZF0gPSBjb2xsZWN0aW9uW2ZpZWxkXTtcblx0XHR9XG5cdFx0Y29sbGVjdGlvbiA9IGNvbGxlY3Rpb24uY29sbGVjdGlvbjtcblx0fVxuXHRyZXR1cm4gdGhpcy5zZW5kKCdlbnRpdGllcy8nICsgY29sbGVjdGlvbiwgZGF0YSwgYXV0aCk7XG59O1xuXG4vKipcbiAqIFVwZGF0ZSBhbiBleGlzdGluZyByZWNvcmQgaW4gdGhlIHNwZWNpZmllZCBjb2xsZWN0aW9uXG4gKiBAcGFyYW0gY29sbGVjdGlvbiBTdHJpbmd8T2JqZWN0IGNvbGxlY3Rpb24gd2hlcmUgdGhlIG9iamVjdCBpcyB0byBiZSB1ZHBhdGVkXG4gKiBAcGFyYW0gaWQgTnVtYmVyfFN0cmluZyBpZCBvZiB0aGUgb2JqZWN0IHRvIGJlIHVwZGF0ZWRcbiAqIEBwYXJhbSBkYXRhIE9iamVjdCBsaXN0IG9mIGZpZWxkcyB0byBzZW5kIGluIHRoZSB1cGRhdGVcbiAqIEByZXR1cm4gYSBwcm9taXNlIHRoYXQgd2lsbCByZXNvbHZlIG9yIHJlamVjdCB3aXRoIHRoZSBzZXJ2ZXIgcmVzcG9uc2VcbiAqL1xuQ29uVHJvbGwucHJvdG90eXBlLnVwZGF0ZSA9IGZ1bmN0aW9uKGNvbGxlY3Rpb24sIGlkLCBkYXRhKSB7XG5cdHZhciBhdXRoID0ge307XG5cdGlmICh0eXBlb2YgY29sbGVjdGlvbiA9PSAnb2JqZWN0Jykge1xuXHRcdGZvciAodmFyIGZpZWxkIGluIGNvbGxlY3Rpb24pIHtcblx0XHRcdGlmIChmaWVsZCA9PSAnY29sbGVjdGlvbicpIGNvbnRpbnVlO1xuXHRcdFx0YXV0aFtmaWVsZF0gPSBjb2xsZWN0aW9uW2ZpZWxkXTtcblx0XHR9XG5cdFx0Y29sbGVjdGlvbiA9IGNvbGxlY3Rpb24uY29sbGVjdGlvbjtcblx0fVxuXHRyZXR1cm4gdGhpcy5zZW5kKCdlbnRpdGllcy8nICsgY29sbGVjdGlvbiArICcvJyArIGlkLCBkYXRhLCBhdXRoLCAnUFVUJyk7XG59O1xuXG4vKipcbiAqIERlbGV0ZSBhbiBleGlzdGluZyByZWNvcmQgaW4gdGhlIHNwZWNpZmllZCBjb2xsZWN0aW9uXG4gKiBAcGFyYW0gY29sbGVjdGlvbiBTdHJpbmd8T2JqZWN0IGNvbGxlY3Rpb24gd2hlcmUgdGhlIG9iamVjdCBpcyB0byBiZSBkZWxldGVkXG4gKiBAcGFyYW0gaWQgTnVtYmVyfFN0cmluZyBpZCBvZiB0aGUgb2JqZWN0IHRvIGJlIGRlbGV0ZWRcbiAqIEByZXR1cm4gYSBwcm9taXNlIHRoYXQgd2lsbCByZXNvbHZlIG9yIHJlamVjdCB3aXRoIHRoZSBzZXJ2ZXIgcmVzcG9uc2VcbiAqL1xuQ29uVHJvbGwucHJvdG90eXBlLmRlbD0gZnVuY3Rpb24oY29sbGVjdGlvbiwgaWQpIHtcblx0dmFyIGF1dGggPSB7fTtcblx0aWYgKHR5cGVvZiBjb2xsZWN0aW9uID09ICdvYmplY3QnKSB7XG5cdFx0Zm9yICh2YXIgZmllbGQgaW4gY29sbGVjdGlvbikge1xuXHRcdFx0aWYgKGZpZWxkID09ICdjb2xsZWN0aW9uJykgY29udGludWU7XG5cdFx0XHRhdXRoW2ZpZWxkXSA9IGNvbGxlY3Rpb25bZmllbGRdO1xuXHRcdH1cblx0XHRjb2xsZWN0aW9uID0gY29sbGVjdGlvbi5jb2xsZWN0aW9uO1xuXHR9XG5cdHJldHVybiB0aGlzLnNlbmQoJ2VudGl0aWVzLycgKyBjb2xsZWN0aW9uICsgJy8nICsgaWQsIG51bGwsIGF1dGgsICdERUxFVEUnKTtcbn07XG5cbm1vZHVsZS5leHBvcnRzID0gQ29uVHJvbGw7XG4iLCIndXNlIHN0cmljdCc7XG5cbmNvbnN0IHJvbGxiYXIgPSByZXF1aXJlKCdyb2xsYmFyLWJyb3dzZXInKTtcblxuY29uc3Qgcm9sbGJhckNvbmZpZyA9IHtcblx0YWNjZXNzVG9rZW46IFwiMzVkYjY0MTNlOTk4NDI2NmE2MDdiMmMzMWUzNzZkYjNcIixcblx0Y2FwdHVyZVVuY2F1Z2h0OiB0cnVlLFxuXHRjYXB0dXJlVW5oYW5kbGVkUmVqZWN0aW9uczogZmFsc2UsXG5cdHBheWxvYWQ6IHtcblx0XHRlbnZpcm9ubWVudDogKCh3aW5kb3d8fHt9KS5sb2NhdGlvbnx8e30pLmhvc3R8fCdsb2NhbCcsXG5cdH1cbn07XG5cbmNvbnN0IFJvbGxiYXIgPSByb2xsYmFyLmluaXQocm9sbGJhckNvbmZpZyk7XG5jb25zdCBjdHIgPSBuZXcgKHJlcXVpcmUoJy4vYmFzZScpKSh3aW5kb3cpO1xuY3RyLlJvbGxiYXIgPSBSb2xsYmFyO1xuXG5tb2R1bGUuZXhwb3J0cyA9IGN0cjtcbiIsIi8qKlxuICogaHR0cDovL3VzZWpzZG9jLm9yZy9cbiAqL1xuXG4ndXNlIHN0cmljdCc7XG5cbi8qKlxuICogQ29uVHJvbGwgQ291cG9uIFR5cGVzIEFQSVxuICogQHBhcmFtIENvblRyb2xsIGFwaVxuICovXG52YXIgQ29uVHJvbGxDb3Vwb25UeXBlcyA9IGZ1bmN0aW9uKGFwaSkge1xuXHR0aGlzLmFwaSA9IGFwaTtcblx0dGhpcy5jb2xsZWN0aW9uID0ge2NvbnZlbnRpb246IHRydWUsIGNvbGxlY3Rpb246ICdjb3Vwb250eXBlcyd9O1xuXHRyZXR1cm4gdGhpcztcbn07XG5cbi8qKlxuICogUmV0cmlldmUgdGhlIGxpc3Qgb2YgYWxsIGNvdXBvbiB0eXBlcyBpbiB0aGUgY29udmVudGlvblxuICogQHJldHVybiBhIHByb21pc2UgdGhhdCB3aWxsIHJlc29sdmUgdG8gdGhlIGxpc3Qgb2YgY291cG9uIHR5cGUgb2JqZWN0c1xuICovXG5Db25Ucm9sbENvdXBvblR5cGVzLnByb3RvdHlwZS5jYXRhbG9nID0gZnVuY3Rpb24oKSB7XG5cdHJldHVybiB0aGlzLmFwaS5nZXQodGhpcy5jb2xsZWN0aW9uKTtcbn07XG5cbi8qKlxuICogQWRkIGEgbmV3IGNvdXBvbiB0eXBlIHRvIHRoZSBjb252ZW50aW9uXG4gKiBAcGFyYW0gdGl0bGUgU3RyaW5nIHRpdGxlIG9mIHRoZSBuZXcgY291cG9uIHR5cGVcbiAqIEBwYXJhbSB2YWx1ZSBOdW1iZXJ8U3RyaW5nIG1vbmV0YXJ5IHZhbHVlIG9mIHRoZSBuZXcgY291cG9uIHR5cGVcbiAqIEBwYXJhbSBjYXRlZ29yeSBTdHJpbmcgbmFtZSBvZiB0aGUgY2F0ZWdvcnkgdGhpcyB0eXBlIGJlbG9uZ3MgdG9cbiAqIEBwYXJhbSBjb2RlIFN0cmluZyBhY3RpdmF0aW9uIGNvZGUgdG8gYWxsb3cgdXNlcnMgdG8gY3JlYXRlIGNvdXBvbnNcbiAqIEByZXR1cm4gYSBwcm9taXNlIHRoYXQgd2lsbCByZXNvbHZlIHRvIHRoZSBuZXcgY291cG9uIHR5cGUgb2JqZWN0IGNyZWF0ZWRcbiAqL1xuQ29uVHJvbGxDb3Vwb25UeXBlcy5wcm90b3R5cGUuYWRkID0gZnVuY3Rpb24odGl0bGUsIHZhbHVlLCBjYXRlZ29yeSwgY29kZSkge1xuXHR2YXIgZGF0YSA9IHtcblx0XHRcdHRpdGxlOiB0aXRsZSxcblx0XHRcdHR5cGU6ICdmaXhlZCcsXG5cdFx0XHR2YWx1ZTogdmFsdWUsXG5cdFx0XHRjYXRlZ29yeTogY2F0ZWdvcnksXG5cdFx0XHRjb2RlOiBjb2RlLFxuXHRcdFx0bXVsdGl1c2U6IGZhbHNlXG5cdH07XG5cdHJldHVybiB0aGlzLmFwaS5jcmVhdGUodGhpcy5jb2xsZWN0aW9uLCBkYXRhKTtcbn07XG5cbi8qKlxuICogUmVtb3ZlIGEgY291cG9uIHR5cGUgZnJvbSB0aGUgY29udmVudGlvblxuICogQHBhcm0gaWQgTnVtYmVyIGlkIG9mIHRoZSBjb3Vwb24gdHlwZSB0byByZW1vdmVcbiAqIEByZXR1cm4gYSBwcm9taXNlIHRoYXQgd2lsbCByZXNvbHZlIHRvIGEgcHJvcGVydHkgbGlzdCBjb250YWluaW5nIGEgYm9vbGVhbiBzdGF0dXMgZmllbGRcbiAqL1xuQ29uVHJvbGxDb3Vwb25UeXBlcy5wcm90b3R5cGUucmVtb3ZlID0gZnVuY3Rpb24oaWQpIHtcblx0cmV0dXJuIHRoaXMuYXBpLmRlbCh0aGlzLmNvbGxlY3Rpb24sIGlkKTtcbn1cblxubW9kdWxlLmV4cG9ydHMgPSBDb25Ucm9sbENvdXBvblR5cGVzO1xuIiwiLyoqXG4gKiBodHRwOi8vdXNlanNkb2Mub3JnL1xuICovXG5cbid1c2Ugc3RyaWN0JztcblxuLyoqXG4gKiBDb25Ucm9sbCBDb3Vwb25zIEFQSVxuICogQHBhcmFtIENvblRyb2xsIGFwaVxuICovXG52YXIgQ29uVHJvbGxDb3Vwb25zID0gZnVuY3Rpb24oYXBpKSB7XG5cdHRoaXMuYXBpID0gYXBpO1xuXHR0aGlzLmNvbGxlY3Rpb24gPSB7Y29udmVudGlvbjogdHJ1ZSwgY29sbGVjdGlvbjogJ2NvdXBvbnMnfTtcblx0cmV0dXJuIHRoaXM7XG59O1xuXG4vKipcbiAqIFJldHJpZXZlIGEgbGlzdCBvZiBhbGwgY291cG9ucyBpbiB0aGUgY29udmVudGlvblxuICogQHJldHVybiBhIHByb21pc2UgdGhhdCB3aWxsIHJlc29sdmUgdG8gdGhlIGxpc3Qgb2YgY291cG9uIG9iamVjdHNcbiAqL1xuQ29uVHJvbGxDb3Vwb25zLnByb3RvdHlwZS5jYXRhbG9nID0gZnVuY3Rpb24oKSB7XG5cdHJldHVybiB0aGlzLmFwaS5nZXQodGhpcy5jb2xsZWN0aW9uKTtcbn07XG5cbi8qKlxuICogUmV0cmlldmUgYSBsaXN0IG9mIGFsbCBjb3Vwb25zIGZvciBhIHNwZWNpZmllZCBjb3Vwb24gdHlwZVxuICogQHBhcmFtIHR5cGVJZCBOdW1iZXIgaWQgb2YgdGhlIGNvdXBvbiB0eXBlXG4gKiBAcmV0dXJuIGEgcHJvbWlzZSB0aGF0IHdpbGwgcmVzb2x2ZSB0byB0aGUgbGlzdCBvZiBjb3Vwb24gb2JqZWN0cyBmb3IgdGhlIHNwZWNpZmVpZFxuICogICB0eXBlXG4gKi9cbkNvblRyb2xsQ291cG9ucy5wcm90b3R5cGUuZm9yVHlwZSA9IGZ1bmN0aW9uKHR5cGVJZCkge1xuXHRyZXR1cm4gdGhpcy5hcGkuZ2V0KHRoaXMuY29sbGVjdGlvbiwgeyB0eXBlOiB0eXBlSWQgfSk7XG59O1xuXG4vKipcbiAqIEFkZCBhIG5ldyBjb3Vwb24gZm9yIGEgdXNlclxuICogQHBhcmFtIHR5cGVJZCBOdW1iZXIgaWQgb2YgdGhlIGNvdXBvbiB0eXBlIHRvIGFkZFxuICogQHBhcmFtIHVzZXIgU3RyaW5nfE51bWJlciBpZCBvciBlbWFpbCBvZiB0aGUgdXNlciB0byBhZGQgYSBjb3Vwb24gZm9yXG4gKiBAcmV0dXJuIGEgcHJvbWlzZSB0aGF0IHdpbGwgcmVzb2x2ZSB0byB0aGUgbmV3IGNvdXBvbiBvYmplY3QgY3JlYXRlZFxuICovXG5Db25Ucm9sbENvdXBvbnMucHJvdG90eXBlLmFkZCA9IGZ1bmN0aW9uKHR5cGVJZCwgdXNlcikge1xuXHR2YXIgZGF0YSA9IHtcblx0XHRcdHR5cGU6IHR5cGVJZCxcblx0XHRcdHVzZXI6IHVzZXJcblx0XHR9O1xuXHRyZXR1cm4gdGhpcy5hcGkuY3JlYXRlKHRoaXMuY29sbGVjdGlvbiwgZGF0YSk7XG59O1xuXG4vKipcbiAqIFJlbW92ZSBhbiBleGlzdGluZyBjb3Vwb24gZnJvbSB0aGUgY29udmVudGlvblxuICogQHBhcmFtIGlkIE51bWJlciBpZCBvZiB0aGUgY291cG9uIHRvIHJlbW92ZVxuICogQHJldHVybiBhIHByb21pc2UgdGhhdCB3aWxsIHJlc29sdmUgdG8gYSBwcm9wZXJ0eSBsaXN0IGNvbnRhaW5pbmcgYSBib29sZWFuIHN0YXR1cyBmaWVsZFxuICovXG5Db25Ucm9sbENvdXBvbnMucHJvdG90eXBlLnJlbW92ZSA9IGZ1bmN0aW9uKGlkKSB7XG5cdHJldHVybiB0aGlzLmFwaS5kZWwodGhpcy5jb2xsZWN0aW9uLCBpZCk7XG59O1xuXG5tb2R1bGUuZXhwb3J0cyA9IENvblRyb2xsQ291cG9ucztcbiIsIi8qKlxuICogaHR0cDovL3VzZWpzZG9jLm9yZy9cbiAqL1xuXG4ndXNlIHN0cmljdCc7XG5cbi8qKlxuICogQ29uVHJvbGwgRXZlbnRzIEFQSVxuICogQHBhcmFtIENvblRyb2xsIGFwaVxuICovXG52YXIgQ29uVHJvbGxFdmVudHMgPSBmdW5jdGlvbihhcGkpIHtcblx0dGhpcy5hcGkgPSBhcGk7XG5cdHRoaXMuY29sbGVjdGlvbiA9IHtjb252ZW50aW9uOiB0cnVlLCBjb2xsZWN0aW9uOiAnZXZlbnRzJ307XG5cdHJldHVybiB0aGlzO1xufTtcblxuLyoqXG4gKiBMaXN0IGFsbCBldmVudHMgaW4gdGhlIGNvbnZlbnRpb25cbiAqIEByZXR1cm4gYSBwcm9taXNlIHRoYXQgd2lsbCByZXNvbHZlIHdpdGggdGhlIGxpc3Qgb2YgZXZlbnQgb2JqZWN0c1xuICovXG5Db25Ucm9sbEV2ZW50cy5wcm90b3R5cGUuY2F0YWxvZyA9IGZ1bmN0aW9uKCkge1xuXHRyZXR1cm4gdGhpcy5hcGkuZ2V0KHRoaXMuY29sbGVjdGlvbik7XG59O1xuXG4vKipcbiAqIFJldHJpZXZlIGEgc3BlY2lmaWMgZXZlbnQgYnkgSURcbiAqIEBwYXJhbSBpZCBOdW1iZXIgaWQgb2YgdGhlIGV2ZW50IHRvIHJldHJpZXZlXG4gKiBAcmV0dXJuIGEgcHJvbWlzZSB0aGF0IHdpbGwgcmVzb2x2ZSB3aXRoIHRoZSBldmVudCBvYmplY3QgcmV0cmVpdmVkXG4gKi9cbkNvblRyb2xsRXZlbnRzLnByb3RvdHlwZS5nZXQgPSBmdW5jdGlvbihpZCkge1xuXHRyZXR1cm4gdGhpcy5hcGkuZ2V0KHRoaXMuY29sbGVjdGlvbiwgaWQpO1xufTtcblxuLyoqXG4gKiBDcmVhdGUgYSBuZXcgZXZlbnQgaW4gdGhlIGNvbnZlbnRpb25cbiAqIEBwYXJhbSB0aXRsZSBTdHJpbmcgdGl0bGUgb2YgdGhlIGV2ZW50IHRvIGNyZWF0ZVxuICogQHBhcmFtIHRlYXNlciBTdHJpbmcgdGVhc2VyIHRleHQgZm9yIHRoZSBldmVudFxuICogQHBhcmFtIGR1cmF0aW9uIE51bWJlciBkZWZhdWx0IGR1cmF0aW9uIGZvciB0aGUgZXZlbnQgaW4gbWludXRlc1xuICogQHBhcmFtIG93bmVySWQgTnVtYmVyIHRoZSB1c2VyIElEIHdobyBzdWdnZXN0ZWQvb3ducyB0aGlzIGV2ZW50XG4gKiBAcmV0dXJuIGEgcHJvbWlzZSB0aGF0IHdpbGwgcmVzb2x2ZSB3aXRoIHRoZSBuZXcgZXZlbnQgb2JqZWN0XG4gKi9cbkNvblRyb2xsRXZlbnRzLnByb3RvdHlwZS5jcmVhdGUgPSBmdW5jdGlvbih0aXRsZSwgdGVhc2VyLCBkdXJhdGlvbiwgb3duZXJJZCkge1xuXHRyZXR1cm4gdGhpcy5hcGkuY3JlYXRlKHRoaXMuY29sbGVjdGlvbiwge1xuXHRcdHRpdGxlOiB0aXRsZSxcblx0XHR0ZWFzZXI6IHRlYXNlcixcblx0XHRkdXJhdGlvbjogZHVyYXRpb24sXG5cdFx0dXNlcjogeyBpZDogb3duZXJJZCB9XG5cdH0pO1xufTtcblxuLyoqXG4gKiBVcGRhdGUgYW4gZXhpc3RpbmcgZXZlbnQgd2l0aCBuZXcgZGF0YVxuICogQHBhcmFtIGlkIE51bWJlciBpZCBvZiB0aGUgZXZlbnQgdG8gdXBkYXRlXG4gKiBAcGFyYW0gZmllbGRzIE9iamVjdCBhIGxpc3Qgb2YgZmllbGRzIHRvIHVwZGF0ZSBvbiB0aGUgZXZlbnRcbiAqIEByZXR1cm4gYSBwcm9taXNlIHRoYXQgd2lsbCByZXNvbHZlIHRvIHRoZSB1cGRhdGVkIGV2ZW50IG9iamVjdFxuICovXG5Db25Ucm9sbEV2ZW50cy5wcm90b3R5cGUudXBkYXRlID0gZnVuY3Rpb24oaWQsIGZpZWxkcykge1xuXHRyZXR1cm4gdGhpcy5hcGkudXBkYXRlKHRoaXMuY29sbGVjdGlvbiwgaWQsIGZpZWxkcyk7XG59O1xuXG4vKipcbiAqIFJlbW92ZSBhbiBleGlzdGluZyBldmVudFxuICogQHBhcmFtIGlkIE51bWJlciBpZCBvZiB0aGUgZXZlbnQgdG8gcmVtb3ZlXG4gKiBAcmV0dXJuIGEgcHJvbWlzZSB0aGF0IHdpbGwgcmVzb2x2ZSB0byBhIHByb3BlcnR5IGxpc3Qgd2l0aCBhIGJvb2xlYW4gc3RhdHVzIGZpZWxkXG4gKi9cbkNvblRyb2xsRXZlbnRzLnByb3RvdHlwZS5yZW1vdmUgPSBmdW5jdGlvbihpZCkge1xuXHRyZXR1cm4gdGhpcy5hcGkuZGVsKHRoaXMuY29sbGVjdGlvbiwgaWQpO1xufTtcblxubW9kdWxlLmV4cG9ydHMgPSBDb25Ucm9sbEV2ZW50cztcbiIsIi8qKlxuICogaHR0cDovL3VzZWpzZG9jLm9yZy9cbiAqL1xuXG4ndXNlIHN0cmljdCc7XG5cbi8qKlxuICogQ29uVHJvbGwgTG9jYXRpb25zIEFQSVxuICogQHBhcmFtIENvblRyb2xsIGFwaVxuICovXG52YXIgQ29uVHJvbGxMb2NhdGlvbnMgPSBmdW5jdGlvbihhcGkpIHtcblx0dGhpcy5hcGkgPSBhcGk7XG5cdHRoaXMuY29sbGVjdGlvbiA9IHtjb252ZW50aW9uOiB0cnVlLCBjb2xsZWN0aW9uOiAnbG9jYXRpb25zJ307XG5cdHJldHVybiB0aGlzO1xufTtcblxuLyoqXG4gKiBSZXRyaWV2ZSB0aGUgbGlzdCBvZiBhbGwgbG9jYXRpb25zIGluIHRoZSBjdXJyZW50IGNvbnZlbnRpb25cbiAqIEByZXR1cm4gYSBwcm9taXNlIHRoYXQgd2lsbCByZXNvbHZlIHRvIHRoZSBsaXN0IG9mIGxvY2F0aW9uIG9iamVjdHNcbiAqL1xuQ29uVHJvbGxMb2NhdGlvbnMucHJvdG90eXBlLmNhdGFsb2cgPSBmdW5jdGlvbigpIHtcblx0cmV0dXJuIHRoaXMuYXBpLmdldCh0aGlzLmNvbGxlY3Rpb24pO1xufTtcblxuLyoqXG4gKiBBZGQgYSBuZXcgbG9jYXRpb24gdG8gdGhlIGNvbnZlbnRpb25cbiAqIEBwYXJhbSB0aXRsZSBTdHJpbmcgdGl0bGUgZm9yIHRoZSBuZXcgbG9jYXRpb25cbiAqIEBwYXJhbSBhcmVhIFN0cmluZyBhcmVhIGRlc2NyaXB0aW9uIGZvciB0aGUgbmV3IGxvY2F0aW9uXG4gKiBAcGFyYW0gbWF4X2F0dGVuZGVlcyBOdW1iZXIgbWF4aW11bSBudW1iZXIgb2YgYXR0ZW5kZWVzIHRoZSBuZXcgbG9jYXRpb24gc3VwcG9ydHNcbiAqIEByZXR1cm4gYSBwcm9taXNlIHRoYXQgd2lsbCByZXNvbHZlIHRvIHRoZSBuZXcgbG9jYXRpb24gb2JqZWN0IGNyZWF0ZWRcbiAqL1xuQ29uVHJvbGxMb2NhdGlvbnMucHJvdG90eXBlLmFkZCA9IGZ1bmN0aW9uKHRpdGxlLCBhcmVhLCBtYXhfYXR0ZW5kZWVzKSB7XG5cdHJldHVybiB0aGlzLmFwaS5jcmVhdGUodGhpcy5jb2xsZWN0aW9uLCB7XG5cdFx0dGl0bGU6IHRpdGxlLFxuXHRcdGFyZWE6IGFyZWEsXG5cdFx0XCJtYXgtYXR0ZW5kZWVzXCI6IG1heF9hdHRlbmRlZXNcblx0fSk7XG59XG5cbi8qKlxuICogVXBkYXRlIGFuIGV4aXN0aW5nIGxvY2F0aW9uIGluIHRoZSBjb252ZW50aW9uXG4gKiBAcGFyYW0gaWQgTnVtYmVyIGlkIG9mIHRoZSBsb2NhdGlvbiB0byB1cGRhdGVcbiAqIEBwcmFtIHRpdGxlIFN0cmluZyBuZXcgdGl0bGUgdG8gc2V0IGZvciB0aGlzIGxvY2F0aW9uXG4gKiBAcGFyYW0gYXJlYSBTdHJpbmcgbmV3IGFyZWEgZGVzY3JpcHRpb24gdG8gc2V0IGZvciB0aGlzIGxvY2F0aW9uXG4gKiBAcGFyYW0gbWF4X2F0dGVuZGVlcyBOdW1iZXIgbmV3IG1heGltdW0gbnVtYmVyIG9mIGF0dGVuZGVlcyBhbGxvd2VkIGluIHRoaXMgbG9jYXRpb25cbiAqIEByZXR1cm4gYSBwcm9taXNlIHRoYXQgd2lsbCByZXNvbHZlIHRvIGEgcHJvcGVydHkgbGlzdCBjb250YWluaW5nIGEgYm9vbGVhbiBzdGF0dXMgZmllbGRcbiAqL1xuQ29uVHJvbGxMb2NhdGlvbnMucHJvdG90eXBlLnVwZGF0ZSA9IGZ1bmN0aW9uKGlkLCB0aXRsZSwgYXJlYSwgbWF4X2F0dGVuZGVlcykge1xuXHRyZXR1cm4gdGhpcy5hcGkudXBkYXRlKHRoaXMuY29sbGVjdGlvbiwgaWQsIHtcblx0XHR0aXRsZTogdGl0bGUsXG5cdFx0YXJlYTogYXJlYSxcblx0XHRcIm1heC1hdHRlbmRlZXNcIjogbWF4X2F0dGVuZGVlc1xuXHR9KTtcbn1cblxuLyoqXG4gKiBSZW1vdmUgYW4gZXhpc3RpbmcgbG9jYXRpb24gZnJvbSB0aGUgY29udmVudGlvblxuICogQHBhcmFtIGlkIE51bWJlciBpZCBvZiB0aGUgbG9jYXRpb24gdG8gcmVtb3ZlXG4gKiBAcmV0dXJuIGEgcHJvbWlzZSB0aGF0IHdpbGwgcmVzb2x2ZSB0byBhIHByb3BlcnR5IGxpc3QgY29udGlhbmluZyBhIGJvb2xlYW4gc3RhdHVzIGZpZWxkXG4gKi9cbkNvblRyb2xsTG9jYXRpb25zLnByb3RvdHlwZS5yZW1vdmUgPSBmdW5jdGlvbihpZCkge1xuXHRyZXR1cm4gdGhpcy5hcGkuZGVsKHRoaXMuY29sbGVjdGlvbiwgaWQpO1xufVxuXG5tb2R1bGUuZXhwb3J0cyA9IENvblRyb2xsTG9jYXRpb25zO1xuIiwiLyoqXG4gKiBodHRwOi8vdXNlanNkb2Mub3JnL1xuICovXG5cbid1c2Ugc3RyaWN0JztcblxuLyoqXG4gKiBDb25Ucm9sbCBNYW5hZ2VycyBBUElcbiAqIEBwYXJhbSBDb25Ucm9sbCBhcGlcbiAqL1xudmFyIENvblRyb2xsTWFuYWdlcnMgPSBmdW5jdGlvbihhcGkpIHtcblx0dGhpcy5hcGkgPSBhcGk7XG5cdHRoaXMuY29sbGVjdGlvbiA9IHtjb252ZW50aW9uOiB0cnVlLCBjb2xsZWN0aW9uOiAnbWFuYWdlcnMnfTtcblx0cmV0dXJuIHRoaXM7XG59O1xuXG4vKipcbiAqIFJldHVybiBhIGxpc3Qgb2YgYWxsIGNvbnZlbnRpb24gbWFuYWdlbWVudCB1c2VycyAoZm9yIGFsbCByb2xlcylcbiAqIEByZXR1cm4gYSBwcm9taXNlIHRoYXQgd2lsbCByZXNvbHZlIHdpdGggdGhlIGxpc3Qgb2YgYWxsIG1hbmFnZW1lbnQgb2JqZWN0c1xuICovXG5Db25Ucm9sbE1hbmFnZXJzLnByb3RvdHlwZS5jYXRhbG9nID0gZnVuY3Rpb24oKSB7XG5cdHJldHVybiB0aGlzLmFwaS5nZXQodGhpcy5jb2xsZWN0aW9uKTtcbn07XG5cbi8qKlxuICogQWRkIGEgbmV3IG1hbmFnZW1lbnQgcm9sZSB0byB0aGUgY29udmVudGlvblxuICogQHBhcmFtIHVzZXJPYmogT2JqZWN0IGEgbWFuYWdlbWVudCBvYmplY3QgdG8gY3JlYXRlXG4gKiBAcmV0dXJuIGEgcHJvbWlzZSB0aGF0IHdpbGwgcmVzb2x2ZSB3aXRoIHRoZSBuZXcgbWFuYWdlbWVudCBvYmplY3RcbiAqL1xuQ29uVHJvbGxNYW5hZ2Vycy5wcm90b3R5cGUuYWRkID0gZnVuY3Rpb24odXNlck9iaikge1xuXHRyZXR1cm4gdGhpcy5hcGkuY3JlYXRlKHRoaXMuY29sbGVjdGlvbiwgdXNlck9iaik7XG59O1xuXG4vKipcbiAqIFJlbW92ZSBhIG1hbmFnZW1lbnQgcm9sZSBmcm9tIHRoZSBjb252ZW50aW9uXG4gKiBAcGFyYW0gaWQgTnVtYmVyIGlkIG9mIG1hbmFnZW1lbnQgb2JqZWN0IHRvIHJlbW92ZVxuICogQHJldHVybiBhIHByb21pc2UgdGhhdCB3aWxsIHJlc29sdmUgd2l0aCBhIHByb3BlcnR5IGxpc3QgY29udGFpbmluZyBhIGJvb2xlYW4gc3RhdHVzIGZpZWxkXG4gKi9cbkNvblRyb2xsTWFuYWdlcnMucHJvdG90eXBlLnJlbW92ZSA9IGZ1bmN0aW9uKGlkKSB7XG5cdHRoaXMuYXBpLmRlbCh0aGlzLmNvbGxlY3Rpb24sIGlkKTtcbn07XG5cbm1vZHVsZS5leHBvcnRzID0gQ29uVHJvbGxNYW5hZ2VycztcbiIsIi8qKlxuICogaHR0cDovL3VzZWpzZG9jLm9yZy9cbiAqL1xuXG4ndXNlIHN0cmljdCc7XG5cbi8qKlxuICogQ29uVHJvbGwgTWVyY2hhbmRpc2UgQVBJXG4gKiBAcGFyYW0gQ29uVHJvbGwgYXBpXG4gKi9cbnZhciBDb25Ucm9sbE1lcmNoYW5kaXNlID0gZnVuY3Rpb24oYXBpKSB7XG5cdHRoaXMuYXBpID0gYXBpO1xuXHR0aGlzLmNvbGxlY3Rpb24gPSB7Y29udmVudGlvbjogdHJ1ZSwgY29sbGVjdGlvbjogJ21lcmNoYW5kaXNlc2t1cyd9O1xuXHRyZXR1cm4gdGhpcztcbn07XG5cbi8qKlxuICogUmV0cmlldmUgdGhlIGNhdGFsb2cgb2YgYWxsIGN1cnJlbnRseSBhdmFpbGFibGUgbWVyY2hhbmRpc2UgaW4gdGhlIGNvbnZlbnRpb25cbiAqIEByZXR1cm4gYSBwcm9taXNlIHRoYXQgd2lsbCByZXNvbHZlIHdpdGggdGhlIGxpc3Qgb2YgbWVyY2hhbmRpc2Ugb2JqZWN0c1xuICovXG5Db25Ucm9sbE1lcmNoYW5kaXNlLnByb3RvdHlwZS5jYXRhbG9nID0gZnVuY3Rpb24oKSB7XG5cdHJldHVybiB0aGlzLmFwaS5nZXQodGhpcy5jb2xsZWN0aW9uKTtcbn07XG5cbi8qKlxuICogQ3JlYXRlIGEgbmV3IG1lcmNoYW5kaXNlIGl0ZW0gZm9yIHRoZSBjb252ZW50aW9uXG4gKiBAcGFyYW0gdGl0bGUgU3RyaW5nIHRoZSB0aXRsZSBmb3IgdGhlIG5ldyBpdGVtXG4gKiBAcGFyYW0gY29kZSBTdHJpbmcgYW4gU0tVIHByb3BlcnRpZXMgY29kZSAtIGEgY29sb24gZGVsaW1pdGVkIGxpc3Qgb2YgcHJvcGVydGllc1xuICogQHBhcmFtIHByaWNlIE51bWJlcnxTdHJpbmcgcHJpY2Ugb2YgdGhlIG5ldyBpdGVtXG4gKiBAcGFyYW0gZGVzY3JpcHRpb24gU3RyaW5nIGEgbG9uZyB0ZXh0dWFsIGRlc2NyaXB0aW9uIG9mIHRoZSBuZXcgaXRlbVxuICogQHJldHVybiBhIHByb21pc2UgdGhhdCB3aWxsIHJlc29sdmUgd2l0aCB0aGUgbmV3IG1lcmNoYW5kaXNlIGl0ZW0gb2JqZWN0XG4gKi9cbkNvblRyb2xsTWVyY2hhbmRpc2UucHJvdG90eXBlLmNyZWF0ZSA9IGZ1bmN0aW9uKHRpdGxlLCBjb2RlLCBwcmljZSwgZGVzY3JpcHRpb24pIHtcblx0dmFyIGRhdGEgPSB7XG5cdFx0XHR0aXRsZTogdGl0bGUsXG5cdFx0XHRjb2RlOiBjb2RlLFxuXHRcdFx0cHJpY2U6IHByaWNlLFxuXHRcdFx0ZGVzY3JpcHRpb246IGRlc2NyaXB0aW9uXG5cdFx0fTtcblx0cmV0dXJuIHRoaXMuYXBpLmNyZWF0ZSh0aGlzLmNvbGxlY3Rpb24sIGRhdGEpO1xufTtcblxuLyoqXG4gKiBSZW1vdmUgYW4gZXhpc3RpbmcgbWVyY2hhbmRpc2UgaXRlbSBmcm9tIHRoZSBjb252ZW50aW9uXG4gKiBAcGFyYW0gaWQgTnVtYmVyIGlkIG9mIHRoZSBtZXJjaGFuZGlzZSBpdGVtIHRvIHJlbW92ZVxuICogQHJldHVybiBhIHByb21pc2UgdGhhdCB3aWxsIHJlc29sdmUgdG8gYSBwcm9wZXJ0eSBsaXN0IGNvbnRhaW5pbmcgYSBib29sZWFuIHN0YXR1cyBmaWVsZFxuICovXG5Db25Ucm9sbE1lcmNoYW5kaXNlLnByb3RvdHlwZS5yZW1vdmUgPSBmdW5jdGlvbihpZCkge1xuXHRyZXR1cm4gdGhpcy5hcGkuZGVsKHRoaXMuY29sbGVjdGlvbiwgaWQpO1xufTtcblxuLyoqXG4gKiBVcGFkdGUgYW4gZXhpc3RpbmcgbWVyY2hhbmRpc2Ugb2JqZWN0IGluIHRoZSBjb252ZW50aW9uXG4gKiBAcGFyYW0gaWQgTnVtYmVyIGlkIG9mIHRoZSBtZXJjaGFuZGlzZSBpdGVtIHRvIHVwZGF0ZVxuICogQHBhcmFtIHRpdGxlIFN0cmluZyBuZXcgdGl0bGUgdG8gc2V0IGZvciB0aGUgbWVyY2hhbmRpc2UgaXRlbVxuICogQHBhcmFtIHByaWNlIE51bWJlcnxTdHJpbmcgbmV3IHByaWNlIHRvIHNldCBmb3IgdGhlIG1lcmNoYW5kaXNlIGl0ZW1cbiAqIEByZXR1cm4gYSBwcm9taXNlIHRoYXQgd2lsbCByZXNvbHZlIHRvIGEgcHJvcGVydHkgbGlzdCBjb250YWluaW5nIGEgYm9vbGVhbiBzdGF0dXMgZmllbGRcbiAqL1xuQ29uVHJvbGxNZXJjaGFuZGlzZS5wcm90b3R5cGUudXBkYXRlID0gZnVuY3Rpb24oaWQsIHRpdGxlLCBwcmljZSkge1xuXHRyZXR1cm4gdGhpcy5hcGkudXBkYXRlKHRoaXMuY29sbGVjdGlvbiwgaWQsIHtcblx0XHR0aXRsZTogdGl0bGUsXG5cdFx0cHJpY2U6IHByaWNlXG5cdH0pO1xufTtcblxubW9kdWxlLmV4cG9ydHMgPSBDb25Ucm9sbE1lcmNoYW5kaXNlO1xuIiwiLyoqXG4gKiBodHRwOi8vdXNlanNkb2Mub3JnL1xuICovXG5cbid1c2Ugc3RyaWN0JztcblxuLyoqXG4gKiBDb25Ucm9sbCBQYXNzIEFQSVxuICogQHBhcmFtIENvblRyb2xsIGFwaVxuICovXG5jb25zdCBDb25Ucm9sbFBhc3NlcyA9IGZ1bmN0aW9uKGFwaSkge1xuXHR0aGlzLmFwaSA9IGFwaTtcblx0dGhpcy5jb2xsZWN0aW9uID0geyBjb252ZW50aW9uOiB0cnVlLCBjb2xsZWN0aW9uOiAncGFzc2VzJyB9O1xuXHRyZXR1cm4gdGhpcztcbn07XG5cbi8qKlxuICogUmV0cmlldmUgdGhlIGxpc3Qgb2YgYWxsIGF2YWlsYWJsZSBwYXNzZXMgKGluY2x1ZGluZyBub24tcHVibGljIG9uZXMpXG4gKiBAcmV0dXJuIGEgcHJvbWlzZSB0aGF0IHdpbGwgcmVzb2x2ZSB0byB0aGUgbGlzdCBvZiBwYXNzIG9iamVjdHNcbiAqL1xuQ29uVHJvbGxQYXNzZXMucHJvdG90eXBlLmNhdGFsb2cgPSBmdW5jdGlvbigpIHtcblx0cmV0dXJuIHRoaXMuYXBpLmdldCh0aGlzLmNvbGxlY3Rpb24sICc/YWxsPTEnKTtcbn07XG5cbm1vZHVsZS5leHBvcnRzID0gQ29uVHJvbGxQYXNzZXM7XG4iLCIvKipcbiAqIGh0dHA6Ly91c2Vqc2RvYy5vcmcvXG4gKi9cblxuJ3VzZSBzdHJpY3QnO1xuXG4vKipcbiAqIENvblRyb2xsIFB1cmNoYXNlcyBBUElcbiAqIEBwYXJhbSBDb25Ucm9sbCBhcGlcbiAqL1xudmFyIENvblRyb2xsUHVyY2hhc2VzID0gZnVuY3Rpb24oYXBpKSB7XG5cdHRoaXMuYXBpID0gYXBpO1xuXHR0aGlzLmNvbGxlY3Rpb24gPSB7Y29udmVudGlvbjogdHJ1ZSwgY29sbGVjdGlvbjogJ3B1cmNoYXNlcyd9O1xuXHRyZXR1cm4gdGhpcztcbn07XG5cbi8qKlxuICogUmV0cmlldmUgdGhlIGxpc3Qgb2YgcHVyY2hhc2VkIGl0ZW1zIGluIHRoZSBjb252ZW50aW9uXG4gKiBAcmV0dXJuIGEgcHJvbWlzZSB0aGF0IHdpbGwgcmVzb2x2ZSB0byB0aGUgbGlzdCBvZiBwdXJjaGFzZSBvYmplY3RzXG4gKi9cbkNvblRyb2xsUHVyY2hhc2VzLnByb3RvdHlwZS5jYXRhbG9nID0gZnVuY3Rpb24oKSB7XG5cdHJldHVybiB0aGlzLmFwaS5nZXQodGhpcy5jb2xsZWN0aW9uLCAnP2FsbD0xJyk7XG59O1xuXG5tb2R1bGUuZXhwb3J0cyA9IENvblRyb2xsUHVyY2hhc2VzO1xuIiwiLyoqXG4gKiBodHRwOi8vdXNlanNkb2Mub3JnL1xuICovXG5cbid1c2Ugc3RyaWN0JztcblxuLyoqXG4gKiBDb25Ucm9sbCBTeXN0ZW0gVGFncyBBUElcbiAqIEBwYXJhbSBDb25Ucm9sbCBhcGlcbiAqL1xudmFyIENvblRyb2xsVGFncyA9IGZ1bmN0aW9uKGFwaSkge1xuXHR0aGlzLmFwaSA9IGFwaTtcblx0dGhpcy5jb2xsZWN0aW9uID0ge2NvbnZlbnRpb246IHRydWUsIGNvbGxlY3Rpb246ICd0YWd0eXBlcyd9O1xuXHRyZXR1cm4gdGhpcztcbn07XG5cbi8qKlxuICogUmV0cmlldmUgdGhlIGxpc3Qgb2Ygc3lzdGVtIHRhZ3NcbiAqIEByZXR1cm4gYSBwcm9taXNlIHRoYXQgd2lsbCByZXNvbHZlIHRvIHRoZSBsaXN0IG9mIHN5c3RlbSB0YWcgb2JqZWN0c1xuICovXG5Db25Ucm9sbFRhZ3MucHJvdG90eXBlLmNhdGFsb2cgPSBmdW5jdGlvbigpIHtcblx0cmV0dXJuIHRoaXMuYXBpLmdldCh0aGlzLmNvbGxlY3Rpb24pO1xufTtcblxuLyoqXG4gKiBSZXR1cm4gYSBzcGVjaWZpYyBzeXN0ZW0gdGFnXG4gKiBAcGFyYW0gdGl0bGUgU3RyaW5nIHRpdGxlIG9yIHNsdWcgb2YgdGhlIHN5c3RlbSB0YWcgdG8gcmV0cmlldmVcbiAqIEByZXR1cm4gYSBwcm9taXNlIHRoYXQgd2lsbCByZXNvbHZlIHRvIHRoZSBzeXN0ZW0gdGFnIG9iamVjdFxuICovXG5Db25Ucm9sbFRhZ3MucHJvdG90eXBlLmdldFR5cGUgPSBmdW5jdGlvbih0aXRsZSkge1xuXHRyZXR1cm4gdGhpcy5hcGkuZ2V0KHRoaXMuY29sbGVjdGlvbiwgdGl0bGUpO1xufTtcblxuLyoqXG4gKiBBZGQgYSBzeXN0ZW0gdGFnIHRvIHRoZSBjb252ZW50aW9uXG4gKiBAcGFyYW0gdGl0bGUgU3RyaW5nIHRpdGxlIG9mIHRoZSBzeXN0ZW0gdGFnIHRvIGFkZFxuICogQHBhcmFtIHJlcXVpcmVtZW50IFN0cmluZyB0aGUgdHlwZSBvZiByZXF1aXJlbWVudCBmb3IgdGhpcyB0YWcuIE9uZSBvZjpcbiAqICAqIE9wdGlvbmFsXG4gKiAgKiBPbmVcbiAqICAqIE11bHRpcGxlXG4gKiBAcGFyYW0gaXNwdWJsaWMgQm9vbGVhbiB3aGV0aGVyIHRoZSBuZXcgdGFnIGlzIHRvIGJlIGV4cG9zZWQgdG8gbm9uLW1hbmFnZXJzXG4gKiBAcmV0dXJuIGEgcHJvbWlzZSB0aGF0IHdpbGwgcmVzb2x2ZSB0byBhIHByb3BlcnR5IGxpc3Qgd2l0aCBhIGJvb2xlYW4gc3RhdHVzIGZpZWxkXG4gKi9cbkNvblRyb2xsVGFncy5wcm90b3R5cGUuYWRkVHlwZSA9IGZ1bmN0aW9uKHRpdGxlLCByZXF1aXJlbWVudCwgaXNwdWJsaWMpIHtcblx0cmV0dXJuIHRoaXMuYXBpLmNyZWF0ZSh0aGlzLmNvbGxlY3Rpb24sIHtcblx0XHR0aXRsZTogdGl0bGUsXG5cdFx0cmVxdWlyZW1lbnQ6IHJlcXVpcmVtZW50LFxuXHRcdFwicHVibGljXCI6IGlzcHVibGljXG5cdH0pO1xufTtcblxuLyoqXG4gKiBVcGRhdGUgYW4gZXhpc3Rpbmcgc3lzdGVtIHRhZ1xuICogQHBhcmFtIG9sZHRpdGxlIFN0cmluZyB0aXRsZSBvciBzbHVnIG9mIHRoZSBzeXN0ZW0gdGFnIHRvIHVwZGF0ZVxuICogQHBhcmFtIG5ld3RpdGxlIFN0cmluZyB1cGRhdGUgdGl0bGUgdG8gY2hhbmdlIHRvXG4gKiBAcGFyYW0gcmVxdWlyZW1lbnQgU3RyaW5nIHVwZGF0ZWQgcmVxdWlyZW1lbnQgdG8gY2hhbmdlIHRvIChzZWUgI2FkZFR5cGUgZm9yIGRldGFpbHMpXG4gKiBAcGFyYW0gaXNwdWJsaWMgQm9vbGVhbiBuZXcgcHVibGljL3ByaXZhdGUgc3RhdGUgdG8gc2V0XG4gKiBAcmV0dXJuIGEgcHJvbWlzZSB0aGF0IHdpbGwgcmVzb2x2ZSB0byBhIHByb3BlcnR5IGxpc3Qgd2l0aCBhIGJvb2xlYW4gc3RhdHVzIGZpZWxkXG4gKi9cbkNvblRyb2xsVGFncy5wcm90b3R5cGUudXBkYXRlVHlwZSA9IGZ1bmN0aW9uKG9sZHRpdGxlLCBuZXd0aXRsZSwgcmVxdWlyZW1lbnQsIGlzcHVibGljKSB7XG5cdHJldHVybiB0aGlzLmFwaS51cGRhdGUodGhpcy5jb2xsZWN0aW9uLCBvbGR0aXRsZSwge1xuXHRcdHRpdGxlOiBuZXd0aXRsZSxcblx0XHRyZXF1aXJlbWVudDogcmVxdWlyZW1lbnQsXG5cdFx0XCJwdWJsaWNcIjogaXNwdWJsaWNcblx0fSk7XG59O1xuXG4vKipcbiAqIFJlbW92ZSBhbmQgZXhpc3Rpbmcgc3lzdGVtIHRhZ1xuICogQHBhcmFtIHRpdGxlIFN0cmluZyB0aXRsZSBvciBzbHVnIG9mIHRoZSBzeXN0ZW0gdGFnIHRvIHJlbW92ZVxuICogQHJldHVybiBhIHByb21pc2UgdGhhdCB3aWxsIHJlc29sdmUgdG8gYSBwcm9wZXJ0eSBsaXN0IHdpdGggYSBib29sZWFuIHN0YXR1cyBmaWVsZFxuICovXG5Db25Ucm9sbFRhZ3MucHJvdG90eXBlLmRlbGV0ZVR5cGUgPSBmdW5jdGlvbih0aXRsZSkge1xuXHRyZXR1cm4gdGhpcy5hcGkuZGVsKHRoaXMuY29sbGVjdGlvbiwgdGl0bGUpO1xufTtcblxuLyoqXG4gKiBSZXBsYWNlIGFuIG9wdGlvbiB2YWx1ZSBpbiBhbiBleGlzdGluZyBzeXN0ZW0gdGFnXG4gKiBAcGFyYW0gdGl0bGUgU3RyaW5nIHRpdGxlIG9yIHNsdWcgb2YgdGhlIHN5c3RlbSB0YWcgd2hlcmUgYSB2YWx1ZSBpcyB0byBiZSB1cGRhdGVkXG4gKiBAcGFyYW0gb2xkdmFsdWUgU3RyaW5nIG9sZCB2YWx1ZSB0byByZXBsYWNlXG4gKiBAcGFyYW0gbmV3dmFsdWUgU3RyaW5nIG5ldyB2YWx1ZSB0byB1c2UgaW5zdGVhZFxuICogQHJldHVybiBhIHByb21pc2UgdGhhdCB3aWxsIHJlc29sdmUgdG8gYSBwcm9wZXJ0eSBsaXN0IHdpdGggYSBib29sZWFuIHN0YXR1cyBmaWVsZFxuICovXG5Db25Ucm9sbFRhZ3MucHJvdG90eXBlLnJlcGxhY2VWYWx1ZSA9IGZ1bmN0aW9uKHRpdGxlLCBvbGR2YWx1ZSwgbmV3dmFsdWUpIHtcblx0dmFyIHJlcGxhY2VfdmFsdWVzID0ge307XG5cdHJlcGxhY2VfdmFsdWVzW29sZHZhbHVlXSA9IG5ld3ZhbHVlO1xuXHRyZXR1cm4gdGhpcy5hcGkudXBkYXRlKHRoaXMuY29sbGVjdGlvbiwgdGl0bGUsIHtcblx0XHRcInJlcGxhY2UtdmFsdWVzXCI6IHJlcGxhY2VfdmFsdWVzXG5cdH0pO1xufTtcblxuLyoqXG4gKiBBZGQgYSBuZXcgb3B0aW9uIHZhbHVlIHRvIGFuIGV4aXN0aW5nIHN5c3RlbSB0YWdcbiAqIEBwYXJhbSB0aXRsZSBTdHJpbmcgdGl0bGUgb3Igc2x1ZyBvZiB0aGUgc3lzdGVtIHRhZyB3aGVyZSBhIHZhbHVlIGlzIHRvIGJlIHVwZGF0ZWRcbiAqIEBwYXJhbSBuZXd2YWx1ZSBTdHJpbmcgbmV3IHZhbHVlIHRvIGFkZCBhcyBhbiBvcHRpb24gdG8gdGhlIHN5c3RlbSB0YWdcbiAqIEByZXR1cm4gYSBwcm9taXNlIHRoYXQgd2lsbCByZXNvbHZlIHRvIGEgcHJvcGVydHkgbGlzdCB3aXRoIGEgYm9vbGVhbiBzdGF0dXMgZmllbGRcbiAqL1xuQ29uVHJvbGxUYWdzLnByb3RvdHlwZS5hZGRWYWx1ZSA9IGZ1bmN0aW9uKHRpdGxlLCBuZXd2YWx1ZSkge1xuXHRyZXR1cm4gdGhpcy5hcGkudXBkYXRlKHRoaXMuY29sbGVjdGlvbiwgdGl0bGUsIHtcblx0XHR2YWx1ZXM6IFsgbmV3dmFsdWUgXVxuXHR9KTtcbn07XG5cbi8qKlxuICogRGVsZXRlIGFuIGV4aXN0aW5nIG9wdGlvbiBvbiBhbiBleGlzdGluZyBzeXN0ZW0gdGFnXG4gKiBAcGFyYW0gdGl0bGUgU3RyaW5nIHRpdGxlIG9yIHNsdWcgb2YgdGhlIHN5c3RlbSB0YWcgd2hlcmUgYSB2YWx1ZSBpcyB0byBiZSB1cGRhdGVkXG4gKiBAcGFyYW0gdmFsdWUgU3RyaW5nIHRoZSBleGlzdGluZyBvcHRpb24gdmFsdWUgdG8gcmVtb3ZlXG4gKiBAcmV0dXJuIGEgcHJvbWlzZSB0aGF0IHdpbGwgcmVzb2x2ZSB0byBhIHByb3BlcnR5IGxpc3Qgd2l0aCBhIGJvb2xlYW4gc3RhdHVzIGZpZWxkXG4gKi9cbkNvblRyb2xsVGFncy5wcm90b3R5cGUuZGVsZXRlVmFsdWUgPSBmdW5jdGlvbih0aXRsZSwgdmFsdWUpIHtcblx0cmV0dXJuIHRoaXMuYXBpLnVwZGF0ZSh0aGlzLmNvbGxlY3Rpb24sIHRpdGxlLCB7XG5cdFx0XCJyZW1vdmUtdmFsdWVzXCI6IFsgdmFsdWUgXVxuXHR9KTtcbn07XG5cbm1vZHVsZS5leHBvcnRzID0gQ29uVHJvbGxUYWdzO1xuIiwiLyoqXG4gKiBodHRwOi8vdXNlanNkb2Mub3JnL1xuICovXG5cbid1c2Ugc3RyaWN0JztcblxuLyoqXG4gKiBDb25Ucm9sbCBUaWNrZXRzIEFQSVxuICogQHBhcmFtIENvblRyb2xsIGFwaVxuICovXG52YXIgQ29uVHJvbGxUaWNrZXRzID0gZnVuY3Rpb24oYXBpKSB7XG5cdHRoaXMuYXBpID0gYXBpO1xuXHR0aGlzLmNvbGxlY3Rpb24gPSB7Y29udmVudGlvbjogdHJ1ZSwgY29sbGVjdGlvbjogJ3RpY2tldHMnfTtcblx0cmV0dXJuIHRoaXM7XG59O1xuXG4vKipcbiAqIFJldHJpZXZlIGFsbCB0aWNrZXRzIGF1dGhvcml6ZWQgZm9yIHRoZSBzcGVjaWZpZWQgZXZlbnRcbiAqIEBwYXJhbSBldmVudElkIE51bWJlciBpZCBvZiB0aGUgZXZlbnQgdG8gbGlzdCB0aWNrZXRzIGZvclxuICogQHJldHVybiBhIHByb21pc2UgdGhhdCB3aWxsIHJlc29sdmUgdG8gdGhlIGxpc3Qgb2YgdGlja2V0IG9iamVjdHNcbiAqL1xuQ29uVHJvbGxUaWNrZXRzLnByb3RvdHlwZS5mb3JFdmVudCA9IGZ1bmN0aW9uKGV2ZW50SWQpIHtcblx0cmV0dXJuIHRoaXMuYXBpLmdldCh0aGlzLmNvbGxlY3Rpb24sICc/YWxsPTEmaXNfdmFsaWQ9MSZieV9ldmVudD0nICsgZXZlbnRJZCk7XG59O1xuXG4vKipcbiAqIFJldHJpZXZlIGFsbCB0aWNrZXRzIGF1dGhvcml6ZWQgZm9yIHRoZSBzcGVjaWZpZWQgdGltZSBzbG90XG4gKiBAcGFyYW0gdGltZXNsb3RJZCBOdW1iZXIgaWQgb2YgdGhlIHRpbWVzbG90IHRvIGxpc3QgdGlja2V0cyBmb3JcbiAqIEByZXR1cm4gYSBwcm9taXNlIHRoYXQgd2lsbCByZXNvbHZlIHRvIHRoZSBsaXN0IG9mIHRpY2tldCBvYmplY3RzXG4gKi9cbkNvblRyb2xsVGlja2V0cy5wcm90b3R5cGUuZm9yVGltZXNsb3QgPSBmdW5jdGlvbih0aW1lc2xvdElkKSB7XG5cdHJldHVybiB0aGlzLmFwaS5nZXQodGhpcy5jb2xsZWN0aW9uLCAnP2FsbD0xJmlzX3ZhbGlkPTEmYnlfdGltZXNsb3Q9JyArIHRpbWVzbG90SWQpO1xufTtcblxuLyoqXG4gKiBSZXRyaWV2ZSBhbGwgdGlja2V0cyBhdXRob3JpemVkIGZvciB0aGUgc3BlY2lmaWVkIHVzZXJcbiAqIEBwYXJhbSB1c2VySWQgTnVtYmVyIGlkIG9mIHRoZSB1c2VyIHRvIGxpc3QgdGlja2V0cyBmb3JcbiAqIEByZXR1cm4gYSBwcm9taXNlIHRoYXQgd2lsbCByZXNvbHZlIHRvIHRoZSBsaXN0IG9mIHRpY2tldCBvYmplY3RzXG4gKi9cbkNvblRyb2xsVGlja2V0cy5wcm90b3R5cGUuZm9yVXNlciA9IGZ1bmN0aW9uKHVzZXJJZCkge1xuXHRyZXR1cm4gdGhpcy5hcGkuZ2V0KHRoaXMuY29sbGVjdGlvbiwgJz9hbGw9MSZpc192YWxpZD0xJmJ5X3VzZXI9JyArIHVzZXJJZCk7XG59O1xuXG4vKipcbiAqIFJldHJpZXZlIGFsbCB0aWNrZXRzIGF1dGhvcml6ZWQgaW4gdGhlIGNvbnZlbnRpb25cbiAqIEByZXR1cm4gYSBwcm9taXNlIHRoYXQgd2lsbCByZXNvbHZlIHRvIHRoZSBsaXN0IG9mIHRpY2tldCBvYmplY3RzXG4gKi9cbkNvblRyb2xsVGlja2V0cy5wcm90b3R5cGUuY2F0YWxvZyA9IGZ1bmN0aW9uKCkge1xuXHRyZXR1cm4gdGhpcy5hcGkuZ2V0KHRoaXMuY29sbGVjdGlvbiwgJz9hbGw9MSZpc192YWxpZD0xJyk7XG59O1xuXG4vKipcbiAqIFJlZ2lzdGVyIGEgdGlja2V0IGZvciB0aGUgc3BlY2lmaWVkIHVzZXIsIG9uIHRoZSBzcGVjaWZpZWQgdGltZXNsb3Qgd2l0aCBcbiAqIHRoZSBzcGVjaWZpZWQgcGFzcy5cbiAqIEBwYXJhbSB1c2VyIE51bWJlcnxTdHJpbmcgaWQgb3IgZW1haWwgb2YgdGhlIHVzZXIgdG8gcmVnaXN0ZXIgYSB0aWNrZXQgZm9yXG4gKiBAcGFyYW0gdGltZXNsb3QgTnVtYmVyIGlkIG9mIHRoZSB0aW1lc2xvdCB0byByZWdpc3RlciBhIHRpY2tldCBmb3JcbiAqIEBwYXJhbSBwYXNzSWQgTnVtYmVyfEFycmF5W051bWJlcl0gaWQgb2YgdGhlIHBhc3Mgb3IgcGFzc2VzIHRvIHJlZ2lzdGVyIHRpY2tldHMgZm9yXG4gKiBAcmV0dXJuIGEgcHJvbWlzZSB0aGF0IHdpbGwgcmVzb2x2ZSB0byB0aGUgbmV3bHkgY3JlYXRlZCB0aWNrZXQgb3IgYSBsaXN0IG9mIHRpY2t0ZXNcbiAqL1xuQ29uVHJvbGxUaWNrZXRzLnByb3RvdHlwZS5jcmVhdGUgPSBmdW5jdGlvbih1c2VyLCB0aW1lc2xvdCwgcGFzc0lkKSB7XG5cdHZhciBkYXRhID0ge1xuXHRcdFx0dGltZXNsb3Q6IHRpbWVzbG90LFxuXHRcdFx0dXNlcl9wYXNzZXM6IHBhc3NJZCxcblx0XHRcdHVzZXI6IHVzZXJcblx0fTtcblx0cmV0dXJuIHRoaXMuYXBpLmNyZWF0ZSh0aGlzLmNvbGxlY3Rpb24sIGRhdGEpO1xufTtcblxuLyoqXG4gKiBSZW1vdmUgYSB0aWNrZXQgZnJvbSByZWdpc3RyYXRpb25cbiAqIEBwYXJhbSBpZCBOdW1iZXIgaWQgb2YgdGhlIHRpY2tldCB0byBjYW5jZWxcbiAqIEBwYXJhLCByZWZ1bmRUeXBlSWQgTnVtYmVyIGlkIG9mIHRoZSBjb3Vwb24gdHlwZSB0byBjcmVhdGUgYXMgYSByZWZ1bmQsIGlmIG5lZWRlZFxuICogQHJldHVybiBhIHByb21pc2UgdGhhdCB3aWxsIHJlc29sdmUgdG8gYSBwcm9wZXJ0eSBsaXN0IGNvbnRhaW5pbmcgYSBib29sZWFuIHN0YXR1cyBmaWVsZFxuICovXG5Db25Ucm9sbFRpY2tldHMucHJvdG90eXBlLnJlbW92ZSA9IGZ1bmN0aW9uKGlkLCByZWZ1bmRUeXBlSWQpIHtcblx0dmFyIHF1ZXJ5ID0gaWQ7XG5cdGlmIChyZWZ1bmRUeXBlSWQpIFxuXHRcdHF1ZXJ5ICs9ICc/cmVmdW5kLWNvdXBvbi10eXBlPScrIHJlZnVuZFR5cGVJZDtcblx0cmV0dXJuIHRoaXMuYXBpLmRlbCh0aGlzLmNvbGxlY3Rpb24sIHF1ZXJ5KTtcbn07XG5cbm1vZHVsZS5leHBvcnRzID0gQ29uVHJvbGxUaWNrZXRzO1xuIiwiLyoqXG4gKiBodHRwOi8vdXNlanNkb2Mub3JnL1xuICovXG5cbid1c2Ugc3RyaWN0JztcblxuLyoqXG4gKiBDb25Ucm9sbCBUaW1lIFNsb3RzIEFQSVxuICogQHBhcmFtIENvblRyb2xsIGFwaVxuICovXG52YXIgQ29uVHJvbGxUaW1lc2xvdHMgPSBmdW5jdGlvbihhcGkpIHtcblx0dGhpcy5hcGkgPSBhcGk7XG5cdHRoaXMuY29sbGVjdGlvbiA9IHtjb252ZW50aW9uOiB0cnVlLCBjb2xsZWN0aW9uOiAndGltZXNsb3RzJ307XG5cdHJldHVybiB0aGlzO1xufTtcblxuLyoqXG4gKiBSZXRyaWV2ZSBhbGwgdGltZXNsb3RzIGZvciB0aGUgY29udmVudGlvblxuICogQHJldHVybiBhIHByb21pc2UgdGhhdCB3aWxsIHJlc29sdmUgdG8gYSBsaXN0IG9mIHRpbWVzbG90IG9iamVjdHNcbiAqL1xuQ29uVHJvbGxUaW1lc2xvdHMucHJvdG90eXBlLmNhdGFsb2cgPSBmdW5jdGlvbigpIHtcblx0cmV0dXJuIHRoaXMuYXBpLmdldCh0aGlzLmNvbGxlY3Rpb24pO1xufTtcblxuLyoqXG4gKiBSZXRyaWV2ZSBhbGwgdGltZXNsb3RzIGZvciBhIHNwZWNpZmljIGV2ZW50XG4gKiBAcGFyYW0gZXZlbnRfaWQgTnVtYmVyIGlkIG9mIHRoZSBldmVudCB0byByZXRyaWV2ZSB0aW1lc2xvdHMgZm9yXG4gKiBAcmV0dXJuIGEgcHJvbWlzZSB0aGF0IHdpbGwgcmVzb3ZsZSB0byBhIGxpc3Qgb2YgdGltZXNsb3Qgb2JqZWN0c1xuICovXG5Db25Ucm9sbFRpbWVzbG90cy5wcm90b3R5cGUuZm9yRXZlbnQgPSBmdW5jdGlvbihldmVudF9pZCkge1xuXHRyZXR1cm4gdGhpcy5hcGkuZ2V0KHRoaXMuY29sbGVjdGlvbiwgeyBldmVudDogZXZlbnRfaWQgfSk7XG59O1xuXG4vKipcbiAqIENyZWF0ZSBhIG5ldyB0aW1lc2xvdCBmb3IgYW4gZXZlbnRcbiAqIEBwYXJhbSBldmVudF9pZCBOdW1iZXIgaWQgb2YgdGhlIGV2ZW50IHRvIGNyZWF0ZSBhIHRpbWVzbG90IGZvclxuICogQHJldHVybiBhIHByb21pc2UgdGhhdCB3aWxsIHJlc29sdmUgdG8gdGhlIGNyZWF0ZWQgdGltZXNsb3Qgb2JqZWN0XG4gKi9cbkNvblRyb2xsVGltZXNsb3RzLnByb3RvdHlwZS5jcmVhdGUgPSBmdW5jdGlvbihldmVudF9pZCwgc3RhcnRfdGltZSwgZHVyYXRpb24sIGxvY2F0aW9ucywgaG9zdHMpIHtcblx0dmFyIGRhdGEgPSB7XG5cdFx0XHRldmVudDogZXZlbnRfaWQsXG5cdFx0XHRzdGFydDogcGFyc2VJbnQoc3RhcnRfdGltZS5nZXRUaW1lKCkvMTAwMCksXG5cdFx0XHRkdXJhdGlvbjogZHVyYXRpb24sXG5cdFx0XHRsb2NhdGlvbnM6IGxvY2F0aW9uc1xuXHRcdH07XG5cdGlmIChob3N0cyAmJiBob3N0cy5sZW5ndGggPiAwKVxuXHRcdC8vIHN1Ym1pdCBob3N0cyBkaXJlY3RseSAtIGFzc3VtZXMgYSBob3N0IGlzIGFuIG9iamVjdCB3aXRoIGVpdGhlciBlbWFpbCBvciBpZCBmaWVsZHMgYW5kIG9wdGlvbmFsbHkgYSBuYW1lIC0gbGlrZSBhcGkgd2FudHMgXG5cdFx0ZGF0YS5ob3N0cyA9IGhvc3RzOyBcblx0cmV0dXJuIHRoaXMuYXBpLmNyZWF0ZSh0aGlzLmNvbGxlY3Rpb24sIGRhdGEpO1xufTtcblxuLyoqXG4gKiBSZW1vdmUgYW4gZXhpc3RpbmcgdGltZXNsb3RcbiAqIEBwYXJhbSBpZCBOdW1iZXIgaWQgb2YgdGhlIHRpbWVzbG90IHRvIHJlbW92ZVxuICogQHJldHVybiBhIHByb21pc2UgdGhhdCB3aWxsIHJlc29sdmUgdG8gYSBwcm9wZXJ0eSBsaXN0IHdpdGggYSBib29sZWFuIHN0YXR1cyBmaWVsZFxuICovXG5Db25Ucm9sbFRpbWVzbG90cy5wcm90b3R5cGUucmVtb3ZlID0gZnVuY3Rpb24oaWQpIHtcblx0cmV0dXJuIHRoaXMuYXBpLmRlbCh0aGlzLmNvbGxlY3Rpb24sIGlkKTtcbn07XG5cbi8qKlxuICogVXBkYXRlIHRoZSB0aW1lc2xvdCBsb2NhdGlvbiBsaXN0XG4gKiBAcGFyYW0gaWQgTnVtYmVyIGlkIG9mIHRoZSB0aW1lc2xvdCB0byB1cGRhdGVcbiAqIEBwYXJhbSB0b0FkZCBBcnJheVtPYmplY3R8U3RyaW5nXSBsaXN0IG9mIGxvY2F0aW9uIG9iamVjdHMgb3IgbG9jYXRpb24gc2x1Z3MgdG8gYWRkXG4gKiAgIHRvIHRoZSB0aW1lc2xvdFxuICogQHBhcmFtIHRvUmVtb3ZlIEFycmF5W09iamVjdHxTdHJpbmddIGxpc3Qgb2YgbG9jYXRpb24gb2JqZWN0cyBvciBsb2NhdGlvbiBzbHVncyB0b1xuICogICByZW1vdmUgZnJvbSB0aGUgdGltZXNsb3RcbiAqIEByZXR1cm4gYSBwcm9taXNlIHRoYXQgd2lsbCByZXNvbHZlIHRvIGEgcHJvcGVydHkgbGlzdCB3aXRoIGEgYm9vbGVhbiBzdGF0dXMgZmllbGRcbiAqL1xuQ29uVHJvbGxUaW1lc2xvdHMucHJvdG90eXBlLnVwZGF0ZUxvY2F0aW9ucyA9IGZ1bmN0aW9uKGlkLCB0b0FkZCwgdG9SZW1vdmUpIHtcblx0cmV0dXJuIHRoaXMuYXBpLnVwZGF0ZSh0aGlzLmNvbGxlY3Rpb24sIGlkLCB7XG5cdFx0J2xvY2F0aW9ucyc6IHRvQWRkLm1hcChmdW5jdGlvbihsKXsgcmV0dXJuIGwuc2x1ZyA/IGwuc2x1ZyA6IGw7IH0pLFxuXHRcdCdyZW1vdmUtbG9jYXRpb25zJzogdG9SZW1vdmUubWFwKGZ1bmN0aW9uKGwpeyByZXR1cm4gbC5zbHVnID8gbC5zbHVnIDogbDsgfSlcblx0fSk7XG59O1xuXG4vKipcbiAqIFVwZGF0ZSB0aGUgaG9zdCBsaXN0IGZvciB0aGUgdGltZXNsb3RcbiAqIEBwYXJhbSBpZCBOdW1iZXIgaWQgb2YgdGhlIHRpbWVzbG90IHRvIHVwZGF0ZVxuICogQHBhcmFtIHRvQWRkIEFycmF5W09iamVjdF0gbGlzdCBvZiB0aW1lc2xvdCBob3N0IG9iamVjdHMgdG8gYWRkIHRvIHRoZSB0aW1lc2xvdFxuICogQHBhcmFtIHRvUmVtb3ZlIEFycmF1W09iamVjdF0gbGlzdCBvZiB0aW1lc2xvdCBob3N0IG9iamVjdHMgdG8gcmVtb3ZlIGZyb20gdGhlXG4gKiAgIHRpbWVzbG90XG4gKiBAcmV0dXJuIGEgcHJvbWlzZSB0aGF0IHdpbGwgcmVzb2x2ZSB0byBhIHByb3BlcnR5IGxpc3Qgd2l0aCBhIGJvb2xlYW4gc3RhdHVzIGZpZWxkIFxuICovXG5Db25Ucm9sbFRpbWVzbG90cy5wcm90b3R5cGUudXBkYXRlSG9zdHMgPSBmdW5jdGlvbihpZCwgdG9BZGQsIHRvUmVtb3ZlKSB7XG5cdHJldHVybiB0aGlzLmFwaS51cGRhdGUodGhpcy5jb2xsZWN0aW9uLCBpZCwge1xuXHRcdCdob3N0cyc6IHRvQWRkLFxuXHRcdCdyZW1vdmUtaG9zdHMnOiB0b1JlbW92ZVxuXHR9KTtcbn07XG5cbm1vZHVsZS5leHBvcnRzID0gQ29uVHJvbGxUaW1lc2xvdHM7XG4iLCIvKipcbiAqIGh0dHA6Ly91c2Vqc2RvYy5vcmcvXG4gKi9cblxuJ3VzZSBzdHJpY3QnO1xuXG4vKipcbiAqIENvblRyb2xsIFVzZXIgUGFzcyBBUElcbiAqIEBwYXJhbSBDb25Ucm9sbCBhcGlcbiAqL1xudmFyIENvblRyb2xsVXNlclBhc3NlcyA9IGZ1bmN0aW9uKGFwaSkge1xuXHR0aGlzLmFwaSA9IGFwaTtcblx0dGhpcy5jb2xsZWN0aW9uID0ge2NvbnZlbnRpb246IHRydWUsIGNvbGxlY3Rpb246ICd1c2VycGFzc2VzJ307XG5cdHJldHVybiB0aGlzO1xufTtcblxuLyoqXG4gKiBSZXRyaWV2ZSB0aGUgbGlzdCBvZiBhbGwgdmFsaWQgdXNlciBwYXNzZXMgZm9yIGFsbCB1c2Vyc1xuICogQHJldHVybiBhIHByb21pc2UgdGhhdCB3aWxsIHJlc29sdmUgdG8gdGhlIGxpc3Qgb2YgdXNlciBwYXNzIG9iamVjdHNcbiAqL1xuQ29uVHJvbGxVc2VyUGFzc2VzLnByb3RvdHlwZS5jYXRhbG9nID0gZnVuY3Rpb24oKSB7XG5cdHJldHVybiB0aGlzLmFwaS5nZXQodGhpcy5jb2xsZWN0aW9uLCAnP2lzX3ZhbGlkPTEmYWxsPTEnKTtcbn07XG5cbi8qKlxuICogUmV0cmlldmUgYWxsIHBhc3NlcyBmb3IgYSB1c2VyXG4gKiBAcGFyYW0gdXNlcklkIElEIHRvIHJldHJpZXZlIGEgcmVwb3J0IGZvclxuICogQHBhcmFtIHZhbGlkIGJvb2xlYW4gc3BlY2lmeWluZyBpZiBvbmx5IHZhbGlkIHBhc3NlcyBhcmUgdG8gYmUgcmV0cmlldmVkXG4gKiBAcmV0dXJuIGEgcHJvbWlzZSB0aGF0IHdpbGwgcmVzb2x2ZSB0b3QgaGUgbGlzdCBvZiB1c2VyIHBhc3Mgb2JqZWN0c1xuICovXG5Db25Ucm9sbFVzZXJQYXNzZXMucHJvdG90eXBlLnVzZXJSZXBvcnQgPSBmdW5jdGlvbih1c2VySWQsIHZhbGlkKXtcblx0dmFyIHBhcmFtcyA9ICc/dXNlcj0nICsgdXNlcklkO1xuXHRpZiAodmFsaWQpXG5cdFx0cGFyYW1zICs9ICcmaXNfdmFsaWQ9MSc7XG5cdHJldHVybiB0aGlzLmFwaS5nZXQodGhpcy5jb2xsZWN0aW9uLCBwYXJhbXMpO1xufTtcblxuLyoqXG4gKiBSZXRyaWV2ZSBhbGwgcGFzc2VzIGZvciBhIHVzZXIsIGluY2x1ZGluZyBhdmFpbGFiaWxpdHkgcmVwb3J0IGZvciB0aGUgc3BlY2lmaWVkIHRpbWVzbG90XG4gKiBAcGFyYW0gdXNlcklkIElEIHRvIHJldHJpZXZlIGEgcmVwb3J0IGZvclxuICogQHBhcmFtIHRpbWVzbG90IElEIG9mIHRpbWVzbG90IHRvIGdldCBhdmFpbGFiaXRsaXR5IGZvclxuICogQHJldHVybiBhIHByb21pc2UgdGhhdCB3aWxsIHJlc29sdmUgdG8gdGhlIGxpc3Qgb2YgYWxsIHVzZXIgcGFzcyBvYmplY3RzXG4gKi9cbkNvblRyb2xsVXNlclBhc3Nlcy5wcm90b3R5cGUudXNlclRpbWVzbG90UmVwb3J0ID0gZnVuY3Rpb24odXNlcklkLCB0aW1lc2xvdCkge1xuXHRyZXR1cm4gdGhpcy5hcGkuZ2V0KHRoaXMuY29sbGVjdGlvbiwgJz91c2VyPScgKyB1c2VySWQgKyAnJmZvcl90aW1lc2xvdD0nICsgdGltZXNsb3QpO1xufTtcblxuLyoqXG4gKiBDYW5jZWwgb3IgcmVmdW5kIGFuIGV4aXN0aW5nIHBhc3NcbiAqIEBwYXJhbSBwYXNzSWQgSUQgb2YgdGhlIHVzZXIgcGFzcyB0byBjYW5jZWxcbiAqIEByZXR1cm4gYSBwcm9taXNlIHRoYXQgd2lsbCByZXNvbHZlIHRvIGEgcHJvcGVydHkgbGlzdCBjb250YWluaW5nIGEgYm9vbGVhbiBzdGF0dXMgZmllbGRcbiAqL1xuQ29uVHJvbGxVc2VyUGFzc2VzLnByb3RvdHlwZS5yZW1vdmUgPSBmdW5jdGlvbihwYXNzSWQpIHtcblx0cmV0dXJuIHRoaXMuYXBpLmRlbCh0aGlzLmNvbGxlY3Rpb24sIHBhc3NJZCk7XG59O1xuXG4vKipcbiAqIFJlc2VydmUgYSB1c2VyIHBhc3MgaW4gdGhlIHNob3BwaW5nIGNhcnRcbiAqIEBwYXJhbSB1c2VySWQgSUQgb2YgdGhlIHVzZXIgdG8gcmVzZXJ2ZSBhIHBhc3MgZm9yXG4gKiBAcGFyYW0gbmFtZSBwYXNzIGhvbGRlciBuYW1lIHRvIGxpc3Qgb24gdGhlIHBhc3NcbiAqIEBwYXJhbSBwYXNzSWQgSWQgb2YgcGFzcyB0eXBlIHRvIHJlc2VydmVcbiAqIEByZXR1cm4gYSBwcm9taXNlIHRoYXQgd2lsbCByZXNvbHZlIHRvIHRoZSBuZXdseSBjcmVhdGVkIHVzZXIgcGFzcyBvYmplY3RcbiAqL1xuQ29uVHJvbGxVc2VyUGFzc2VzLnByb3RvdHlwZS5jcmVhdGUgPSBmdW5jdGlvbih1c2VySWQsIG5hbWUsIHBhc3NJZCkge1xuXHR2YXIgZGF0YSA9IHtcblx0XHRcdHBhc3M6IHBhc3NJZCxcblx0XHRcdHVzZXI6IHVzZXJJZCxcblx0XHRcdG5hbWU6IG5hbWVcblx0fTtcblx0cmV0dXJuIHRoaXMuYXBpLmNyZWF0ZSh0aGlzLmNvbGxlY3Rpb24sIGRhdGEpO1xufTtcblxubW9kdWxlLmV4cG9ydHMgPSBDb25Ucm9sbFVzZXJQYXNzZXM7XG4iLCIvKipcbiAqIGh0dHA6Ly91c2Vqc2RvYy5vcmcvXG4gKi9cblxuJ3VzZSBzdHJpY3QnO1xuXG4vKipcbiAqIFVzZXIgcmVjb3JkcyBBUElcbiAqL1xudmFyIENvblRyb2xsUmVjb3JkcyA9IGZ1bmN0aW9uKGFwaSkge1xuXHR0aGlzLmFwaSA9IGFwaTtcblx0cmV0dXJuIHRoaXM7XG59O1xuXG4vKipcbiAqIFJldHJpZXZlIHRoZSB1c2VyIGRhdGEgcmVjb3JkIGZvciB0aGUgc3BlY2lmaWVkIGRlc2NyaXB0b3JcbiAqIEBwYXJhbSBkZXNjcmlwdG9yIFN0cmluZyB0ZXh0IGxhYmVsIGFzc29jaWF0ZWQgd2l0aCB0aGUgZGF0YVxuICogQHJldHVybiBwcm9taXNlIHRoYXQgd2lsbCByZXNvbGV2IHdpdGggdGhlIGRhdGEgb2JqZWN0IG9yaWdpbmFsbHlcbiAqICAgc3RvcmVkIG9uIHRoYXQgZGVzY3JpcHRvciwgb3IgbnVsbCBpZiBubyBzdWNoIGRhdGEgd2FzIHN0b3JlZFxuICovXG5Db25Ucm9sbFJlY29yZHMucHJvdG90eXBlLmdldCA9IGZ1bmN0aW9uKGRlc2NyaXB0b3IpIHtcblx0cmV0dXJuIHRoaXMuYXBpLmdldCh7Y29udmVudGlvbjogdHJ1ZSwgY29sbGVjdGlvbjogJ3JlY29yZHMnfSwgZGVzY3JpcHRvcilcblx0XHQudGhlbihyZXMgPT4gZGF0YSA/IEpTT04ucGFyc2UoZGF0YS5kYXRhKSA6IG51bGwpO1xufTtcblxuLyoqXG4gKiBTdG9yZSBhIHVzZXIgZGF0YSByZWNvcmQgZm9yIHRoZSBjb252ZW50aW9uIG9uIHRoZSBzcGVjaWZpZWQgZGVzY3JpcHRpb3JcbiAqIEBwYXJhbSBkZXNjcmlwdGlvciBTdHJpbmcgdGV4dCBsYWJlbCB0byBhc3NvY2lhdGUgd2l0aCB0aGUgZGF0YVxuICogQHBhcmFtIGRhdGEgQSBKYXZhc2NyaXB0IHZhbHVlIHRvIHN0b3JlXG4gKiBAcGFyYW0gYWNsIGEgc3RyaW5nIGRlc2NyaWJpbmcgYWNjZXNzIHBlcm1pc3Npb25zLiBWYWxpZCB2YWx1ZXMgYXJlXG4gKiAgICdwcml2YXRlJyAodGhlIGRlZmF1bHQpIGFuZCAncHVibGljJ1xuICogQHJldHVybiBhIHJwb21pc2UgdGhhdCB3aWxsIHJlc29sdmUgdG8gdHJ1ZSBpZiB0aGUgZGF0YSB3YXMgc3VjY2Vzc2Z1bGx5XG4gKiAgIHN0b3JlZFxuICovXG5Db25Ucm9sbFJlY29yZHMucHJvdG90eXBlLnNhdmUgPSBmdW5jdGlvbihkZXNjcmlwdG9yLCBkYXRhLCBhY2wpIHtcblx0aWYgKGFyZ3VtZW50cy5sZW5ndGggPT0gMykgeyAvLyBhY2wgaXMgb3B0aW9uYWxcblx0XHRjYWxsYmFjayA9IGFjbDtcblx0XHRhY2wgPSBudWxsO1xuXHR9XG5cdGlmICghYWNsKSBhY2wgPSAncHJpdmF0ZSc7XG5cdHJldHVybiB0aGlzLmFwaS5jcmVhdGUoe2NvbnZlbnRpb246IHRydWUsIGNvbGxlY3Rpb246ICdyZWNvcmRzJ30sIHtcblx0XHRkZXNjcmlwdG9yOiBkZXNjcmlwdG9yLFxuXHRcdGNvbnRlbnRfdHlwZTogJ2FwcGxpY2F0aW9uL2pzb24nLFxuXHRcdGFjbDogYWNsLFxuXHRcdGRhdGE6IEpTT04uc3RyaW5naWZ5KGRhdGEpXG5cdH0pLnRoZW4oKCkgPT4gdHJ1ZSk7XG59O1xuXG5tb2R1bGUuZXhwb3J0cyA9IENvblRyb2xsUmVjb3JkcztcbiIsIi8qKlxuICogaHR0cDovL3VzZWpzZG9jLm9yZy9cbiAqL1xuXG4ndXNlIHN0cmljdCc7XG5cbi8qKlxuICogQ29uVHJvbGwgQ29udmVudGlvbnMgQVBJXG4gKiBAcGFyYW0gQ29uVHJvbGwgYXBpXG4gKi9cbnZhciBDb25Ucm9sbENvbnZlbnRpb25zID0gZnVuY3Rpb24oYXBpKSB7XG5cdHRoaXMuYXBpID0gYXBpO1xuXHR0aGlzLmNvbGxlY3Rpb24gPSB7Y29udmVudGlvbjogdHJ1ZSwgY29sbGVjdGlvbjogJ2NvbnZlbnRpb25zJ307XG5cdHJldHVybiB0aGlzO1xufTtcblxuLyoqXG4gKiBMaXN0IGFsbCBjb252ZW50aW9uc1xuICogQHJldHVybiBhIHByb21pc2UgdGhhdCB3aWxsIHJlc29sdmUgdG8gdGhlIGxpc3Qgb2YgYWxsIGNvbnZlbnRpb24gb2JqZWN0cyBtYW5hZ2VkXG4gKiAgIGJ5IHRoZSBjdXJyZW50IHVzZXIsIGluY2x1ZGluZyB0aGVpciBrZXlzXG4gKi9cbkNvblRyb2xsQ29udmVudGlvbnMucHJvdG90eXBlLmNhdGFsb2cgPSBmdW5jdGlvbigpIHtcblx0cmV0dXJuIHRoaXMuYXBpLmdldCh0aGlzLmNvbGxlY3Rpb24sICc/a2V5cz0xJm1hbmFnZXI9MScpO1xufTtcblxuLyoqXG4gKiBDcmVhdGUgYSBuZXcgY29udmVudGlvblxuICogQHBhcmFtIHRpdGxlIFRpdGxlIG9mIHRoZSBjb252ZW50aW9uXG4gKiBAcGFyYW0gc2VyaWVzIE5hbWUgb2YgYSBjb252ZW50aW9uIHNlcmllcyBpZiB0aGlzIGluc3RhbmNlIGlzIGEgeWVhcmx5IGNvbnZlbnRpb25cbiAqIEBwYXJhbSBsb2NhdGlvbiBQaHlzaWNhbCBsb2NhdGlvbiAoc3VjaCBhcyBhbiBhZGRyZXNzKSB3aGVyZSB0aGUgY29udmVudGlvbiB3aWxsIGJlIGhlbGRcbiAqIEBwYXJhbSB3ZWJzaXRlIFVSTCBvZiB0aGUgY29udmVudGlvbiB3ZWJzaXRlXG4gKiBAcmV0dXJuIGEgcHJvbWlzZSB0aGF0IHdpbGwgcmVzb2x2ZSB0byB0aGUgbmV3bHkgY3JlYXRlZCBjb252ZW50aW9uIG9iamVjdFxuICovXG5Db25Ucm9sbENvbnZlbnRpb25zLnByb3RvdHlwZS5jcmVhdGUgPSBmdW5jdGlvbih0aXRsZSwgc2VyaWVzLCBsb2NhdGlvbiwgd2Vic2l0ZSkge1xuXHR2YXIgZGF0YSA9IHtcblx0XHRcdHRpdGxlOiB0aXRsZSxcblx0XHRcdHNlcmllczogc2VyaWVzLFxuXHRcdFx0bG9jYXRpb246IGxvY2F0aW9uLFxuXHRcdFx0d2Vic2l0ZTogd2Vic2l0ZVxuXHR9O1xuXHRyZXR1cm4gdGhpcy5hcGkuY3JlYXRlKHRoaXMuY29sbGVjdGlvbiwgZGF0YSk7XG59O1xuXG4vKipcbiAqIFJldHJpZXZlIHRoZSBjdXJyZW50IGNvbnZlbnRpb24ncyBkZXRhaWxzXG4gKiBAcmV0dXJuIGEgcHJvbWlzZSB0aGF0IHdpbGwgcmVzb2x2ZSB0byB0aGUgY3VycmVudGx5IHNlbGVjdGVkIGNvbnZuZXRpb24gb2JqZWN0XG4gKi9cbkNvblRyb2xsQ29udmVudGlvbnMucHJvdG90eXBlLmdldEN1cnJlbnQgPSBmdW5jdGlvbigpIHtcblx0cmV0dXJuIHRoaXMuYXBpLmdldCh0aGlzLmNvbGxlY3Rpb24sICdzZWxmJyk7XG59O1xuXG4vKipcbiAqIFVwZGF0ZSBjb252ZW50aW9uIHNldHRpbmdzXG4gKiBAcGFyYW0gaWQgY29udmVudGlvbiBJRCB0byB1cGRhdGVcbiAqIEBwYXJhbSBkYXRhIHByb3BlcnR5IGxpc3Qgd2l0aCBkYXRhIHRvIHVwZGF0ZVxuICogQHJldHVybiBhIHByb21pc2UgdGhhdCB3aWxsIHJlc29sdmUgdG8gdGhlIHVwZGF0ZWQgY29udmVudGlvbiBvYmplY3RcbiAqL1xuQ29uVHJvbGxDb252ZW50aW9ucy5wcm90b3R5cGUudXBkYXRlID0gZnVuY3Rpb24oaWQsIGRhdGEpIHtcblx0cmV0dXJuIHRoaXMuYXBpLnVwZGF0ZSh0aGlzLmNvbGxlY3Rpb24sIGlkLCBkYXRhKTtcbn07XG5cbm1vZHVsZS5leHBvcnRzID0gQ29uVHJvbGxDb252ZW50aW9ucztcbiIsIi8qKlxuICogaHR0cDovL3VzZWpzZG9jLm9yZy9cbiAqL1xuXG4ndXNlIHN0cmljdCc7XG5cbi8qKlxuICogQ29uVHJvbGwgVXNlcnMgQVBJXG4gKiBAcGFyYW0gQ29uVHJvbGwgYXBpXG4gKi9cbnZhciBDb25Ucm9sbFVzZXJzID0gZnVuY3Rpb24oYXBpKSB7XG5cdHRoaXMuYXBpID0gYXBpO1xuXHR0aGlzLmNvbGxlY3Rpb24gPSB7Y29udmVudGlvbjogdHJ1ZSwgY29sbGVjdGlvbjogJ3VzZXJzJ307XG5cdHJldHVybiB0aGlzO1xufTtcblxuLyoqXG4gKiBSZXRyaWV2ZSBhbGwgdXNlciBwcm9maWxlcyBpbiB0aGUgc3lzdGVtXG4gKiBAcmV0dXJuIGEgcHJvbWlzZSB0aGF0IHdpbGwgcmVzb2x2ZSB3aXRoIHRoZSBsaXN0IG9mIHVzZXIgcHJvZmlsZSBvYmplY3RzXG4gKi9cbkNvblRyb2xsVXNlcnMucHJvdG90eXBlLmNhdGFsb2cgPSBmdW5jdGlvbigpIHtcblx0cmV0dXJuIHRoaXMuYXBpLmdldCh0aGlzLmNvbGxlY3Rpb24pO1xufTtcblxuLyoqXG4gKiBSZXRyaWV2ZSBhIHVzZXIgcmVwb3J0IGZvciB0aGUgc3BlY2lmaWVkIHVzZXJcbiAqIEBwYXJhbSBpZCBVc2VyIElEIHRvIHJldHJpZXZlXG4gKiBAcmV0dXJuIGEgcHJvbWlzZSB0aGF0IHdpbGwgcmVzb2x2ZSB3aXRoIHRoZSByZXF1ZXN0ZWQgdXNlciBwcm9maWxlIG9iamVjdFxuICovXG5Db25Ucm9sbFVzZXJzLnByb3RvdHlwZS5nZXQgPSBmdW5jdGlvbihpZCkge1xuXHRyZXR1cm4gdGhpcy5hcGkuZ2V0KHRoaXMuY29sbGVjdGlvbiwgaWQpO1xufTtcblxuLyoqXG4gKiBDcmVhdGUgYSBuZXcgdXNlciBwcm9maWxlXG4gKiBAcGFyYW0gbmFtZSBTdHJpbmcgZnVsbCBuYW1lIG9mIHRoZSB1c2VyXG4gKiBAcGFyYW0gZW1haWwgU3RyaW5nIGVtYWlsIGFkZHJlc3Mgb2YgdGhlIHVzZXJcbiAqIEByZXR1cm4gYSBwcm9taXNlIHRoYXQgd2lsbCByZXNvbHZlIHdpdGggdGhlIG5ldyB1c2VyIHByb2ZpbGUgb2JqZWN0XG4gKi9cbkNvblRyb2xsVXNlcnMucHJvdG90eXBlLmNyZWF0ZSA9IGZ1bmN0aW9uKG5hbWUsIGVtYWlsKSB7XG5cdHJldHVybiB0aGlzLmFwaS5jcmVhdGUodGhpcy5jb2xsZWN0aW9uLCB7XG5cdFx0bmFtZTogbmFtZSxcblx0XHRlbWFpbDogZW1haWwsXG5cdH0pO1xufVxuXG5tb2R1bGUuZXhwb3J0cyA9IENvblRyb2xsVXNlcnM7XG4iLCIhZnVuY3Rpb24oZSxyKXtpZihcIm9iamVjdFwiPT10eXBlb2YgZXhwb3J0cyYmXCJvYmplY3RcIj09dHlwZW9mIG1vZHVsZSltb2R1bGUuZXhwb3J0cz1yKCk7ZWxzZSBpZihcImZ1bmN0aW9uXCI9PXR5cGVvZiBkZWZpbmUmJmRlZmluZS5hbWQpZGVmaW5lKFtdLHIpO2Vsc2V7dmFyIHQ9cigpO2Zvcih2YXIgbiBpbiB0KShcIm9iamVjdFwiPT10eXBlb2YgZXhwb3J0cz9leHBvcnRzOmUpW25dPXRbbl19fSh0aGlzLGZ1bmN0aW9uKCl7cmV0dXJuIGZ1bmN0aW9uKGUpe2Z1bmN0aW9uIHIobil7aWYodFtuXSlyZXR1cm4gdFtuXS5leHBvcnRzO3ZhciBvPXRbbl09e2V4cG9ydHM6e30saWQ6bixsb2FkZWQ6ITF9O3JldHVybiBlW25dLmNhbGwoby5leHBvcnRzLG8sby5leHBvcnRzLHIpLG8ubG9hZGVkPSEwLG8uZXhwb3J0c312YXIgdD17fTtyZXR1cm4gci5tPWUsci5jPXQsci5wPVwiXCIscigwKX0oW2Z1bmN0aW9uKGUscix0KXtlLmV4cG9ydHM9dCgxKX0sZnVuY3Rpb24oZSxyLHQpe1widXNlIHN0cmljdFwiO2Z1bmN0aW9uIG4oKXt2YXIgZT1cInVuZGVmaW5lZFwiPT10eXBlb2YgSlNPTj97fTpKU09OO28uc2V0dXBKU09OKGUpfXZhciBvPXQoMiksaT10KDMpO24oKTt2YXIgYT13aW5kb3cuX3JvbGxiYXJDb25maWcscz1hJiZhLmdsb2JhbEFsaWFzfHxcIlJvbGxiYXJcIix1PXdpbmRvd1tzXSYmXCJ1bmRlZmluZWRcIiE9dHlwZW9mIHdpbmRvd1tzXS5zaGltSWQ7IXUmJmE/by53cmFwcGVyLmluaXQoYSk6KHdpbmRvdy5Sb2xsYmFyPW8ud3JhcHBlcix3aW5kb3cuUm9sbGJhck5vdGlmaWVyPWkuTm90aWZpZXIpLGUuZXhwb3J0cz1vLndyYXBwZXJ9LGZ1bmN0aW9uKGUscix0KXtcInVzZSBzdHJpY3RcIjtmdW5jdGlvbiBuKGUscix0KXshdFs0XSYmd2luZG93Ll9yb2xsYmFyV3JhcHBlZEVycm9yJiYodFs0XT13aW5kb3cuX3JvbGxiYXJXcmFwcGVkRXJyb3Isd2luZG93Ll9yb2xsYmFyV3JhcHBlZEVycm9yPW51bGwpLGUudW5jYXVnaHRFcnJvci5hcHBseShlLHQpLHImJnIuYXBwbHkod2luZG93LHQpfWZ1bmN0aW9uIG8oZSxyKXtpZihyLmhhc093blByb3BlcnR5JiZyLmhhc093blByb3BlcnR5KFwiYWRkRXZlbnRMaXN0ZW5lclwiKSl7dmFyIHQ9ci5hZGRFdmVudExpc3RlbmVyO3IuYWRkRXZlbnRMaXN0ZW5lcj1mdW5jdGlvbihyLG4sbyl7dC5jYWxsKHRoaXMscixlLndyYXAobiksbyl9O3ZhciBuPXIucmVtb3ZlRXZlbnRMaXN0ZW5lcjtyLnJlbW92ZUV2ZW50TGlzdGVuZXI9ZnVuY3Rpb24oZSxyLHQpe24uY2FsbCh0aGlzLGUsciYmci5fd3JhcHBlZHx8cix0KX19fXZhciBpPXQoMyksYT10KDgpLHM9aS5Ob3RpZmllcjt3aW5kb3cuX3JvbGxiYXJXcmFwcGVkRXJyb3I9bnVsbDt2YXIgdT17fTt1LmluaXQ9ZnVuY3Rpb24oZSxyKXt2YXIgdD1uZXcgcyhyKTtpZih0LmNvbmZpZ3VyZShlKSxlLmNhcHR1cmVVbmNhdWdodCl7dmFyIGk7ciYmYS5pc1R5cGUoci5fcm9sbGJhck9sZE9uRXJyb3IsXCJmdW5jdGlvblwiKT9pPXIuX3JvbGxiYXJPbGRPbkVycm9yOndpbmRvdy5vbmVycm9yJiYhd2luZG93Lm9uZXJyb3IuYmVsb25nc1RvU2hpbSYmKGk9d2luZG93Lm9uZXJyb3IpLHdpbmRvdy5vbmVycm9yPWZ1bmN0aW9uKCl7dmFyIGU9QXJyYXkucHJvdG90eXBlLnNsaWNlLmNhbGwoYXJndW1lbnRzLDApO24odCxpLGUpfTt2YXIgdSxjLGw9W1wiRXZlbnRUYXJnZXRcIixcIldpbmRvd1wiLFwiTm9kZVwiLFwiQXBwbGljYXRpb25DYWNoZVwiLFwiQXVkaW9UcmFja0xpc3RcIixcIkNoYW5uZWxNZXJnZXJOb2RlXCIsXCJDcnlwdG9PcGVyYXRpb25cIixcIkV2ZW50U291cmNlXCIsXCJGaWxlUmVhZGVyXCIsXCJIVE1MVW5rbm93bkVsZW1lbnRcIixcIklEQkRhdGFiYXNlXCIsXCJJREJSZXF1ZXN0XCIsXCJJREJUcmFuc2FjdGlvblwiLFwiS2V5T3BlcmF0aW9uXCIsXCJNZWRpYUNvbnRyb2xsZXJcIixcIk1lc3NhZ2VQb3J0XCIsXCJNb2RhbFdpbmRvd1wiLFwiTm90aWZpY2F0aW9uXCIsXCJTVkdFbGVtZW50SW5zdGFuY2VcIixcIlNjcmVlblwiLFwiVGV4dFRyYWNrXCIsXCJUZXh0VHJhY2tDdWVcIixcIlRleHRUcmFja0xpc3RcIixcIldlYlNvY2tldFwiLFwiV2ViU29ja2V0V29ya2VyXCIsXCJXb3JrZXJcIixcIlhNTEh0dHBSZXF1ZXN0XCIsXCJYTUxIdHRwUmVxdWVzdEV2ZW50VGFyZ2V0XCIsXCJYTUxIdHRwUmVxdWVzdFVwbG9hZFwiXTtmb3IodT0wO3U8bC5sZW5ndGg7Kyt1KWM9bFt1XSx3aW5kb3dbY10mJndpbmRvd1tjXS5wcm90b3R5cGUmJm8odCx3aW5kb3dbY10ucHJvdG90eXBlKX1yZXR1cm4gZS5jYXB0dXJlVW5oYW5kbGVkUmVqZWN0aW9ucyYmKHImJmEuaXNUeXBlKHIuX3VuaGFuZGxlZFJlamVjdGlvbkhhbmRsZXIsXCJmdW5jdGlvblwiKSYmd2luZG93LnJlbW92ZUV2ZW50TGlzdGVuZXIoXCJ1bmhhbmRsZWRyZWplY3Rpb25cIixyLl91bmhhbmRsZWRSZWplY3Rpb25IYW5kbGVyKSx0Ll91bmhhbmRsZWRSZWplY3Rpb25IYW5kbGVyPWZ1bmN0aW9uKGUpe3ZhciByPWUucmVhc29uLG49ZS5wcm9taXNlLG89ZS5kZXRhaWw7IXImJm8mJihyPW8ucmVhc29uLG49by5wcm9taXNlKSx0LnVuaGFuZGxlZFJlamVjdGlvbihyLG4pfSx3aW5kb3cuYWRkRXZlbnRMaXN0ZW5lcihcInVuaGFuZGxlZHJlamVjdGlvblwiLHQuX3VuaGFuZGxlZFJlamVjdGlvbkhhbmRsZXIpKSx3aW5kb3cuUm9sbGJhcj10LHMucHJvY2Vzc1BheWxvYWRzKCksdH0sZS5leHBvcnRzPXt3cmFwcGVyOnUsc2V0dXBKU09OOmkuc2V0dXBKU09OfX0sZnVuY3Rpb24oZSxyLHQpe1widXNlIHN0cmljdFwiO2Z1bmN0aW9uIG4oZSl7RT1lLHcuc2V0dXBKU09OKGUpLHYuc2V0dXBKU09OKGUpfWZ1bmN0aW9uIG8oZSxyKXtyZXR1cm4gZnVuY3Rpb24oKXt2YXIgdD1yfHx0aGlzO3RyeXtyZXR1cm4gZS5hcHBseSh0LGFyZ3VtZW50cyl9Y2F0Y2goZSl7di5jb25zb2xlRXJyb3IoXCJbUm9sbGJhcl06XCIsZSl9fX1mdW5jdGlvbiBpKCl7aHx8KGg9c2V0VGltZW91dChmLDFlMykpfWZ1bmN0aW9uIGEoKXtyZXR1cm4gX31mdW5jdGlvbiBzKGUpe189X3x8dGhpczt2YXIgcj1cImh0dHBzOi8vXCIrcy5ERUZBVUxUX0VORFBPSU5UO3RoaXMub3B0aW9ucz17ZW5hYmxlZDohMCxlbmRwb2ludDpyLGVudmlyb25tZW50OlwicHJvZHVjdGlvblwiLHNjcnViRmllbGRzOmcoW10scy5ERUZBVUxUX1NDUlVCX0ZJRUxEUyksY2hlY2tJZ25vcmU6bnVsbCxsb2dMZXZlbDpzLkRFRkFVTFRfTE9HX0xFVkVMLHJlcG9ydExldmVsOnMuREVGQVVMVF9SRVBPUlRfTEVWRUwsdW5jYXVnaHRFcnJvckxldmVsOnMuREVGQVVMVF9VTkNBVUdIVF9FUlJPUl9MRVZFTCxwYXlsb2FkOnt9fSx0aGlzLmxhc3RFcnJvcj1udWxsLHRoaXMucGx1Z2lucz17fSx0aGlzLnBhcmVudE5vdGlmaWVyPWUsZSYmKGUuaGFzT3duUHJvcGVydHkoXCJzaGltSWRcIik/ZS5ub3RpZmllcj10aGlzOnRoaXMuY29uZmlndXJlKGUub3B0aW9ucykpfWZ1bmN0aW9uIHUoZSl7d2luZG93Ll9yb2xsYmFyUGF5bG9hZFF1ZXVlLnB1c2goZSksaSgpfWZ1bmN0aW9uIGMoZSl7cmV0dXJuIG8oZnVuY3Rpb24oKXt2YXIgcj10aGlzLl9nZXRMb2dBcmdzKGFyZ3VtZW50cyk7cmV0dXJuIHRoaXMuX2xvZyhlfHxyLmxldmVsfHx0aGlzLm9wdGlvbnMubG9nTGV2ZWx8fHMuREVGQVVMVF9MT0dfTEVWRUwsci5tZXNzYWdlLHIuZXJyLHIuY3VzdG9tLHIuY2FsbGJhY2spfSl9ZnVuY3Rpb24gbChlLHIpe2V8fChlPXI/RS5zdHJpbmdpZnkocik6XCJcIik7dmFyIHQ9e2JvZHk6ZX07cmV0dXJuIHImJih0LmV4dHJhPWcoITAse30scikpLHttZXNzYWdlOnR9fWZ1bmN0aW9uIHAoZSxyLHQpe3ZhciBuPW0uZ3Vlc3NFcnJvckNsYXNzKHIubWVzc2FnZSksbz1yLm5hbWV8fG5bMF0saT1uWzFdLGE9e2V4Y2VwdGlvbjp7Y2xhc3M6byxtZXNzYWdlOml9fTtpZihlJiYoYS5leGNlcHRpb24uZGVzY3JpcHRpb249ZXx8XCJ1bmNhdWdodCBleGNlcHRpb25cIiksci5zdGFjayl7dmFyIHMsdSxjLHAsZixkLGgsdztmb3IoYS5mcmFtZXM9W10saD0wO2g8ci5zdGFjay5sZW5ndGg7KytoKXM9ci5zdGFja1toXSx1PXtmaWxlbmFtZTpzLnVybD92LnNhbml0aXplVXJsKHMudXJsKTpcIih1bmtub3duKVwiLGxpbmVubzpzLmxpbmV8fG51bGwsbWV0aG9kOnMuZnVuYyYmXCI/XCIhPT1zLmZ1bmM/cy5mdW5jOlwiW2Fub255bW91c11cIixjb2xubzpzLmNvbHVtbn0sYz1wPWY9bnVsbCxkPXMuY29udGV4dD9zLmNvbnRleHQubGVuZ3RoOjAsZCYmKHc9TWF0aC5mbG9vcihkLzIpLHA9cy5jb250ZXh0LnNsaWNlKDAsdyksYz1zLmNvbnRleHRbd10sZj1zLmNvbnRleHQuc2xpY2UodykpLGMmJih1LmNvZGU9YyksKHB8fGYpJiYodS5jb250ZXh0PXt9LHAmJnAubGVuZ3RoJiYodS5jb250ZXh0LnByZT1wKSxmJiZmLmxlbmd0aCYmKHUuY29udGV4dC5wb3N0PWYpKSxzLmFyZ3MmJih1LmFyZ3M9cy5hcmdzKSxhLmZyYW1lcy5wdXNoKHUpO3JldHVybiBhLmZyYW1lcy5yZXZlcnNlKCksdCYmKGEuZXh0cmE9ZyghMCx7fSx0KSkse3RyYWNlOmF9fXJldHVybiBsKG8rXCI6IFwiK2ksdCl9ZnVuY3Rpb24gZigpe3ZhciBlO3RyeXtmb3IoO2U9d2luZG93Ll9yb2xsYmFyUGF5bG9hZFF1ZXVlLnNoaWZ0KCk7KWQoZSl9ZmluYWxseXtoPXZvaWQgMH19ZnVuY3Rpb24gZChlKXt2YXIgcj1lLmVuZHBvaW50VXJsLHQ9ZS5hY2Nlc3NUb2tlbixuPWUucGF5bG9hZCxvPWUuY2FsbGJhY2t8fGZ1bmN0aW9uKCl7fSxpPShuZXcgRGF0ZSkuZ2V0VGltZSgpO2ktTD49NmU0JiYoTD1pLFI9MCk7dmFyIGE9d2luZG93Ll9nbG9iYWxSb2xsYmFyT3B0aW9ucy5tYXhJdGVtcyxjPXdpbmRvdy5fZ2xvYmFsUm9sbGJhck9wdGlvbnMuaXRlbXNQZXJNaW51dGUsbD1mdW5jdGlvbigpe3JldHVybiFuLmlnbm9yZVJhdGVMaW1pdCYmYT49MSYmVD49YX0scD1mdW5jdGlvbigpe3JldHVybiFuLmlnbm9yZVJhdGVMaW1pdCYmYz49MSYmUj49Y307cmV0dXJuIGwoKT92b2lkIG8obmV3IEVycm9yKGErXCIgbWF4IGl0ZW1zIHJlYWNoZWRcIikpOnAoKT92b2lkIG8obmV3IEVycm9yKGMrXCIgaXRlbXMgcGVyIG1pbnV0ZSByZWFjaGVkXCIpKTooVCsrLFIrKyxsKCkmJl8uX2xvZyhfLm9wdGlvbnMudW5jYXVnaHRFcnJvckxldmVsLFwibWF4SXRlbXMgaGFzIGJlZW4gaGl0LiBJZ25vcmluZyBlcnJvcnMgZm9yIHRoZSByZW1haW5kZXIgb2YgdGhlIGN1cnJlbnQgcGFnZSBsb2FkLlwiLG51bGwse21heEl0ZW1zOmF9LG51bGwsITEsITApLG4uaWdub3JlUmF0ZUxpbWl0JiZkZWxldGUgbi5pZ25vcmVSYXRlTGltaXQsdm9pZCB5LnBvc3Qocix0LG4sZnVuY3Rpb24ocix0KXtyZXR1cm4gcj8ociBpbnN0YW5jZW9mIGImJihlLmNhbGxiYWNrPWZ1bmN0aW9uKCl7fSxzZXRUaW1lb3V0KGZ1bmN0aW9uKCl7dShlKX0scy5SRVRSWV9ERUxBWSkpLG8ocikpOm8obnVsbCx0KX0pKX12YXIgaCxnPXQoNCksbT10KDUpLHY9dCg4KSx3PXQoMTEpLHk9dy5YSFIsYj13LkNvbm5lY3Rpb25FcnJvcixFPW51bGw7cy5OT1RJRklFUl9WRVJTSU9OPVwiMS45LjRcIixzLkRFRkFVTFRfRU5EUE9JTlQ9XCJhcGkucm9sbGJhci5jb20vYXBpLzEvXCIscy5ERUZBVUxUX1NDUlVCX0ZJRUxEUz1bXCJwd1wiLFwicGFzc1wiLFwicGFzc3dkXCIsXCJwYXNzd29yZFwiLFwic2VjcmV0XCIsXCJjb25maXJtX3Bhc3N3b3JkXCIsXCJjb25maXJtUGFzc3dvcmRcIixcInBhc3N3b3JkX2NvbmZpcm1hdGlvblwiLFwicGFzc3dvcmRDb25maXJtYXRpb25cIixcImFjY2Vzc190b2tlblwiLFwiYWNjZXNzVG9rZW5cIixcInNlY3JldF9rZXlcIixcInNlY3JldEtleVwiLFwic2VjcmV0VG9rZW5cIl0scy5ERUZBVUxUX0xPR19MRVZFTD1cImRlYnVnXCIscy5ERUZBVUxUX1JFUE9SVF9MRVZFTD1cImRlYnVnXCIscy5ERUZBVUxUX1VOQ0FVR0hUX0VSUk9SX0xFVkVMPVwiZXJyb3JcIixzLkRFRkFVTFRfSVRFTVNfUEVSX01JTj02MCxzLkRFRkFVTFRfTUFYX0lURU1TPTAscy5MRVZFTFM9e2RlYnVnOjAsaW5mbzoxLHdhcm5pbmc6MixlcnJvcjozLGNyaXRpY2FsOjR9LHMuUkVUUllfREVMQVk9MWU0LHdpbmRvdy5fcm9sbGJhclBheWxvYWRRdWV1ZT13aW5kb3cuX3JvbGxiYXJQYXlsb2FkUXVldWV8fFtdLHdpbmRvdy5fZ2xvYmFsUm9sbGJhck9wdGlvbnM9e3N0YXJ0VGltZToobmV3IERhdGUpLmdldFRpbWUoKSxtYXhJdGVtczpzLkRFRkFVTFRfTUFYX0lURU1TLGl0ZW1zUGVyTWludXRlOnMuREVGQVVMVF9JVEVNU19QRVJfTUlOfTt2YXIgXyx4PXMucHJvdG90eXBlO3guX2dldExvZ0FyZ3M9ZnVuY3Rpb24oZSl7Zm9yKHZhciByLHQsbixpLGEsdSxjPXRoaXMub3B0aW9ucy5sb2dMZXZlbHx8cy5ERUZBVUxUX0xPR19MRVZFTCxsPVtdLHA9MDtwPGUubGVuZ3RoOysrcCl1PWVbcF0sYT12LnR5cGVOYW1lKHUpLFwic3RyaW5nXCI9PT1hP3I/bC5wdXNoKHUpOnI9dTpcImZ1bmN0aW9uXCI9PT1hP2k9byh1LHRoaXMpOlwiZGF0ZVwiPT09YT9sLnB1c2godSk6XCJlcnJvclwiPT09YXx8dSBpbnN0YW5jZW9mIEVycm9yfHxcInVuZGVmaW5lZFwiIT10eXBlb2YgRE9NRXhjZXB0aW9uJiZ1IGluc3RhbmNlb2YgRE9NRXhjZXB0aW9uP3Q/bC5wdXNoKHUpOnQ9dTpcIm9iamVjdFwiIT09YSYmXCJhcnJheVwiIT09YXx8KG4/bC5wdXNoKHUpOm49dSk7cmV0dXJuIGwubGVuZ3RoJiYobj1ufHx7fSxuLmV4dHJhQXJncz1sKSx7bGV2ZWw6YyxtZXNzYWdlOnIsZXJyOnQsY3VzdG9tOm4sY2FsbGJhY2s6aX19LHguX3JvdXRlPWZ1bmN0aW9uKGUpe3ZhciByPXRoaXMub3B0aW9ucy5lbmRwb2ludCx0PS9cXC8kLy50ZXN0KHIpLG49L15cXC8vLnRlc3QoZSk7cmV0dXJuIHQmJm4/ZT1lLnN1YnN0cmluZygxKTp0fHxufHwoZT1cIi9cIitlKSxyK2V9LHguX3Byb2Nlc3NTaGltUXVldWU9ZnVuY3Rpb24oZSl7Zm9yKHZhciByLHQsbixvLGksYSx1LGM9e307dD1lLnNoaWZ0KCk7KXI9dC5zaGltLG49dC5tZXRob2Qsbz10LmFyZ3MsaT1yLnBhcmVudFNoaW0sdT1jW3Iuc2hpbUlkXSx1fHwoaT8oYT1jW2kuc2hpbUlkXSx1PW5ldyBzKGEpKTp1PXRoaXMsY1tyLnNoaW1JZF09dSksdVtuXSYmdi5pc1R5cGUodVtuXSxcImZ1bmN0aW9uXCIpJiZ1W25dLmFwcGx5KHUsbyl9LHguX2J1aWxkUGF5bG9hZD1mdW5jdGlvbihlLHIsdCxuLG8pe3ZhciBpPXRoaXMub3B0aW9ucy5hY2Nlc3NUb2tlbixhPXRoaXMub3B0aW9ucy5lbnZpcm9ubWVudCx1PWcoITAse30sdGhpcy5vcHRpb25zLnBheWxvYWQpLGM9di51dWlkNCgpO2lmKHZvaWQgMD09PXMuTEVWRUxTW3JdKXRocm93IG5ldyBFcnJvcihcIkludmFsaWQgbGV2ZWxcIik7aWYoIXQmJiFuJiYhbyl0aHJvdyBuZXcgRXJyb3IoXCJObyBtZXNzYWdlLCBzdGFjayBpbmZvIG9yIGN1c3RvbSBkYXRhXCIpO3ZhciBsPXtlbnZpcm9ubWVudDphLGVuZHBvaW50OnRoaXMub3B0aW9ucy5lbmRwb2ludCx1dWlkOmMsbGV2ZWw6cixwbGF0Zm9ybTpcImJyb3dzZXJcIixmcmFtZXdvcms6XCJicm93c2VyLWpzXCIsbGFuZ3VhZ2U6XCJqYXZhc2NyaXB0XCIsYm9keTp0aGlzLl9idWlsZEJvZHkodCxuLG8pLHJlcXVlc3Q6e3VybDp3aW5kb3cubG9jYXRpb24uaHJlZixxdWVyeV9zdHJpbmc6d2luZG93LmxvY2F0aW9uLnNlYXJjaCx1c2VyX2lwOlwiJHJlbW90ZV9pcFwifSxjbGllbnQ6e3J1bnRpbWVfbXM6ZS5nZXRUaW1lKCktd2luZG93Ll9nbG9iYWxSb2xsYmFyT3B0aW9ucy5zdGFydFRpbWUsdGltZXN0YW1wOk1hdGgucm91bmQoZS5nZXRUaW1lKCkvMWUzKSxqYXZhc2NyaXB0Onticm93c2VyOndpbmRvdy5uYXZpZ2F0b3IudXNlckFnZW50LGxhbmd1YWdlOndpbmRvdy5uYXZpZ2F0b3IubGFuZ3VhZ2UsY29va2llX2VuYWJsZWQ6d2luZG93Lm5hdmlnYXRvci5jb29raWVFbmFibGVkLHNjcmVlbjp7d2lkdGg6d2luZG93LnNjcmVlbi53aWR0aCxoZWlnaHQ6d2luZG93LnNjcmVlbi5oZWlnaHR9LHBsdWdpbnM6dGhpcy5fZ2V0QnJvd3NlclBsdWdpbnMoKX19LHNlcnZlcjp7fSxub3RpZmllcjp7bmFtZTpcInJvbGxiYXItYnJvd3Nlci1qc1wiLHZlcnNpb246cy5OT1RJRklFUl9WRVJTSU9OfX07dS5ib2R5JiZkZWxldGUgdS5ib2R5O3ZhciBwPXthY2Nlc3NfdG9rZW46aSxkYXRhOmcoITAsbCx1KX07cmV0dXJuIHRoaXMuX3NjcnViKHAuZGF0YSkscH0seC5fYnVpbGRCb2R5PWZ1bmN0aW9uKGUscix0KXt2YXIgbjtyZXR1cm4gbj1yP3AoZSxyLHQpOmwoZSx0KX0seC5fZ2V0QnJvd3NlclBsdWdpbnM9ZnVuY3Rpb24oKXtpZighdGhpcy5fYnJvd3NlclBsdWdpbnMpe3ZhciBlLHIsdD13aW5kb3cubmF2aWdhdG9yLnBsdWdpbnN8fFtdLG49dC5sZW5ndGgsbz1bXTtmb3Iocj0wO3I8bjsrK3IpZT10W3JdLG8ucHVzaCh7bmFtZTplLm5hbWUsZGVzY3JpcHRpb246ZS5kZXNjcmlwdGlvbn0pO3RoaXMuX2Jyb3dzZXJQbHVnaW5zPW99cmV0dXJuIHRoaXMuX2Jyb3dzZXJQbHVnaW5zfSx4Ll9zY3J1Yj1mdW5jdGlvbihlKXtmdW5jdGlvbiByKGUscix0LG4sbyxpKXtyZXR1cm4gcit2LnJlZGFjdChpKX1mdW5jdGlvbiB0KGUpe3ZhciB0O2lmKHYuaXNUeXBlKGUsXCJzdHJpbmdcIikpZm9yKHQ9MDt0PHMubGVuZ3RoOysrdCllPWUucmVwbGFjZShzW3RdLHIpO3JldHVybiBlfWZ1bmN0aW9uIG4oZSxyKXt2YXIgdDtmb3IodD0wO3Q8YS5sZW5ndGg7Kyt0KWlmKGFbdF0udGVzdChlKSl7cj12LnJlZGFjdChyKTticmVha31yZXR1cm4gcn1mdW5jdGlvbiBvKGUscil7dmFyIG89bihlLHIpO3JldHVybiBvPT09cj90KG8pOm99dmFyIGk9dGhpcy5vcHRpb25zLnNjcnViRmllbGRzLGE9dGhpcy5fZ2V0U2NydWJGaWVsZFJlZ2V4cyhpKSxzPXRoaXMuX2dldFNjcnViUXVlcnlQYXJhbVJlZ2V4cyhpKTtyZXR1cm4gdi50cmF2ZXJzZShlLG8pLGV9LHguX2dldFNjcnViRmllbGRSZWdleHM9ZnVuY3Rpb24oZSl7Zm9yKHZhciByLHQ9W10sbj0wO248ZS5sZW5ndGg7KytuKXI9XCJcXFxcWz8oJTVbYkJdKT9cIitlW25dK1wiXFxcXFs/KCU1W2JCXSk/XFxcXF0/KCU1W2REXSk/XCIsdC5wdXNoKG5ldyBSZWdFeHAocixcImlcIikpO3JldHVybiB0fSx4Ll9nZXRTY3J1YlF1ZXJ5UGFyYW1SZWdleHM9ZnVuY3Rpb24oZSl7Zm9yKHZhciByLHQ9W10sbj0wO248ZS5sZW5ndGg7KytuKXI9XCJcXFxcWz8oJTVbYkJdKT9cIitlW25dK1wiXFxcXFs/KCU1W2JCXSk/XFxcXF0/KCU1W2REXSk/XCIsdC5wdXNoKG5ldyBSZWdFeHAoXCIoXCIrcitcIj0pKFteJlxcXFxuXSspXCIsXCJpZ21cIikpO3JldHVybiB0fSx4Ll91cmxJc1doaXRlbGlzdGVkPWZ1bmN0aW9uKGUpe3ZhciByLHQsbixvLGksYSxzLHUsYyxsO3RyeXtpZihyPXRoaXMub3B0aW9ucy5ob3N0V2hpdGVMaXN0LHQ9ZSYmZS5kYXRhJiZlLmRhdGEuYm9keSYmZS5kYXRhLmJvZHkudHJhY2UsIXJ8fDA9PT1yLmxlbmd0aClyZXR1cm4hMDtpZighdClyZXR1cm4hMDtmb3Iocz1yLmxlbmd0aCxpPXQuZnJhbWVzLmxlbmd0aCxjPTA7YzxpO2MrKyl7aWYobj10LmZyYW1lc1tjXSxvPW4uZmlsZW5hbWUsIXYuaXNUeXBlKG8sXCJzdHJpbmdcIikpcmV0dXJuITA7Zm9yKGw9MDtsPHM7bCsrKWlmKGE9cltsXSx1PW5ldyBSZWdFeHAoYSksdS50ZXN0KG8pKXJldHVybiEwfX1jYXRjaChlKXtyZXR1cm4gdGhpcy5jb25maWd1cmUoe2hvc3RXaGl0ZUxpc3Q6bnVsbH0pLHYuY29uc29sZUVycm9yKFwiW1JvbGxiYXJdOiBFcnJvciB3aGlsZSByZWFkaW5nIHlvdXIgY29uZmlndXJhdGlvbidzIGhvc3RXaGl0ZUxpc3Qgb3B0aW9uLiBSZW1vdmluZyBjdXN0b20gaG9zdFdoaXRlTGlzdC5cIixlKSwhMH1yZXR1cm4hMX0seC5fbWVzc2FnZUlzSWdub3JlZD1mdW5jdGlvbihlKXt2YXIgcix0LG4sbyxpLGEscyx1LGM7dHJ5e2lmKGk9ITEsbj10aGlzLm9wdGlvbnMuaWdub3JlZE1lc3NhZ2VzLCFufHwwPT09bi5sZW5ndGgpcmV0dXJuITE7aWYocz1lJiZlLmRhdGEmJmUuZGF0YS5ib2R5LHU9cyYmcy50cmFjZSYmcy50cmFjZS5leGNlcHRpb24mJnMudHJhY2UuZXhjZXB0aW9uLm1lc3NhZ2UsYz1zJiZzLm1lc3NhZ2UmJnMubWVzc2FnZS5ib2R5LHI9dXx8YywhcilyZXR1cm4hMTtmb3Iobz1uLmxlbmd0aCx0PTA7dDxvJiYoYT1uZXcgUmVnRXhwKG5bdF0sXCJnaVwiKSwhKGk9YS50ZXN0KHIpKSk7dCsrKTt9Y2F0Y2goZSl7dGhpcy5jb25maWd1cmUoe2lnbm9yZWRNZXNzYWdlczpudWxsfSksdi5jb25zb2xlRXJyb3IoXCJbUm9sbGJhcl06IEVycm9yIHdoaWxlIHJlYWRpbmcgeW91ciBjb25maWd1cmF0aW9uJ3MgaWdub3JlZE1lc3NhZ2VzIG9wdGlvbi4gUmVtb3ZpbmcgY3VzdG9tIGlnbm9yZWRNZXNzYWdlcy5cIil9cmV0dXJuIGl9LHguX2VucXVldWVQYXlsb2FkPWZ1bmN0aW9uKGUscix0LG4pe3ZhciBvPXtjYWxsYmFjazpuLGFjY2Vzc1Rva2VuOnRoaXMub3B0aW9ucy5hY2Nlc3NUb2tlbixlbmRwb2ludFVybDp0aGlzLl9yb3V0ZShcIml0ZW0vXCIpLHBheWxvYWQ6ZX0saT1mdW5jdGlvbigpe2lmKG4pe3ZhciBlPVwiVGhpcyBpdGVtIHdhcyBub3Qgc2VudCB0byBSb2xsYmFyIGJlY2F1c2UgaXQgd2FzIGlnbm9yZWQuIFRoaXMgY2FuIGhhcHBlbiBpZiBhIGN1c3RvbSBjaGVja0lnbm9yZSgpIGZ1bmN0aW9uIHdhcyB1c2VkIG9yIGlmIHRoZSBpdGVtJ3MgbGV2ZWwgd2FzIGxlc3MgdGhhbiB0aGUgbm90aWZpZXInIHJlcG9ydExldmVsLiBTZWUgaHR0cHM6Ly9yb2xsYmFyLmNvbS9kb2NzL25vdGlmaWVyL3JvbGxiYXIuanMvY29uZmlndXJhdGlvbiBmb3IgbW9yZSBkZXRhaWxzLlwiO24obnVsbCx7ZXJyOjAscmVzdWx0OntpZDpudWxsLHV1aWQ6bnVsbCxtZXNzYWdlOmV9fSl9fTtpZih0aGlzLl9pbnRlcm5hbENoZWNrSWdub3JlKHIsdCxlKSlyZXR1cm4gdm9pZCBpKCk7dHJ5e2lmKHYuaXNUeXBlKHRoaXMub3B0aW9ucy5jaGVja0lnbm9yZSxcImZ1bmN0aW9uXCIpJiZ0aGlzLm9wdGlvbnMuY2hlY2tJZ25vcmUocix0LGUpKXJldHVybiB2b2lkIGkoKX1jYXRjaChlKXt0aGlzLmNvbmZpZ3VyZSh7Y2hlY2tJZ25vcmU6bnVsbH0pLHYuY29uc29sZUVycm9yKFwiW1JvbGxiYXJdOiBFcnJvciB3aGlsZSBjYWxsaW5nIGN1c3RvbSBjaGVja0lnbm9yZSgpIGZ1bmN0aW9uLiBSZW1vdmluZyBjdXN0b20gY2hlY2tJZ25vcmUoKS5cIixlKX1pZih0aGlzLl91cmxJc1doaXRlbGlzdGVkKGUpJiYhdGhpcy5fbWVzc2FnZUlzSWdub3JlZChlKSl7aWYodGhpcy5vcHRpb25zLnZlcmJvc2Upe2lmKGUuZGF0YSYmZS5kYXRhLmJvZHkmJmUuZGF0YS5ib2R5LnRyYWNlKXt2YXIgYT1lLmRhdGEuYm9keS50cmFjZSxzPWEuZXhjZXB0aW9uLm1lc3NhZ2U7di5jb25zb2xlRXJyb3IoXCJbUm9sbGJhcl06IFwiLHMpfXYuY29uc29sZUluZm8oXCJbUm9sbGJhcl06IFwiLG8pfXYuaXNUeXBlKHRoaXMub3B0aW9ucy5sb2dGdW5jdGlvbixcImZ1bmN0aW9uXCIpJiZ0aGlzLm9wdGlvbnMubG9nRnVuY3Rpb24obyk7dHJ5e3YuaXNUeXBlKHRoaXMub3B0aW9ucy50cmFuc2Zvcm0sXCJmdW5jdGlvblwiKSYmdGhpcy5vcHRpb25zLnRyYW5zZm9ybShlKX1jYXRjaChlKXt0aGlzLmNvbmZpZ3VyZSh7dHJhbnNmb3JtOm51bGx9KSx2LmNvbnNvbGVFcnJvcihcIltSb2xsYmFyXTogRXJyb3Igd2hpbGUgY2FsbGluZyBjdXN0b20gdHJhbnNmb3JtKCkgZnVuY3Rpb24uIFJlbW92aW5nIGN1c3RvbSB0cmFuc2Zvcm0oKS5cIixlKX10aGlzLm9wdGlvbnMuZW5hYmxlZCYmdShvKX19LHguX2ludGVybmFsQ2hlY2tJZ25vcmU9ZnVuY3Rpb24oZSxyLHQpe3ZhciBuPXJbMF0sbz1zLkxFVkVMU1tuXXx8MCxpPXMuTEVWRUxTW3RoaXMub3B0aW9ucy5yZXBvcnRMZXZlbF18fDA7aWYobzxpKXJldHVybiEwO3ZhciBhPXRoaXMub3B0aW9ucz90aGlzLm9wdGlvbnMucGx1Z2luczp7fTtpZihhJiZhLmpxdWVyeSYmYS5qcXVlcnkuaWdub3JlQWpheEVycm9ycyl0cnl7cmV0dXJuISF0LmRhdGEuYm9keS5tZXNzYWdlLmV4dHJhLmlzQWpheH1jYXRjaChlKXtyZXR1cm4hMX1yZXR1cm4hMX0seC5fbG9nPWZ1bmN0aW9uKGUscix0LG4sbyxpLGEpe3ZhciBzPW51bGw7aWYodCl0cnl7aWYocz10Ll9zYXZlZFN0YWNrVHJhY2U/dC5fc2F2ZWRTdGFja1RyYWNlOm0ucGFyc2UodCksdD09PXRoaXMubGFzdEVycm9yKXJldHVybjt0aGlzLmxhc3RFcnJvcj10fWNhdGNoKGUpe3YuY29uc29sZUVycm9yKFwiW1JvbGxiYXJdOiBFcnJvciB3aGlsZSBwYXJzaW5nIHRoZSBlcnJvciBvYmplY3QuXCIsZSkscj10Lm1lc3NhZ2V8fHQuZGVzY3JpcHRpb258fHJ8fFN0cmluZyh0KSx0PW51bGx9dmFyIHU9dGhpcy5fYnVpbGRQYXlsb2FkKG5ldyBEYXRlLGUscixzLG4pO3JldHVybiBhJiYodS5pZ25vcmVSYXRlTGltaXQ9ITApLHRoaXMuX2VucXVldWVQYXlsb2FkKHUsISFpLFtlLHIsdCxuXSxvKSx7dXVpZDp1LmRhdGEudXVpZH19LHgubG9nPWMoKSx4LmRlYnVnPWMoXCJkZWJ1Z1wiKSx4LmluZm89YyhcImluZm9cIikseC53YXJuPWMoXCJ3YXJuaW5nXCIpLHgud2FybmluZz1jKFwid2FybmluZ1wiKSx4LmVycm9yPWMoXCJlcnJvclwiKSx4LmNyaXRpY2FsPWMoXCJjcml0aWNhbFwiKSx4LnVuY2F1Z2h0RXJyb3I9byhmdW5jdGlvbihlLHIsdCxuLG8saSl7aWYoaT1pfHxudWxsLG8mJnYuaXNUeXBlKG8sXCJlcnJvclwiKSlyZXR1cm4gdm9pZCB0aGlzLl9sb2codGhpcy5vcHRpb25zLnVuY2F1Z2h0RXJyb3JMZXZlbCxlLG8saSxudWxsLCEwKTtpZihyJiZ2LmlzVHlwZShyLFwiZXJyb3JcIikpcmV0dXJuIHZvaWQgdGhpcy5fbG9nKHRoaXMub3B0aW9ucy51bmNhdWdodEVycm9yTGV2ZWwsZSxyLGksbnVsbCwhMCk7dmFyIGE9e3VybDpyfHxcIlwiLGxpbmU6dH07YS5mdW5jPW0uZ3Vlc3NGdW5jdGlvbk5hbWUoYS51cmwsYS5saW5lKSxhLmNvbnRleHQ9bS5nYXRoZXJDb250ZXh0KGEudXJsLGEubGluZSk7dmFyIHM9e21vZGU6XCJvbmVycm9yXCIsbWVzc2FnZTpvP1N0cmluZyhvKTplfHxcInVuY2F1Z2h0IGV4Y2VwdGlvblwiLHVybDpkb2N1bWVudC5sb2NhdGlvbi5ocmVmLHN0YWNrOlthXSx1c2VyYWdlbnQ6bmF2aWdhdG9yLnVzZXJBZ2VudH0sdT10aGlzLl9idWlsZFBheWxvYWQobmV3IERhdGUsdGhpcy5vcHRpb25zLnVuY2F1Z2h0RXJyb3JMZXZlbCxlLHMsaSk7dGhpcy5fZW5xdWV1ZVBheWxvYWQodSwhMCxbdGhpcy5vcHRpb25zLnVuY2F1Z2h0RXJyb3JMZXZlbCxlLHIsdCxuLG9dKX0pLHgudW5oYW5kbGVkUmVqZWN0aW9uPW8oZnVuY3Rpb24oZSxyKXt2YXIgdCxuO2lmKGU/KHQ9ZS5tZXNzYWdlfHxTdHJpbmcoZSksbj1lLl9yb2xsYmFyQ29udGV4dCk6dD1cInVuaGFuZGxlZCByZWplY3Rpb24gd2FzIG51bGwgb3IgdW5kZWZpbmVkIVwiLG49bnx8ci5fcm9sbGJhckNvbnRleHR8fG51bGwsZSYmdi5pc1R5cGUoZSxcImVycm9yXCIpKXJldHVybiB2b2lkIHRoaXMuX2xvZyh0aGlzLm9wdGlvbnMudW5jYXVnaHRFcnJvckxldmVsLHQsZSxuLG51bGwsITApO3ZhciBvPXt1cmw6XCJcIixsaW5lOjB9O28uZnVuYz1tLmd1ZXNzRnVuY3Rpb25OYW1lKG8udXJsLG8ubGluZSksby5jb250ZXh0PW0uZ2F0aGVyQ29udGV4dChvLnVybCxvLmxpbmUpO3ZhciBpPXttb2RlOlwidW5oYW5kbGVkcmVqZWN0aW9uXCIsbWVzc2FnZTp0LHVybDpkb2N1bWVudC5sb2NhdGlvbi5ocmVmLHN0YWNrOltvXSx1c2VyYWdlbnQ6bmF2aWdhdG9yLnVzZXJBZ2VudH0sYT10aGlzLl9idWlsZFBheWxvYWQobmV3IERhdGUsdGhpcy5vcHRpb25zLnVuY2F1Z2h0RXJyb3JMZXZlbCx0LGksbik7dGhpcy5fZW5xdWV1ZVBheWxvYWQoYSwhMCxbdGhpcy5vcHRpb25zLnVuY2F1Z2h0RXJyb3JMZXZlbCx0LG8udXJsLG8ubGluZSwwLGUscl0pfSkseC5nbG9iYWw9byhmdW5jdGlvbihlKXtlPWV8fHt9O3ZhciByPXtzdGFydFRpbWU6ZS5zdGFydFRpbWUsbWF4SXRlbXM6ZS5tYXhJdGVtcyxpdGVtc1Blck1pbnV0ZTplLml0ZW1zUGVyTWludXRlfTtnKCEwLHdpbmRvdy5fZ2xvYmFsUm9sbGJhck9wdGlvbnMsciksdm9pZCAwIT09ZS5tYXhJdGVtcyYmKFQ9MCksdm9pZCAwIT09ZS5pdGVtc1Blck1pbnV0ZSYmKFI9MCl9KSx4LmNvbmZpZ3VyZT1vKGZ1bmN0aW9uKGUscil7dmFyIHQ9ZyghMCx7fSxlKTtnKCFyLHRoaXMub3B0aW9ucyx0KSx0aGlzLmdsb2JhbCh0KX0pLHguc2NvcGU9byhmdW5jdGlvbihlKXt2YXIgcj1uZXcgcyh0aGlzKTtyZXR1cm4gZyghMCxyLm9wdGlvbnMucGF5bG9hZCxlKSxyfSkseC53cmFwPWZ1bmN0aW9uKGUscil7dHJ5e3ZhciB0O2lmKHQ9di5pc1R5cGUocixcImZ1bmN0aW9uXCIpP3I6ZnVuY3Rpb24oKXtyZXR1cm4gcnx8e319LCF2LmlzVHlwZShlLFwiZnVuY3Rpb25cIikpcmV0dXJuIGU7aWYoZS5faXNXcmFwKXJldHVybiBlO2lmKCFlLl93cmFwcGVkKXtlLl93cmFwcGVkPWZ1bmN0aW9uKCl7dHJ5e3JldHVybiBlLmFwcGx5KHRoaXMsYXJndW1lbnRzKX1jYXRjaChyKXt0aHJvd1wic3RyaW5nXCI9PXR5cGVvZiByJiYocj1uZXcgU3RyaW5nKHIpKSxyLnN0YWNrfHwoci5fc2F2ZWRTdGFja1RyYWNlPW0ucGFyc2UocikpLHIuX3JvbGxiYXJDb250ZXh0PXQoKXx8e30sci5fcm9sbGJhckNvbnRleHQuX3dyYXBwZWRTb3VyY2U9ZS50b1N0cmluZygpLHdpbmRvdy5fcm9sbGJhcldyYXBwZWRFcnJvcj1yLHJ9fSxlLl93cmFwcGVkLl9pc1dyYXA9ITA7Zm9yKHZhciBuIGluIGUpZS5oYXNPd25Qcm9wZXJ0eShuKSYmKGUuX3dyYXBwZWRbbl09ZVtuXSl9cmV0dXJuIGUuX3dyYXBwZWR9Y2F0Y2gocil7cmV0dXJuIGV9fSx4LmxvYWRGdWxsPWZ1bmN0aW9uKCl7di5jb25zb2xlRXJyb3IoXCJbUm9sbGJhcl06IFVuZXhwZWN0ZWQgUm9sbGJhci5sb2FkRnVsbCgpIGNhbGxlZCBvbiBhIE5vdGlmaWVyIGluc3RhbmNlXCIpfSxzLnByb2Nlc3NQYXlsb2Fkcz1mdW5jdGlvbihlKXtyZXR1cm4gZT92b2lkIGYoKTp2b2lkIGkoKX07dmFyIEw9KG5ldyBEYXRlKS5nZXRUaW1lKCksVD0wLFI9MDtlLmV4cG9ydHM9e05vdGlmaWVyOnMsc2V0dXBKU09OOm4sdG9wTGV2ZWxOb3RpZmllcjphfX0sZnVuY3Rpb24oZSxyKXtcInVzZSBzdHJpY3RcIjt2YXIgdD1PYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LG49T2JqZWN0LnByb3RvdHlwZS50b1N0cmluZyxvPWZ1bmN0aW9uKGUpe3JldHVyblwiZnVuY3Rpb25cIj09dHlwZW9mIEFycmF5LmlzQXJyYXk/QXJyYXkuaXNBcnJheShlKTpcIltvYmplY3QgQXJyYXldXCI9PT1uLmNhbGwoZSl9LGk9ZnVuY3Rpb24oZSl7aWYoIWV8fFwiW29iamVjdCBPYmplY3RdXCIhPT1uLmNhbGwoZSkpcmV0dXJuITE7dmFyIHI9dC5jYWxsKGUsXCJjb25zdHJ1Y3RvclwiKSxvPWUuY29uc3RydWN0b3ImJmUuY29uc3RydWN0b3IucHJvdG90eXBlJiZ0LmNhbGwoZS5jb25zdHJ1Y3Rvci5wcm90b3R5cGUsXCJpc1Byb3RvdHlwZU9mXCIpO2lmKGUuY29uc3RydWN0b3ImJiFyJiYhbylyZXR1cm4hMTt2YXIgaTtmb3IoaSBpbiBlKTtyZXR1cm5cInVuZGVmaW5lZFwiPT10eXBlb2YgaXx8dC5jYWxsKGUsaSl9O2UuZXhwb3J0cz1mdW5jdGlvbiBlKCl7dmFyIHIsdCxuLGEscyx1LGM9YXJndW1lbnRzWzBdLGw9MSxwPWFyZ3VtZW50cy5sZW5ndGgsZj0hMTtmb3IoXCJib29sZWFuXCI9PXR5cGVvZiBjPyhmPWMsYz1hcmd1bWVudHNbMV18fHt9LGw9Mik6KFwib2JqZWN0XCIhPXR5cGVvZiBjJiZcImZ1bmN0aW9uXCIhPXR5cGVvZiBjfHxudWxsPT1jKSYmKGM9e30pO2w8cDsrK2wpaWYocj1hcmd1bWVudHNbbF0sbnVsbCE9cilmb3IodCBpbiByKW49Y1t0XSxhPXJbdF0sYyE9PWEmJihmJiZhJiYoaShhKXx8KHM9byhhKSkpPyhzPyhzPSExLHU9biYmbyhuKT9uOltdKTp1PW4mJmkobik/bjp7fSxjW3RdPWUoZix1LGEpKTpcInVuZGVmaW5lZFwiIT10eXBlb2YgYSYmKGNbdF09YSkpO3JldHVybiBjfX0sZnVuY3Rpb24oZSxyLHQpe1widXNlIHN0cmljdFwiO2Z1bmN0aW9uIG4oKXtyZXR1cm4gbH1mdW5jdGlvbiBvKCl7cmV0dXJuIG51bGx9ZnVuY3Rpb24gaShlKXt2YXIgcj17fTtyZXR1cm4gci5fc3RhY2tGcmFtZT1lLHIudXJsPWUuZmlsZU5hbWUsci5saW5lPWUubGluZU51bWJlcixyLmZ1bmM9ZS5mdW5jdGlvbk5hbWUsci5jb2x1bW49ZS5jb2x1bW5OdW1iZXIsci5hcmdzPWUuYXJncyxyLmNvbnRleHQ9byhyLnVybCxyLmxpbmUpLHJ9ZnVuY3Rpb24gYShlKXtmdW5jdGlvbiByKCl7dmFyIHI9W107dHJ5e3I9Yy5wYXJzZShlKX1jYXRjaChlKXtyPVtdfWZvcih2YXIgdD1bXSxuPTA7bjxyLmxlbmd0aDtuKyspdC5wdXNoKG5ldyBpKHJbbl0pKTtyZXR1cm4gdH1yZXR1cm57c3RhY2s6cigpLG1lc3NhZ2U6ZS5tZXNzYWdlLG5hbWU6ZS5uYW1lfX1mdW5jdGlvbiBzKGUpe3JldHVybiBuZXcgYShlKX1mdW5jdGlvbiB1KGUpe2lmKCFlKXJldHVybltcIlVua25vd24gZXJyb3IuIFRoZXJlIHdhcyBubyBlcnJvciBtZXNzYWdlIHRvIGRpc3BsYXkuXCIsXCJcIl07dmFyIHI9ZS5tYXRjaChwKSx0PVwiKHVua25vd24pXCI7cmV0dXJuIHImJih0PXJbci5sZW5ndGgtMV0sZT1lLnJlcGxhY2UoKHJbci5sZW5ndGgtMl18fFwiXCIpK3QrXCI6XCIsXCJcIiksZT1lLnJlcGxhY2UoLyheW1xcc10rfFtcXHNdKyQpL2csXCJcIikpLFt0LGVdfXZhciBjPXQoNiksbD1cIj9cIixwPW5ldyBSZWdFeHAoXCJeKChbYS16QS1aMC05LV8kIF0qKTogKik/KFVuY2F1Z2h0ICk/KFthLXpBLVowLTktXyQgXSopOiBcIik7ZS5leHBvcnRzPXtndWVzc0Z1bmN0aW9uTmFtZTpuLGd1ZXNzRXJyb3JDbGFzczp1LGdhdGhlckNvbnRleHQ6byxwYXJzZTpzLFN0YWNrOmEsRnJhbWU6aX19LGZ1bmN0aW9uKGUscix0KXt2YXIgbixvLGk7IWZ1bmN0aW9uKGEscyl7XCJ1c2Ugc3RyaWN0XCI7bz1bdCg3KV0sbj1zLGk9XCJmdW5jdGlvblwiPT10eXBlb2Ygbj9uLmFwcGx5KHIsbyk6biwhKHZvaWQgMCE9PWkmJihlLmV4cG9ydHM9aSkpfSh0aGlzLGZ1bmN0aW9uKGUpe1widXNlIHN0cmljdFwiO2Z1bmN0aW9uIHIoZSxyLHQpe2lmKFwiZnVuY3Rpb25cIj09dHlwZW9mIEFycmF5LnByb3RvdHlwZS5tYXApcmV0dXJuIGUubWFwKHIsdCk7Zm9yKHZhciBuPW5ldyBBcnJheShlLmxlbmd0aCksbz0wO288ZS5sZW5ndGg7bysrKW5bb109ci5jYWxsKHQsZVtvXSk7cmV0dXJuIG59ZnVuY3Rpb24gdChlLHIsdCl7aWYoXCJmdW5jdGlvblwiPT10eXBlb2YgQXJyYXkucHJvdG90eXBlLmZpbHRlcilyZXR1cm4gZS5maWx0ZXIocix0KTtmb3IodmFyIG49W10sbz0wO288ZS5sZW5ndGg7bysrKXIuY2FsbCh0LGVbb10pJiZuLnB1c2goZVtvXSk7cmV0dXJuIG59dmFyIG49LyhefEApXFxTK1xcOlxcZCsvLG89L15cXHMqYXQgLiooXFxTK1xcOlxcZCt8XFwobmF0aXZlXFwpKS9tLGk9L14oZXZhbEApPyhcXFtuYXRpdmUgY29kZVxcXSk/JC87cmV0dXJue3BhcnNlOmZ1bmN0aW9uKGUpe2lmKFwidW5kZWZpbmVkXCIhPXR5cGVvZiBlLnN0YWNrdHJhY2V8fFwidW5kZWZpbmVkXCIhPXR5cGVvZiBlW1wib3BlcmEjc291cmNlbG9jXCJdKXJldHVybiB0aGlzLnBhcnNlT3BlcmEoZSk7aWYoZS5zdGFjayYmZS5zdGFjay5tYXRjaChvKSlyZXR1cm4gdGhpcy5wYXJzZVY4T3JJRShlKTtpZihlLnN0YWNrKXJldHVybiB0aGlzLnBhcnNlRkZPclNhZmFyaShlKTt0aHJvdyBuZXcgRXJyb3IoXCJDYW5ub3QgcGFyc2UgZ2l2ZW4gRXJyb3Igb2JqZWN0XCIpfSxleHRyYWN0TG9jYXRpb246ZnVuY3Rpb24oZSl7aWYoZS5pbmRleE9mKFwiOlwiKT09PS0xKXJldHVybltlXTt2YXIgcj1lLnJlcGxhY2UoL1tcXChcXClcXHNdL2csXCJcIikuc3BsaXQoXCI6XCIpLHQ9ci5wb3AoKSxuPXJbci5sZW5ndGgtMV07aWYoIWlzTmFOKHBhcnNlRmxvYXQobikpJiZpc0Zpbml0ZShuKSl7dmFyIG89ci5wb3AoKTtyZXR1cm5bci5qb2luKFwiOlwiKSxvLHRdfXJldHVybltyLmpvaW4oXCI6XCIpLHQsdm9pZCAwXX0scGFyc2VWOE9ySUU6ZnVuY3Rpb24obil7dmFyIGk9dChuLnN0YWNrLnNwbGl0KFwiXFxuXCIpLGZ1bmN0aW9uKGUpe3JldHVybiEhZS5tYXRjaChvKX0sdGhpcyk7cmV0dXJuIHIoaSxmdW5jdGlvbihyKXtyLmluZGV4T2YoXCIoZXZhbCBcIik+LTEmJihyPXIucmVwbGFjZSgvZXZhbCBjb2RlL2csXCJldmFsXCIpLnJlcGxhY2UoLyhcXChldmFsIGF0IFteXFwoKV0qKXwoXFwpXFwsLiokKS9nLFwiXCIpKTt2YXIgdD1yLnJlcGxhY2UoL15cXHMrLyxcIlwiKS5yZXBsYWNlKC9cXChldmFsIGNvZGUvZyxcIihcIikuc3BsaXQoL1xccysvKS5zbGljZSgxKSxuPXRoaXMuZXh0cmFjdExvY2F0aW9uKHQucG9wKCkpLG89dC5qb2luKFwiIFwiKXx8dm9pZCAwLGk9XCJldmFsXCI9PT1uWzBdP3ZvaWQgMDpuWzBdO3JldHVybiBuZXcgZShvLHZvaWQgMCxpLG5bMV0sblsyXSxyKX0sdGhpcyl9LHBhcnNlRkZPclNhZmFyaTpmdW5jdGlvbihuKXt2YXIgbz10KG4uc3RhY2suc3BsaXQoXCJcXG5cIiksZnVuY3Rpb24oZSl7cmV0dXJuIWUubWF0Y2goaSl9LHRoaXMpO3JldHVybiByKG8sZnVuY3Rpb24ocil7aWYoci5pbmRleE9mKFwiID4gZXZhbFwiKT4tMSYmKHI9ci5yZXBsYWNlKC8gbGluZSAoXFxkKykoPzogPiBldmFsIGxpbmUgXFxkKykqID4gZXZhbFxcOlxcZCtcXDpcXGQrL2csXCI6JDFcIikpLHIuaW5kZXhPZihcIkBcIik9PT0tMSYmci5pbmRleE9mKFwiOlwiKT09PS0xKXJldHVybiBuZXcgZShyKTt2YXIgdD1yLnNwbGl0KFwiQFwiKSxuPXRoaXMuZXh0cmFjdExvY2F0aW9uKHQucG9wKCkpLG89dC5zaGlmdCgpfHx2b2lkIDA7cmV0dXJuIG5ldyBlKG8sdm9pZCAwLG5bMF0sblsxXSxuWzJdLHIpfSx0aGlzKX0scGFyc2VPcGVyYTpmdW5jdGlvbihlKXtyZXR1cm4hZS5zdGFja3RyYWNlfHxlLm1lc3NhZ2UuaW5kZXhPZihcIlxcblwiKT4tMSYmZS5tZXNzYWdlLnNwbGl0KFwiXFxuXCIpLmxlbmd0aD5lLnN0YWNrdHJhY2Uuc3BsaXQoXCJcXG5cIikubGVuZ3RoP3RoaXMucGFyc2VPcGVyYTkoZSk6ZS5zdGFjaz90aGlzLnBhcnNlT3BlcmExMShlKTp0aGlzLnBhcnNlT3BlcmExMChlKX0scGFyc2VPcGVyYTk6ZnVuY3Rpb24ocil7Zm9yKHZhciB0PS9MaW5lIChcXGQrKS4qc2NyaXB0ICg/OmluICk/KFxcUyspL2ksbj1yLm1lc3NhZ2Uuc3BsaXQoXCJcXG5cIiksbz1bXSxpPTIsYT1uLmxlbmd0aDtpPGE7aSs9Mil7dmFyIHM9dC5leGVjKG5baV0pO3MmJm8ucHVzaChuZXcgZSh2b2lkIDAsdm9pZCAwLHNbMl0sc1sxXSx2b2lkIDAsbltpXSkpfXJldHVybiBvfSxwYXJzZU9wZXJhMTA6ZnVuY3Rpb24ocil7Zm9yKHZhciB0PS9MaW5lIChcXGQrKS4qc2NyaXB0ICg/OmluICk/KFxcUyspKD86OiBJbiBmdW5jdGlvbiAoXFxTKykpPyQvaSxuPXIuc3RhY2t0cmFjZS5zcGxpdChcIlxcblwiKSxvPVtdLGk9MCxhPW4ubGVuZ3RoO2k8YTtpKz0yKXt2YXIgcz10LmV4ZWMobltpXSk7cyYmby5wdXNoKG5ldyBlKHNbM118fHZvaWQgMCx2b2lkIDAsc1syXSxzWzFdLHZvaWQgMCxuW2ldKSl9cmV0dXJuIG99LHBhcnNlT3BlcmExMTpmdW5jdGlvbihvKXt2YXIgaT10KG8uc3RhY2suc3BsaXQoXCJcXG5cIiksZnVuY3Rpb24oZSl7cmV0dXJuISFlLm1hdGNoKG4pJiYhZS5tYXRjaCgvXkVycm9yIGNyZWF0ZWQgYXQvKX0sdGhpcyk7cmV0dXJuIHIoaSxmdW5jdGlvbihyKXt2YXIgdCxuPXIuc3BsaXQoXCJAXCIpLG89dGhpcy5leHRyYWN0TG9jYXRpb24obi5wb3AoKSksaT1uLnNoaWZ0KCl8fFwiXCIsYT1pLnJlcGxhY2UoLzxhbm9ueW1vdXMgZnVuY3Rpb24oOiAoXFx3KykpPz4vLFwiJDJcIikucmVwbGFjZSgvXFwoW15cXCldKlxcKS9nLFwiXCIpfHx2b2lkIDA7aS5tYXRjaCgvXFwoKFteXFwpXSopXFwpLykmJih0PWkucmVwbGFjZSgvXlteXFwoXStcXCgoW15cXCldKilcXCkkLyxcIiQxXCIpKTt2YXIgcz12b2lkIDA9PT10fHxcIlthcmd1bWVudHMgbm90IGF2YWlsYWJsZV1cIj09PXQ/dm9pZCAwOnQuc3BsaXQoXCIsXCIpO3JldHVybiBuZXcgZShhLHMsb1swXSxvWzFdLG9bMl0scil9LHRoaXMpfX19KX0sZnVuY3Rpb24oZSxyLHQpe3ZhciBuLG8saTshZnVuY3Rpb24odCxhKXtcInVzZSBzdHJpY3RcIjtvPVtdLG49YSxpPVwiZnVuY3Rpb25cIj09dHlwZW9mIG4/bi5hcHBseShyLG8pOm4sISh2b2lkIDAhPT1pJiYoZS5leHBvcnRzPWkpKX0odGhpcyxmdW5jdGlvbigpe1widXNlIHN0cmljdFwiO2Z1bmN0aW9uIGUoZSl7cmV0dXJuIWlzTmFOKHBhcnNlRmxvYXQoZSkpJiZpc0Zpbml0ZShlKX1mdW5jdGlvbiByKGUscix0LG4sbyxpKXt2b2lkIDAhPT1lJiZ0aGlzLnNldEZ1bmN0aW9uTmFtZShlKSx2b2lkIDAhPT1yJiZ0aGlzLnNldEFyZ3Mociksdm9pZCAwIT09dCYmdGhpcy5zZXRGaWxlTmFtZSh0KSx2b2lkIDAhPT1uJiZ0aGlzLnNldExpbmVOdW1iZXIobiksdm9pZCAwIT09byYmdGhpcy5zZXRDb2x1bW5OdW1iZXIobyksdm9pZCAwIT09aSYmdGhpcy5zZXRTb3VyY2UoaSl9cmV0dXJuIHIucHJvdG90eXBlPXtnZXRGdW5jdGlvbk5hbWU6ZnVuY3Rpb24oKXtyZXR1cm4gdGhpcy5mdW5jdGlvbk5hbWV9LHNldEZ1bmN0aW9uTmFtZTpmdW5jdGlvbihlKXt0aGlzLmZ1bmN0aW9uTmFtZT1TdHJpbmcoZSl9LGdldEFyZ3M6ZnVuY3Rpb24oKXtyZXR1cm4gdGhpcy5hcmdzfSxzZXRBcmdzOmZ1bmN0aW9uKGUpe2lmKFwiW29iamVjdCBBcnJheV1cIiE9PU9iamVjdC5wcm90b3R5cGUudG9TdHJpbmcuY2FsbChlKSl0aHJvdyBuZXcgVHlwZUVycm9yKFwiQXJncyBtdXN0IGJlIGFuIEFycmF5XCIpO3RoaXMuYXJncz1lfSxnZXRGaWxlTmFtZTpmdW5jdGlvbigpe3JldHVybiB0aGlzLmZpbGVOYW1lfSxzZXRGaWxlTmFtZTpmdW5jdGlvbihlKXt0aGlzLmZpbGVOYW1lPVN0cmluZyhlKX0sZ2V0TGluZU51bWJlcjpmdW5jdGlvbigpe3JldHVybiB0aGlzLmxpbmVOdW1iZXJ9LHNldExpbmVOdW1iZXI6ZnVuY3Rpb24ocil7aWYoIWUocikpdGhyb3cgbmV3IFR5cGVFcnJvcihcIkxpbmUgTnVtYmVyIG11c3QgYmUgYSBOdW1iZXJcIik7dGhpcy5saW5lTnVtYmVyPU51bWJlcihyKX0sZ2V0Q29sdW1uTnVtYmVyOmZ1bmN0aW9uKCl7cmV0dXJuIHRoaXMuY29sdW1uTnVtYmVyfSxzZXRDb2x1bW5OdW1iZXI6ZnVuY3Rpb24ocil7aWYoIWUocikpdGhyb3cgbmV3IFR5cGVFcnJvcihcIkNvbHVtbiBOdW1iZXIgbXVzdCBiZSBhIE51bWJlclwiKTt0aGlzLmNvbHVtbk51bWJlcj1OdW1iZXIocil9LGdldFNvdXJjZTpmdW5jdGlvbigpe3JldHVybiB0aGlzLnNvdXJjZX0sc2V0U291cmNlOmZ1bmN0aW9uKGUpe3RoaXMuc291cmNlPVN0cmluZyhlKX0sdG9TdHJpbmc6ZnVuY3Rpb24oKXt2YXIgcj10aGlzLmdldEZ1bmN0aW9uTmFtZSgpfHxcInthbm9ueW1vdXN9XCIsdD1cIihcIisodGhpcy5nZXRBcmdzKCl8fFtdKS5qb2luKFwiLFwiKStcIilcIixuPXRoaXMuZ2V0RmlsZU5hbWUoKT9cIkBcIit0aGlzLmdldEZpbGVOYW1lKCk6XCJcIixvPWUodGhpcy5nZXRMaW5lTnVtYmVyKCkpP1wiOlwiK3RoaXMuZ2V0TGluZU51bWJlcigpOlwiXCIsaT1lKHRoaXMuZ2V0Q29sdW1uTnVtYmVyKCkpP1wiOlwiK3RoaXMuZ2V0Q29sdW1uTnVtYmVyKCk6XCJcIjtyZXR1cm4gcit0K24rbytpfX0scn0pfSxmdW5jdGlvbihlLHIsdCl7XCJ1c2Ugc3RyaWN0XCI7ZnVuY3Rpb24gbihlKXt2PWV9ZnVuY3Rpb24gbyhlKXtyZXR1cm57fS50b1N0cmluZy5jYWxsKGUpLm1hdGNoKC9cXHMoW2EtekEtWl0rKS8pWzFdLnRvTG93ZXJDYXNlKCl9ZnVuY3Rpb24gaShlLHIpe3JldHVybiBvKGUpPT09cn1mdW5jdGlvbiBhKGUpe2lmKCFpKGUsXCJzdHJpbmdcIikpdGhyb3cgbmV3IEVycm9yKFwicmVjZWl2ZWQgaW52YWxpZCBpbnB1dFwiKTtmb3IodmFyIHI9dyx0PXIucGFyc2VyW3Iuc3RyaWN0TW9kZT9cInN0cmljdFwiOlwibG9vc2VcIl0uZXhlYyhlKSxuPXt9LG89MTQ7by0tOyluW3Iua2V5W29dXT10W29dfHxcIlwiO3JldHVybiBuW3IucS5uYW1lXT17fSxuW3Iua2V5WzEyXV0ucmVwbGFjZShyLnEucGFyc2VyLGZ1bmN0aW9uKGUsdCxvKXt0JiYobltyLnEubmFtZV1bdF09byl9KSxufWZ1bmN0aW9uIHMoZSl7dmFyIHI9YShlKTtyZXR1cm5cIlwiPT09ci5hbmNob3ImJihyLnNvdXJjZT1yLnNvdXJjZS5yZXBsYWNlKFwiI1wiLFwiXCIpKSxlPXIuc291cmNlLnJlcGxhY2UoXCI/XCIrci5xdWVyeSxcIlwiKX1mdW5jdGlvbiB1KGUscil7dmFyIHQsbixvLGE9aShlLFwib2JqZWN0XCIpLHM9aShlLFwiYXJyYXlcIiksYz1bXTtpZihhKWZvcih0IGluIGUpZS5oYXNPd25Qcm9wZXJ0eSh0KSYmYy5wdXNoKHQpO2Vsc2UgaWYocylmb3Iobz0wO288ZS5sZW5ndGg7KytvKWMucHVzaChvKTtmb3Iobz0wO288Yy5sZW5ndGg7KytvKXQ9Y1tvXSxuPWVbdF0sYT1pKG4sXCJvYmplY3RcIikscz1pKG4sXCJhcnJheVwiKSxhfHxzP2VbdF09dShuLHIpOmVbdF09cih0LG4pO3JldHVybiBlfWZ1bmN0aW9uIGMoZSl7cmV0dXJuIGU9U3RyaW5nKGUpLG5ldyBBcnJheShlLmxlbmd0aCsxKS5qb2luKFwiKlwiKX1mdW5jdGlvbiBsKCl7dmFyIGU9KG5ldyBEYXRlKS5nZXRUaW1lKCkscj1cInh4eHh4eHh4LXh4eHgtNHh4eC15eHh4LXh4eHh4eHh4eHh4eFwiLnJlcGxhY2UoL1t4eV0vZyxmdW5jdGlvbihyKXt2YXIgdD0oZSsxNipNYXRoLnJhbmRvbSgpKSUxNnwwO3JldHVybiBlPU1hdGguZmxvb3IoZS8xNiksKFwieFwiPT09cj90OjcmdHw4KS50b1N0cmluZygxNil9KTtyZXR1cm4gcn1mdW5jdGlvbiBwKGUpe3JldHVyblwiZnVuY3Rpb25cIiE9dHlwZW9mIE9iamVjdC5jcmVhdGU/ZnVuY3Rpb24oZSl7dmFyIHI9ZnVuY3Rpb24oKXt9O3JldHVybiBmdW5jdGlvbihlKXtpZihudWxsIT09ZSYmZSE9PU9iamVjdChlKSl0aHJvdyBUeXBlRXJyb3IoXCJBcmd1bWVudCBtdXN0IGJlIGFuIG9iamVjdCwgb3IgbnVsbFwiKTtyLnByb3RvdHlwZT1lfHx7fTt2YXIgdD1uZXcgcjtyZXR1cm4gci5wcm90b3R5cGU9bnVsbCxudWxsPT09ZSYmKHQuX19wcm90b19fPW51bGwpLHR9fSgpKGUpOk9iamVjdC5jcmVhdGUoZSl9ZnVuY3Rpb24gZigpe2Zvcih2YXIgZT1bXSxyPTA7cjxhcmd1bWVudHMubGVuZ3RoO3IrKyl7dmFyIHQ9YXJndW1lbnRzW3JdO1wib2JqZWN0XCI9PXR5cGVvZiB0Pyh0PXYuc3RyaW5naWZ5KHQpLHQubGVuZ3RoPjUwMCYmKHQ9dC5zdWJzdHIoMCw1MDApK1wiLi4uXCIpKTpcInVuZGVmaW5lZFwiPT10eXBlb2YgdCYmKHQ9XCJ1bmRlZmluZWRcIiksZS5wdXNoKHQpfXJldHVybiBlLmpvaW4oXCIgXCIpfWZ1bmN0aW9uIGQoKXttLmllVmVyc2lvbigpPD04P2NvbnNvbGUuZXJyb3IoZi5hcHBseShudWxsLGFyZ3VtZW50cykpOmNvbnNvbGUuZXJyb3IuYXBwbHkobnVsbCxhcmd1bWVudHMpfWZ1bmN0aW9uIGgoKXttLmllVmVyc2lvbigpPD04P2NvbnNvbGUuaW5mbyhmLmFwcGx5KG51bGwsYXJndW1lbnRzKSk6Y29uc29sZS5pbmZvLmFwcGx5KG51bGwsYXJndW1lbnRzKX1mdW5jdGlvbiBnKCl7bS5pZVZlcnNpb24oKTw9OD9jb25zb2xlLmxvZyhmLmFwcGx5KG51bGwsYXJndW1lbnRzKSk6Y29uc29sZS5sb2cuYXBwbHkobnVsbCxhcmd1bWVudHMpfXQoOSk7dmFyIG09dCgxMCksdj1udWxsLHc9e3N0cmljdE1vZGU6ITEsa2V5OltcInNvdXJjZVwiLFwicHJvdG9jb2xcIixcImF1dGhvcml0eVwiLFwidXNlckluZm9cIixcInVzZXJcIixcInBhc3N3b3JkXCIsXCJob3N0XCIsXCJwb3J0XCIsXCJyZWxhdGl2ZVwiLFwicGF0aFwiLFwiZGlyZWN0b3J5XCIsXCJmaWxlXCIsXCJxdWVyeVwiLFwiYW5jaG9yXCJdLHE6e25hbWU6XCJxdWVyeUtleVwiLHBhcnNlcjovKD86XnwmKShbXiY9XSopPT8oW14mXSopL2d9LHBhcnNlcjp7c3RyaWN0Oi9eKD86KFteOlxcLz8jXSspOik/KD86XFwvXFwvKCg/OigoW146QF0qKSg/OjooW146QF0qKSk/KT9AKT8oW146XFwvPyNdKikoPzo6KFxcZCopKT8pKT8oKCgoPzpbXj8jXFwvXSpcXC8pKikoW14/I10qKSkoPzpcXD8oW14jXSopKT8oPzojKC4qKSk/KS8sbG9vc2U6L14oPzooPyFbXjpAXSs6W146QFxcL10qQCkoW146XFwvPyMuXSspOik/KD86XFwvXFwvKT8oKD86KChbXjpAXSopKD86OihbXjpAXSopKT8pP0ApPyhbXjpcXC8/I10qKSg/OjooXFxkKikpPykoKChcXC8oPzpbXj8jXSg/IVtePyNcXC9dKlxcLltePyNcXC8uXSsoPzpbPyNdfCQpKSkqXFwvPyk/KFtePyNcXC9dKikpKD86XFw/KFteI10qKSk/KD86IyguKikpPykvfX0seT17c2V0dXBKU09OOm4saXNUeXBlOmkscGFyc2VVcmk6YSxwYXJzZVVyaU9wdGlvbnM6dyxyZWRhY3Q6YyxzYW5pdGl6ZVVybDpzLHRyYXZlcnNlOnUsdHlwZU5hbWU6byx1dWlkNDpsLG9iamVjdENyZWF0ZTpwLGNvbnNvbGVFcnJvcjpkLGNvbnNvbGVJbmZvOmgsY29uc29sZUxvZzpnfTtlLmV4cG9ydHM9eX0sZnVuY3Rpb24oZSxyKXshZnVuY3Rpb24oZSl7XCJ1c2Ugc3RyaWN0XCI7ZS5jb25zb2xlfHwoZS5jb25zb2xlPXt9KTtmb3IodmFyIHIsdCxuPWUuY29uc29sZSxvPWZ1bmN0aW9uKCl7fSxpPVtcIm1lbW9yeVwiXSxhPVwiYXNzZXJ0LGNsZWFyLGNvdW50LGRlYnVnLGRpcixkaXJ4bWwsZXJyb3IsZXhjZXB0aW9uLGdyb3VwLGdyb3VwQ29sbGFwc2VkLGdyb3VwRW5kLGluZm8sbG9nLG1hcmtUaW1lbGluZSxwcm9maWxlLHByb2ZpbGVzLHByb2ZpbGVFbmQsc2hvdyx0YWJsZSx0aW1lLHRpbWVFbmQsdGltZWxpbmUsdGltZWxpbmVFbmQsdGltZVN0YW1wLHRyYWNlLHdhcm5cIi5zcGxpdChcIixcIik7cj1pLnBvcCgpOyluW3JdfHwobltyXT17fSk7Zm9yKDt0PWEucG9wKCk7KW5bdF18fChuW3RdPW8pfShcInVuZGVmaW5lZFwiPT10eXBlb2Ygd2luZG93P3RoaXM6d2luZG93KX0sZnVuY3Rpb24oZSxyKXtcInVzZSBzdHJpY3RcIjtmdW5jdGlvbiB0KCl7Zm9yKHZhciBlLHI9Myx0PWRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoXCJkaXZcIiksbj10LmdldEVsZW1lbnRzQnlUYWdOYW1lKFwiaVwiKTt0LmlubmVySFRNTD1cIjwhLS1baWYgZ3QgSUUgXCIrICsrcitcIl0+PGk+PC9pPjwhW2VuZGlmXS0tPlwiLG5bMF07KTtyZXR1cm4gcj40P3I6ZX12YXIgbj17aWVWZXJzaW9uOnR9O2UuZXhwb3J0cz1ufSxmdW5jdGlvbihlLHIsdCl7XCJ1c2Ugc3RyaWN0XCI7ZnVuY3Rpb24gbihlKXthPWV9ZnVuY3Rpb24gbyhlKXt0aGlzLm5hbWU9XCJDb25uZWN0aW9uIEVycm9yXCIsdGhpcy5tZXNzYWdlPWUsdGhpcy5zdGFjaz0obmV3IEVycm9yKS5zdGFja312YXIgaT10KDgpLGE9bnVsbDtvLnByb3RvdHlwZT1pLm9iamVjdENyZWF0ZShFcnJvci5wcm90b3R5cGUpLG8ucHJvdG90eXBlLmNvbnN0cnVjdG9yPW87dmFyIHM9e1hNTEh0dHBGYWN0b3JpZXM6W2Z1bmN0aW9uKCl7cmV0dXJuIG5ldyBYTUxIdHRwUmVxdWVzdH0sZnVuY3Rpb24oKXtyZXR1cm4gbmV3IEFjdGl2ZVhPYmplY3QoXCJNc3htbDIuWE1MSFRUUFwiKX0sZnVuY3Rpb24oKXtyZXR1cm4gbmV3IEFjdGl2ZVhPYmplY3QoXCJNc3htbDMuWE1MSFRUUFwiKX0sZnVuY3Rpb24oKXtyZXR1cm4gbmV3IEFjdGl2ZVhPYmplY3QoXCJNaWNyb3NvZnQuWE1MSFRUUFwiKX1dLGNyZWF0ZVhNTEhUVFBPYmplY3Q6ZnVuY3Rpb24oKXt2YXIgZSxyPSExLHQ9cy5YTUxIdHRwRmFjdG9yaWVzLG49dC5sZW5ndGg7Zm9yKGU9MDtlPG47ZSsrKXRyeXtyPXRbZV0oKTticmVha31jYXRjaChlKXt9cmV0dXJuIHJ9LHBvc3Q6ZnVuY3Rpb24oZSxyLHQsbil7aWYoIWkuaXNUeXBlKHQsXCJvYmplY3RcIikpdGhyb3cgbmV3IEVycm9yKFwiRXhwZWN0ZWQgYW4gb2JqZWN0IHRvIFBPU1RcIik7dD1hLnN0cmluZ2lmeSh0KSxuPW58fGZ1bmN0aW9uKCl7fTt2YXIgdT1zLmNyZWF0ZVhNTEhUVFBPYmplY3QoKTtpZih1KXRyeXt0cnl7dmFyIGM9ZnVuY3Rpb24oKXt0cnl7aWYoYyYmND09PXUucmVhZHlTdGF0ZSl7Yz12b2lkIDA7dmFyIGU9YS5wYXJzZSh1LnJlc3BvbnNlVGV4dCk7MjAwPT09dS5zdGF0dXM/bihudWxsLGUpOmkuaXNUeXBlKHUuc3RhdHVzLFwibnVtYmVyXCIpJiZ1LnN0YXR1cz49NDAwJiZ1LnN0YXR1czw2MDA/KDQwMz09dS5zdGF0dXMmJmkuY29uc29sZUVycm9yKFwiW1JvbGxiYXJdOlwiK2UubWVzc2FnZSksbihuZXcgRXJyb3IoU3RyaW5nKHUuc3RhdHVzKSkpKTpuKG5ldyBvKFwiWEhSIHJlc3BvbnNlIGhhZCBubyBzdGF0dXMgY29kZSAobGlrZWx5IGNvbm5lY3Rpb24gZmFpbHVyZSlcIikpfX1jYXRjaChlKXt2YXIgcjtyPWUmJmUuc3RhY2s/ZTpuZXcgRXJyb3IoZSksbihyKX19O3Uub3BlbihcIlBPU1RcIixlLCEwKSx1LnNldFJlcXVlc3RIZWFkZXImJih1LnNldFJlcXVlc3RIZWFkZXIoXCJDb250ZW50LVR5cGVcIixcImFwcGxpY2F0aW9uL2pzb25cIiksdS5zZXRSZXF1ZXN0SGVhZGVyKFwiWC1Sb2xsYmFyLUFjY2Vzcy1Ub2tlblwiLHIpKSx1Lm9ucmVhZHlzdGF0ZWNoYW5nZT1jLHUuc2VuZCh0KX1jYXRjaChyKXtpZihcInVuZGVmaW5lZFwiIT10eXBlb2YgWERvbWFpblJlcXVlc3Qpe1wiaHR0cDpcIj09PXdpbmRvdy5sb2NhdGlvbi5ocmVmLnN1YnN0cmluZygwLDUpJiZcImh0dHBzXCI9PT1lLnN1YnN0cmluZygwLDUpJiYoZT1cImh0dHBcIitlLnN1YnN0cmluZyg1KSk7dmFyIGw9ZnVuY3Rpb24oKXtuKG5ldyBvKFwiUmVxdWVzdCB0aW1lZCBvdXRcIikpfSxwPWZ1bmN0aW9uKCl7bihuZXcgRXJyb3IoXCJFcnJvciBkdXJpbmcgcmVxdWVzdFwiKSl9LGY9ZnVuY3Rpb24oKXtuKG51bGwsYS5wYXJzZSh1LnJlc3BvbnNlVGV4dCkpfTt1PW5ldyBYRG9tYWluUmVxdWVzdCx1Lm9ucHJvZ3Jlc3M9ZnVuY3Rpb24oKXt9LHUub250aW1lb3V0PWwsdS5vbmVycm9yPXAsdS5vbmxvYWQ9Zix1Lm9wZW4oXCJQT1NUXCIsZSwhMCksdS5zZW5kKHQpfX19Y2F0Y2goZSl7bihlKX19fTtlLmV4cG9ydHM9e1hIUjpzLHNldHVwSlNPTjpuLENvbm5lY3Rpb25FcnJvcjpvfX1dKX0pOyJdfQ==
