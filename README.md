# ConTroll.js

JavaScript SDK for ConTroll clients.

ConTroll is an open source convention management system geared towards table-top gaming conventions. See http://con-troll.org

# Installation & Setup

First install using bower:

```
bower install controll.js
```

Then add the script file to your HTML using the path `bower_components/controll.js/dist/controll.js`

(there's also a minified version if you prefer, but the difference is not large)

# Browser usage

Load the minified version into your application, which will make the ConTroll top level object available. At which point you can
perform a ConTroll login through the `ConTroll.auth` APIs, list available conventions using the `ConTroll.conventions` APIs and
set the active convention using `ConTroll.setConvention()` API.

